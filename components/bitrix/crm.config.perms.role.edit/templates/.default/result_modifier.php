<?php

$arResult['ENTITY'] = array_merge(
    $arResult['ENTITY'],
    array(
        'ITHIVE_VISIT' => GetMessage('CRM_PERMS_PERM_VISIT_TITLE')
    )
);

$arResult['ENTITY_FIELDS']['ITHIVE_VISIT'] = array('STATUS_ID' => ['NEW' => GetMessage('CRM_PERMS_PERM_VISIT_NEW'), 'SENT2MP' => GetMessage('CRM_PERMS_PERM_VISIT_SENT2MP'), 'RECIEVED' => GetMessage('CRM_PERMS_PERM_VISIT_RECIEVED')]);

$arResult['ROLE_PERM']['ITHIVE_VISIT'] = $arResult['ROLE_PERM']['LEAD'];

$arResult['ENTITY_PERMS']['ITHIVE_VISIT'] = array('READ', 'ADD', 'WRITE', 'DELETE');