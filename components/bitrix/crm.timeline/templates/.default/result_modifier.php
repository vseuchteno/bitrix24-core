<?php
if ($arResult['ENTITY_TYPE_NAME'] == "COMPANY") {
    $arTabs[] = [
        'id' => 'ithive_visit',
        'name' => 'Визит на ТТ'
    ];

    if (empty($arResult['ADDITIONAL_TABS'])) {
        $arResult['ADDITIONAL_TABS'] = $arTabs;
    } else {
        $arResult['ADDITIONAL_TABS'] = array_merge($arTabs, $arResult['ADDITIONAL_TABS']);
    }

}