<?php
$MESS["SCP_SALESCENTER_TITLE"] = "Выбрать онлайн-кассу";
$MESS["SCP_SALESCENTER_CASHBOX_SUB_TITLE"] = "Онлайн-кассы";
$MESS["SCP_SALESCENTER_OFFILINE_CASHBOX_SUB_TITLE"] = "Офлайн-касса";
$MESS["SCP_POPUP_TITLE"] = "Подтверждение";
$MESS["SCP_POPUP_CONTENT"] = "Вы не сохранили изменения. Уверены, что хотите закрыть?";
$MESS["SCP_POPUP_BUTTON_CLOSE"] = "Закрыть";
$MESS["SCP_POPUP_BUTTON_CANCEL"] = "Отменить";
$MESS["SALESCENTER_CONTROL_PANEL_ITEM_LABEL_RECOMMENDATION"] = "Рекомендовано";

