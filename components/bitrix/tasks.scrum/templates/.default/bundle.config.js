module.exports = {
	input: './src/scrum.js',
	output: {
		css: './style.css',
		js: './script.js',
	},
	namespace: 'BX.Tasks.Scrum',
	adjustConfigPhp: false
};