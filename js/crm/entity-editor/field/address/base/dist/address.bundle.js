this.BX = this.BX || {};
(function (exports,main_core,main_core_events,main_popup) {
	'use strict';

	function _templateObject18() {
	  var data = babelHelpers.taggedTemplateLiteral(["<span class=\"ui-ctl-after ", "\"></span>"]);

	  _templateObject18 = function _templateObject18() {
	    return data;
	  };

	  return data;
	}

	function _templateObject17() {
	  var data = babelHelpers.taggedTemplateLiteral(["<button class=\"ui-ctl-after ui-ctl-icon-clear\" onclick=\"", "\"></button>"]);

	  _templateObject17 = function _templateObject17() {
	    return data;
	  };

	  return data;
	}

	function _templateObject16() {
	  var data = babelHelpers.taggedTemplateLiteral(["<span class=\"ui-ctl-after ui-ctl-icon-loader\"></span>"]);

	  _templateObject16 = function _templateObject16() {
	    return data;
	  };

	  return data;
	}

	function _templateObject15() {
	  var data = babelHelpers.taggedTemplateLiteral(["\n\t\t\t<div class=\"crm-address-control-item\">\n\t\t\t\t", "\n\t\t\t</div>"]);

	  _templateObject15 = function _templateObject15() {
	    return data;
	  };

	  return data;
	}

	function _templateObject14() {
	  var data = babelHelpers.taggedTemplateLiteral(["\n\t\t\t<div class=\"ui-entity-editor-content-block-text\">\n\t\t\t\t<span class=\"ui-link ui-link-dark ui-link-dotted\">", "</span>\n\t\t\t</div>"]);

	  _templateObject14 = function _templateObject14() {
	    return data;
	  };

	  return data;
	}

	function _templateObject13() {
	  var data = babelHelpers.taggedTemplateLiteral(["\n\t\t\t<div class=\"crm-address-control-item\">\n\t\t\t\t<div class=\"crm-address-control-mode-switch\">\n\t\t\t\t\t", "\n\t\t\t\t</div>\n\t\t\t\t", "\n\t\t\t\t", "\n\t\t\t</div>"]);

	  _templateObject13 = function _templateObject13() {
	    return data;
	  };

	  return data;
	}

	function _templateObject12() {
	  var data = babelHelpers.taggedTemplateLiteral(["<span class=\"ui-link ui-link-secondary ui-entity-editor-block-title-link\" onclick=\"", "\"></span>"]);

	  _templateObject12 = function _templateObject12() {
	    return data;
	  };

	  return data;
	}

	function _templateObject11() {
	  var data = babelHelpers.taggedTemplateLiteral(["\n\t\t\t<div class=\"location-fields-control-block crm-address-type-block\">\n\t\t\t\t<div class=\"ui-entity-editor-content-block ui-entity-editor-field-text\">\n\t\t\t\t\t<div class=\"ui-entity-editor-block-title\">\n\t\t\t\t\t\t<label class=\"ui-entity-editor-block-title-text\">", "</label>\n\t\t\t\t\t</div>\n\t\t\t\t\t", "\n\t\t\t\t</div>\n\t\t\t</div>"]);

	  _templateObject11 = function _templateObject11() {
	    return data;
	  };

	  return data;
	}

	function _templateObject10() {
	  var data = babelHelpers.taggedTemplateLiteral(["\n\t\t\t<div class=\"ui-ctl ui-ctl-w100 ui-ctl-after-icon ui-ctl-dropdown\" onclick=\"", "\">\n\t\t\t\t<div class=\"ui-ctl-after ui-ctl-icon-angle\"></div>\n\t\t\t\t", "\n\t\t\t</div>"]);

	  _templateObject10 = function _templateObject10() {
	    return data;
	  };

	  return data;
	}

	function _templateObject9() {
	  var data = babelHelpers.taggedTemplateLiteral(["\n\t\t\t<div class=\"location-fields-control-block\"></div>"]);

	  _templateObject9 = function _templateObject9() {
	    return data;
	  };

	  return data;
	}

	function _templateObject8() {
	  var data = babelHelpers.taggedTemplateLiteral(["\n\t\t<div class=\"crm-address-search-control-block\">\n\t\t\t<div class=\"ui-ctl ui-ctl-w100 ui-ctl-after-icon\">\n\t\t\t\t", "\n\t\t\t\t", "\n\t\t\t</div>\n\t\t</div>"]);

	  _templateObject8 = function _templateObject8() {
	    return data;
	  };

	  return data;
	}

	function _templateObject7() {
	  var data = babelHelpers.taggedTemplateLiteral(["<span></span>"]);

	  _templateObject7 = function _templateObject7() {
	    return data;
	  };

	  return data;
	}

	function _templateObject6() {
	  var data = babelHelpers.taggedTemplateLiteral(["\n\t\t\t<input type=\"text\" class=\"ui-ctl-element ui-ctl-textbox\" value=\"", "\" ", ">"]);

	  _templateObject6 = function _templateObject6() {
	    return data;
	  };

	  return data;
	}

	function _templateObject5() {
	  var data = babelHelpers.taggedTemplateLiteral(["<div class=\"ui-ctl-element\"></div>"]);

	  _templateObject5 = function _templateObject5() {
	    return data;
	  };

	  return data;
	}

	function _templateObject4() {
	  var data = babelHelpers.taggedTemplateLiteral(["<div>Location module is not installed</div>"]);

	  _templateObject4 = function _templateObject4() {
	    return data;
	  };

	  return data;
	}

	function _templateObject3() {
	  var data = babelHelpers.taggedTemplateLiteral(["\n\t\t\t\t\t<span class=\"ui-link ui-link-secondary ui-link-dotted\" onmouseup=\"", "\"\n\t\t\t\t\t\t>\n\t\t\t\t\t\t", "\n\t\t\t\t\t</span>"]);

	  _templateObject3 = function _templateObject3() {
	    return data;
	  };

	  return data;
	}

	function _templateObject2() {
	  var data = babelHelpers.taggedTemplateLiteral(["\n\t\t\t\t<div class=\"", "\"><span class=\"", "\" onclick=\"", "\">", "</span></div>\n\t\t\t"]);

	  _templateObject2 = function _templateObject2() {
	    return data;
	  };

	  return data;
	}

	function _templateObject() {
	  var data = babelHelpers.taggedTemplateLiteral(["<div class=\"crm-address-control-wrap ", "\"></div>"]);

	  _templateObject = function _templateObject() {
	    return data;
	  };

	  return data;
	}

	function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it.return != null) it.return(); } finally { if (didErr) throw err; } } }; }

	function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

	function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }
	var EntityEditorBaseAddressField = /*#__PURE__*/function () {
	  function EntityEditorBaseAddressField() {
	    babelHelpers.classCallCheck(this, EntityEditorBaseAddressField);
	  }

	  babelHelpers.createClass(EntityEditorBaseAddressField, [{
	    key: "initialize",
	    value: function initialize(id, settings) {
	      this._id = BX.type.isNotEmptyString(id) ? id : BX.util.getRandomString(4);
	      this._isMultiple = false;
	      this._settings = settings ? settings : {};
	      this._typesList = [];
	      this._availableTypesIds = [];
	      this._addressList = [];
	      this._wrapper = null;
	      this._isEditMode = true;
	      this._showFirstItemOnly = BX.prop.getBoolean(settings, 'showFirstItemOnly', false);
	      this._enableAutocomplete = BX.prop.getBoolean(settings, 'enableAutocomplete', true);
	    }
	  }, {
	    key: "setMultiple",
	    value: function setMultiple(isMultiple) {
	      this._isMultiple = !!isMultiple;
	    }
	  }, {
	    key: "setValue",
	    value: function setValue(value) {
	      if (this._isMultiple) {
	        var items = main_core.Type.isPlainObject(value) ? value : {};
	        var types = Object.keys(items);
	        var isSame = this._addressList.length > 0 && types.length == this._addressList.length;

	        if (isSame) {
	          var _iterator = _createForOfIteratorHelper(this._addressList),
	              _step;

	          try {
	            for (_iterator.s(); !(_step = _iterator.n()).done;) {
	              var addressItem = _step.value;
	              var type = addressItem.getType();

	              if (!items.hasOwnProperty(type) || items[type] !== addressItem.getValue()) {
	                isSame = false;
	                break;
	              }
	            }
	          } catch (err) {
	            _iterator.e(err);
	          } finally {
	            _iterator.f();
	          }
	        }

	        if ( // if new value is empty and old value has only one empty element
	        !isSame && !types.length && this._addressList.length === 1 && !this._addressList[0].getValue().length) {
	          isSame = true;
	        }

	        if (isSame) {
	          return false; // update is not required
	        }

	        this.removeAllAddresses();

	        for (var _i = 0, _types = types; _i < _types.length; _i++) {
	          var _type = _types[_i];
	          this.addAddress(_type, items[_type]);
	        }

	        if (!types.length) {
	          this.addAddress(this.getDefaultType(), null);
	        }
	      } else {
	        this.removeAllAddresses();
	        var address = main_core.Type.isStringFilled(value) ? value : null;
	        this.addAddress(null, address);
	      }

	      return true;
	    }
	  }, {
	    key: "getValue",
	    value: function getValue() {
	      if (this._isMultiple) {
	        var result = [];

	        var _iterator2 = _createForOfIteratorHelper(this._addressList),
	            _step2;

	        try {
	          for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
	            var addressItem = _step2.value;
	            var value = addressItem.getValue();

	            if (main_core.Type.isString(value)) {
	              result.push({
	                type: addressItem.getType(),
	                value: value
	              });
	            }
	          }
	        } catch (err) {
	          _iterator2.e(err);
	        } finally {
	          _iterator2.f();
	        }

	        return result;
	      } else {
	        if (this._addressList && this._addressList[0] && main_core.Type.isString(this._addressList[0].getValue())) {
	          return this._addressList[0].getValue();
	        }

	        return null;
	      }
	    }
	  }, {
	    key: "setTypesList",
	    value: function setTypesList(list) {
	      this._typesList = [];

	      if (main_core.Type.isPlainObject(list)) {
	        for (var _i2 = 0, _Object$keys = Object.keys(list); _i2 < _Object$keys.length; _i2++) {
	          var id = _Object$keys[_i2];

	          this._typesList.push(list[id]);
	        }
	      }

	      this.initAvailableTypes();
	    }
	  }, {
	    key: "getTypesList",
	    value: function getTypesList() {
	      var types = [];

	      var _iterator3 = _createForOfIteratorHelper(this._typesList),
	          _step3;

	      try {
	        for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
	          var item = _step3.value;
	          var value = BX.prop.getString(item, "ID", "");
	          var name = BX.prop.getString(item, "DESCRIPTION", "");
	          types.push({
	            name: name,
	            value: value
	          });
	        }
	      } catch (err) {
	        _iterator3.e(err);
	      } finally {
	        _iterator3.f();
	      }

	      return types;
	    }
	  }, {
	    key: "getDefaultType",
	    value: function getDefaultType() {
	      var _iterator4 = _createForOfIteratorHelper(this._typesList),
	          _step4;

	      try {
	        for (_iterator4.s(); !(_step4 = _iterator4.n()).done;) {
	          var item = _step4.value;
	          var value = BX.prop.getString(item, "ID", "");
	          var isDefault = BX.prop.getString(item, "IS_DEFAULT", false);

	          if (isDefault && this._availableTypesIds.indexOf(value) >= 0) {
	            return value;
	          }
	        }
	      } catch (err) {
	        _iterator4.e(err);
	      } finally {
	        _iterator4.f();
	      }

	      var _iterator5 = _createForOfIteratorHelper(this._typesList),
	          _step5;

	      try {
	        for (_iterator5.s(); !(_step5 = _iterator5.n()).done;) {
	          var _item = _step5.value;

	          var _value = BX.prop.getString(_item, "ID", "");

	          if (this._availableTypesIds.indexOf(_value) >= 0) {
	            return _value;
	          }
	        }
	      } catch (err) {
	        _iterator5.e(err);
	      } finally {
	        _iterator5.f();
	      }

	      return null;
	    }
	  }, {
	    key: "layout",
	    value: function layout(isEditMode) {
	      this._isEditMode = isEditMode;
	      this._wrapper = main_core.Tag.render(_templateObject(), this._isEditMode ? 'crm-address-control-wrap-edit' : '');
	      this.refreshLayout();
	      return this._wrapper;
	    }
	  }, {
	    key: "refreshLayout",
	    value: function refreshLayout() {
	      main_core.Dom.clean(this._wrapper);
	      var addrCounter = true;

	      var _iterator6 = _createForOfIteratorHelper(this._addressList),
	          _step6;

	      try {
	        for (_iterator6.s(); !(_step6 = _iterator6.n()).done;) {
	          var addressItem = _step6.value;
	          addressItem.setEditMode(this._isEditMode);

	          if (!this._isEditMode && this._showFirstItemOnly && addrCounter > 1) {
	            var showMore = main_core.Tag.render(_templateObject3(), this.onShowMoreMouseUp.bind(this), main_core.Loc.getMessage('CRM_ADDRESS_SHOW_ALL'));
	            main_core.Dom.append(showMore, this._wrapper);
	            break;
	          } else {
	            main_core.Dom.append(addressItem.layout(), this._wrapper);
	          }

	          addrCounter++;
	        }
	      } catch (err) {
	        _iterator6.e(err);
	      } finally {
	        _iterator6.f();
	      }

	      if (this._isEditMode && this._isMultiple && !main_core.Type.isNull(this.getDefaultType())) {
	        var crmCompatibilityMode = BX.prop.getBoolean(this._settings, 'crmCompatibilityMode', false);
	        var addButtonWrapClass = crmCompatibilityMode ? 'crm-entity-widget-content-block-add-field' : 'ui-entity-widget-content-block-add-field';
	        var addButtonClass = crmCompatibilityMode ? 'crm-entity-widget-content-add-field' : 'ui-entity-editor-content-add-lnk';
	        main_core.Dom.append(main_core.Tag.render(_templateObject2(), addButtonWrapClass, addButtonClass, this.onAddNewAddress.bind(this), main_core.Loc.getMessage('CRM_ADDRESS_ADD')), this._wrapper);
	      }
	    }
	  }, {
	    key: "release",
	    value: function release() {
	      main_core.Dom.clean(this._wrapper);
	      this.removeAllAddresses();
	    }
	  }, {
	    key: "removeAllAddresses",
	    value: function removeAllAddresses() {
	      var ids = this._addressList.map(function (item) {
	        return item.getId();
	      });

	      var _iterator7 = _createForOfIteratorHelper(ids),
	          _step7;

	      try {
	        for (_iterator7.s(); !(_step7 = _iterator7.n()).done;) {
	          var id = _step7.value;
	          this.removeAddress(id);
	        }
	      } catch (err) {
	        _iterator7.e(err);
	      } finally {
	        _iterator7.f();
	      }
	    }
	  }, {
	    key: "addAddress",
	    value: function addAddress(type) {
	      var value = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
	      var addressItem = new AddressItem(main_core.Text.getRandom(8), {
	        typesList: this.getTypesList(),
	        availableTypesIds: babelHelpers.toConsumableArray(this._availableTypesIds),
	        canChangeType: this._isMultiple,
	        enableAutocomplete: this._enableAutocomplete,
	        type: type,
	        value: value
	      });
	      addressItem.subscribe('onUpdateAddress', this.onUpdateAddress.bind(this));
	      addressItem.subscribe('onUpdateAddressType', this.onUpdateAddressType.bind(this));
	      addressItem.subscribe('onDelete', this.onDeleteAddress.bind(this));
	      addressItem.subscribe('onStartLoadAddress', this.onStartLoadAddress.bind(this));
	      addressItem.subscribe('onAddressLoaded', this.onAddressLoaded.bind(this));
	      addressItem.subscribe('onError', this.onError.bind(this));
	      this.updateAvailableTypes(type, null);

	      this._addressList.push(addressItem);
	    }
	  }, {
	    key: "removeAddress",
	    value: function removeAddress(id) {
	      var addressItem = this.getAddressById(id);

	      if (addressItem) {
	        var type = addressItem.getType();

	        this._addressList.splice(this._addressList.indexOf(addressItem), 1);

	        this.updateAvailableTypes(null, type);
	        addressItem.destroy();
	      }
	    }
	  }, {
	    key: "getAddressById",
	    value: function getAddressById(id) {
	      return this._addressList.filter(function (item) {
	        return item.getId() === id;
	      }).reduce(function (prev, item) {
	        return prev ? prev : item;
	      }, null);
	    }
	  }, {
	    key: "initAvailableTypes",
	    value: function initAvailableTypes() {
	      this._availableTypesIds = [];

	      var _iterator8 = _createForOfIteratorHelper(this._typesList),
	          _step8;

	      try {
	        for (_iterator8.s(); !(_step8 = _iterator8.n()).done;) {
	          var type = _step8.value;

	          this._availableTypesIds.push(BX.prop.getString(type, "ID", ""));
	        }
	      } catch (err) {
	        _iterator8.e(err);
	      } finally {
	        _iterator8.f();
	      }
	    }
	  }, {
	    key: "updateAvailableTypes",
	    value: function updateAvailableTypes(removedType, addedType) {
	      if (!main_core.Type.isNull(addedType) && this._availableTypesIds.indexOf(addedType) < 0) {
	        this._availableTypesIds.push(addedType);
	      }

	      if (!main_core.Type.isNull(removedType) && this._availableTypesIds.indexOf(removedType) >= 0) {
	        this._availableTypesIds.splice(this._availableTypesIds.indexOf(removedType), 1);
	      }

	      var _iterator9 = _createForOfIteratorHelper(this._addressList),
	          _step9;

	      try {
	        for (_iterator9.s(); !(_step9 = _iterator9.n()).done;) {
	          var addressItem = _step9.value;
	          addressItem.setAvailableTypesIds(babelHelpers.toConsumableArray(this._availableTypesIds));
	        }
	      } catch (err) {
	        _iterator9.e(err);
	      } finally {
	        _iterator9.f();
	      }
	    }
	  }, {
	    key: "emitUpdateEvent",
	    value: function emitUpdateEvent() {
	      main_core_events.EventEmitter.emit(this, 'onUpdate', {
	        value: this.getValue()
	      });
	    }
	  }, {
	    key: "onAddNewAddress",
	    value: function onAddNewAddress() {
	      this.addAddress(this.getDefaultType());
	      this.refreshLayout();
	    }
	  }, {
	    key: "onUpdateAddress",
	    value: function onUpdateAddress(event) {
	      this.emitUpdateEvent();
	    }
	  }, {
	    key: "onDeleteAddress",
	    value: function onDeleteAddress(event) {
	      var data = event.getData();
	      var id = data.id;

	      if (this._addressList.length <= 1) {
	        // should be at least one address, so just clear it
	        var addressItem = this.getAddressById(id);

	        if (addressItem) {
	          addressItem.clearValue();
	        }

	        return;
	      }

	      this.removeAddress(id);
	      this.refreshLayout();
	    }
	  }, {
	    key: "onUpdateAddressType",
	    value: function onUpdateAddressType(event) {
	      var data = event.getData();
	      var prevType = data.prevType;
	      var type = data.type;
	      this.updateAvailableTypes(type, prevType);
	      this.emitUpdateEvent();
	    }
	  }, {
	    key: "onShowMoreMouseUp",
	    value: function onShowMoreMouseUp(event) {
	      event.stopPropagation(); // cancel switching client to edit mode

	      this._showFirstItemOnly = false;
	      this.refreshLayout();
	      return false;
	    }
	  }, {
	    key: "onStartLoadAddress",
	    value: function onStartLoadAddress(event) {
	      main_core_events.EventEmitter.emit(this, 'onStartLoadAddress');
	    }
	  }, {
	    key: "onAddressLoaded",
	    value: function onAddressLoaded(event) {
	      main_core_events.EventEmitter.emit(this, 'onAddressLoaded');
	    }
	  }, {
	    key: "onError",
	    value: function onError(event) {
	      main_core_events.EventEmitter.emit(this, 'onError', event);
	    }
	  }], [{
	    key: "create",
	    value: function create(id, settings) {
	      var self = new EntityEditorBaseAddressField();
	      self.initialize(id, settings);
	      return self;
	    }
	  }]);
	  return EntityEditorBaseAddressField;
	}();

	var AddressItem = /*#__PURE__*/function (_EventEmitter) {
	  babelHelpers.inherits(AddressItem, _EventEmitter);

	  function AddressItem(id, settings) {
	    var _this;

	    babelHelpers.classCallCheck(this, AddressItem);
	    _this = babelHelpers.possibleConstructorReturn(this, babelHelpers.getPrototypeOf(AddressItem).call(this));

	    _this.setEventNamespace('BX.Crm.AddressItem');

	    _this._id = id;
	    _this._value = BX.prop.getString(settings, 'value', "");
	    _this._isTypesMenuOpened = false;
	    _this._typesList = BX.prop.getArray(settings, 'typesList', []);
	    _this._availableTypesIds = BX.prop.getArray(settings, 'availableTypesIds', []);
	    _this._canChangeType = BX.prop.getBoolean(settings, 'canChangeType', false);
	    _this.typesMenuId = 'address_type_menu_' + _this._id;
	    _this._type = BX.prop.getString(settings, 'type', "");
	    _this._isEditMode = true;
	    _this._isAutocompleteEnabled = BX.prop.getBoolean(settings, 'enableAutocomplete', true);
	    _this._showDetails = !_this._isAutocompleteEnabled || BX.prop.getBoolean(settings, 'showDetails', false);
	    _this._isLoading = false;
	    _this._addressWidget = null;
	    _this._wrapper = null;
	    _this._domNodes = {};
	    _this._isLocationModuleInstalled = !main_core.Type.isUndefined(BX.Location) && !main_core.Type.isUndefined(BX.Location.Core) && !main_core.Type.isUndefined(BX.Location.Widget);

	    _this.initializeAddressWidget();

	    return _this;
	  }

	  babelHelpers.createClass(AddressItem, [{
	    key: "initializeAddressWidget",
	    value: function initializeAddressWidget() {
	      if (!this._isLocationModuleInstalled) {
	        return;
	      }

	      var value = this.getValue();
	      var address = null;

	      if (main_core.Type.isStringFilled(value)) {
	        try {
	          address = new BX.Location.Core.Address(JSON.parse(value));
	        } catch (e) {}
	      }

	      var widgetFactory = new BX.Location.Widget.Factory();
	      this._addressWidget = widgetFactory.createAddressWidget({
	        address: address,
	        mode: this._isEditMode ? BX.Location.Core.ControlMode.edit : BX.Location.Core.ControlMode.view,
	        popupBindOptions: {
	          position: 'right'
	        }
	      });

	      this._addressWidget.subscribeOnStateChangedEvent(this.onAddressWidgetChangedState.bind(this));

	      this._addressWidget.subscribeOnAddressChangedEvent(this.onAddressChanged.bind(this));

	      this._addressWidget.subscribeOnErrorEvent(this.onError.bind(this));
	    }
	  }, {
	    key: "getId",
	    value: function getId() {
	      return this._id;
	    }
	  }, {
	    key: "getType",
	    value: function getType() {
	      return this._type;
	    }
	  }, {
	    key: "getValue",
	    value: function getValue() {
	      return this._value;
	    }
	  }, {
	    key: "setEditMode",
	    value: function setEditMode(isEditMode) {
	      this._isEditMode = !!isEditMode;

	      if (!main_core.Type.isNull(this._addressWidget)) {
	        this._addressWidget.mode = isEditMode ? BX.Location.Core.ControlMode.edit : BX.Location.Core.ControlMode.view;
	      }
	    }
	  }, {
	    key: "setAvailableTypesIds",
	    value: function setAvailableTypesIds(ids) {
	      this._availableTypesIds = ids;
	    }
	  }, {
	    key: "layout",
	    value: function layout() {
	      if (main_core.Type.isNull(this._addressWidget)) {
	        this._wrapper = main_core.Tag.render(_templateObject4());
	        return this._wrapper;
	      }

	      var addressWidgetParams = {};
	      var addressString = this.convertAddressToString(this.getAddress());

	      if (this._isEditMode) {
	        this._wrapper = this.getEditHtml(addressString);
	        addressWidgetParams.mode = BX.Location.Core.ControlMode.edit;
	        addressWidgetParams.inputNode = this._domNodes.searchInput;
	        addressWidgetParams.mapBindElement = this._domNodes.searchInput;
	        addressWidgetParams.fieldsContainer = this._domNodes.detailsContainer;
	        addressWidgetParams.controlWrapper = this._domNodes.addressContainer;
	      } else {
	        this._wrapper = this.getViewHtml(addressString);
	        addressWidgetParams.mode = BX.Location.Core.ControlMode.view;
	        addressWidgetParams.mapBindElement = this._wrapper;
	      }

	      addressWidgetParams.controlWrapper = this._domNodes.addressContainer;

	      this._addressWidget.render(addressWidgetParams);

	      return this._wrapper;
	    }
	  }, {
	    key: "openTypesMenu",
	    value: function openTypesMenu(bindElement) {
	      var _this2 = this;

	      if (this._isTypesMenuOpened) {
	        return;
	      }

	      var menu = [];

	      var _iterator10 = _createForOfIteratorHelper(this._typesList),
	          _step10;

	      try {
	        for (_iterator10.s(); !(_step10 = _iterator10.n()).done;) {
	          var item = _step10.value;
	          var selected = item.value === this._type;

	          if (this._availableTypesIds.indexOf(item.value) < 0 && !selected) {
	            continue;
	          }

	          menu.push({
	            text: item.name,
	            value: item.value,
	            //className: selected ? "menu-popup-item-accept" : "menu-popup-item-none",
	            onclick: this.onChangeType.bind(this)
	          });
	        }
	      } catch (err) {
	        _iterator10.e(err);
	      } finally {
	        _iterator10.f();
	      }

	      main_popup.MenuManager.show(this.typesMenuId, bindElement, menu, {
	        angle: false,
	        cacheable: false,
	        events: {
	          onPopupShow: function onPopupShow() {
	            _this2._isTypesMenuOpened = true;
	          },
	          onPopupClose: function onPopupClose() {
	            _this2._isTypesMenuOpened = false;
	          }
	        }
	      });
	      var createdMenu = main_popup.MenuManager.getMenuById(this.typesMenuId);

	      if (createdMenu && main_core.Type.isDomNode(this._domNodes.addressTypeSelector)) {
	        createdMenu.getPopupWindow().setWidth(this._domNodes.addressTypeSelector.offsetWidth);
	      }
	    }
	  }, {
	    key: "closeTypesMenu",
	    value: function closeTypesMenu() {
	      var menu = main_popup.MenuManager.getMenuById(this.typesMenuId);

	      if (menu) {
	        menu.close();
	      }
	    }
	  }, {
	    key: "getEditHtml",
	    value: function getEditHtml(addressString) {
	      this._domNodes.typeName = main_core.Tag.render(_templateObject5());
	      this._domNodes.searchInput = main_core.Tag.render(_templateObject6(), addressString, this._isAutocompleteEnabled ? '' : 'readonly');
	      this._domNodes.icon = main_core.Tag.render(_templateObject7());
	      this._domNodes.addressContainer = main_core.Tag.render(_templateObject8(), this._domNodes.icon, this._domNodes.searchInput);
	      this._domNodes.detailsContainer = main_core.Tag.render(_templateObject9());

	      if (this._canChangeType) {
	        this._domNodes.addressTypeSelector = main_core.Tag.render(_templateObject10(), this.onToggleTypesMenu.bind(this), this._domNodes.typeName);
	        this._domNodes.addressTypeContainer = main_core.Tag.render(_templateObject11(), main_core.Loc.getMessage('CRM_ADDRESS_TYPE'), this._domNodes.addressTypeSelector);
	        this.refreshTypeName();
	      }

	      this.refreshIcon();
	      this._domNodes.detailsToggler = main_core.Tag.render(_templateObject12(), this.onToggleDetailsVisibility.bind(this));
	      this.setDetailsVisibility(this._showDetails);
	      var result = main_core.Tag.render(_templateObject13(), this._domNodes.detailsToggler, this._domNodes.addressContainer, this._domNodes.detailsContainer);

	      if (this._canChangeType) {
	        main_core.Dom.append(this._domNodes.addressTypeContainer, result);
	      }

	      return result;
	    }
	  }, {
	    key: "getViewHtml",
	    value: function getViewHtml(addressString) {
	      this._domNodes.addressContainer = main_core.Tag.render(_templateObject14(), addressString);
	      return main_core.Tag.render(_templateObject15(), this._domNodes.addressContainer);
	    }
	  }, {
	    key: "refreshTypeName",
	    value: function refreshTypeName() {
	      var _this3 = this;

	      if (main_core.Type.isDomNode(this._domNodes.typeName)) {
	        this._domNodes.typeName.textContent = this._typesList.filter(function (item) {
	          return item.value === _this3._type;
	        }).map(function (item) {
	          return item.name;
	        }).join('');
	      }
	    }
	  }, {
	    key: "refreshIcon",
	    value: function refreshIcon() {
	      var node = this._domNodes.icon;

	      if (main_core.Type.isDomNode(node)) {
	        var newNode;

	        if (this._isLoading) {
	          newNode = main_core.Tag.render(_templateObject16());
	        } else {
	          var address = this.getAddress();

	          if (address) {
	            newNode = main_core.Tag.render(_templateObject17(), this.onDelete.bind(this));
	          } else {
	            newNode = main_core.Tag.render(_templateObject18(), this._isAutocompleteEnabled ? 'ui-ctl-icon-search' : '');
	          }
	        }

	        main_core.Dom.replace(node, newNode);
	        this._domNodes.icon = newNode;
	      }
	    }
	  }, {
	    key: "convertAddressToString",
	    value: function convertAddressToString(address) {
	      if (!address) {
	        return '';
	      }

	      return address.toString(this.getAddressFormat());
	    }
	  }, {
	    key: "getAddress",
	    value: function getAddress() {
	      return main_core.Type.isNull(this._addressWidget) ? null : this._addressWidget.address;
	    }
	  }, {
	    key: "getAddressFormat",
	    value: function getAddressFormat() {
	      return main_core.Type.isNull(this._addressWidget) ? null : this._addressWidget.addressFormat;
	    }
	  }, {
	    key: "clearValue",
	    value: function clearValue() {
	      if (!main_core.Type.isNull(this._addressWidget)) {
	        this._addressWidget.resetView();

	        this._addressWidget.address = null;
	      }

	      if (main_core.Type.isDomNode(this._domNodes.searchInput)) {
	        this._domNodes.searchInput.value = '';
	      }

	      this._value = "";
	      this._isLoading = false;
	      this.refreshIcon();
	    }
	  }, {
	    key: "setDetailsVisibility",
	    value: function setDetailsVisibility(visible) {
	      this._showDetails = !!visible;

	      if (this._showDetails) {
	        main_core.Dom.addClass(this._domNodes.detailsContainer, 'visible');

	        if (main_core.Type.isDomNode(this._domNodes.detailsToggler)) {
	          this._domNodes.detailsToggler.textContent = main_core.Loc.getMessage('CRM_ADDRESS_MODE_SHORT');
	        }

	        if (this._canChangeType) {
	          main_core.Dom.addClass(this._domNodes.addressTypeContainer, 'visible');
	        }
	      } else {
	        main_core.Dom.removeClass(this._domNodes.detailsContainer, 'visible');

	        if (main_core.Type.isDomNode(this._domNodes.detailsToggler)) {
	          this._domNodes.detailsToggler.textContent = main_core.Loc.getMessage('CRM_ADDRESS_MODE_DETAILED');
	        }

	        if (this._canChangeType) {
	          main_core.Dom.removeClass(this._domNodes.addressTypeContainer, 'visible');
	        }
	      }
	    }
	  }, {
	    key: "destroy",
	    value: function destroy() {
	      if (!main_core.Type.isNull(this._addressWidget)) {
	        this._addressWidget.destroy();
	      }
	    }
	  }, {
	    key: "onToggleDetailsVisibility",
	    value: function onToggleDetailsVisibility() {
	      this.setDetailsVisibility(!this._showDetails);
	    }
	  }, {
	    key: "onDelete",
	    value: function onDelete() {
	      this.clearValue();
	      this.emit('onUpdateAddress', {
	        id: this.getId(),
	        value: this.getValue()
	      });
	    }
	  }, {
	    key: "onToggleTypesMenu",
	    value: function onToggleTypesMenu(event) {
	      if (this._isTypesMenuOpened) {
	        this.closeTypesMenu();
	      } else {
	        this.openTypesMenu(event.target);
	      }
	    }
	  }, {
	    key: "onChangeType",
	    value: function onChangeType(e, item) {
	      this.closeTypesMenu();

	      if (this._type !== item.value) {
	        var prevType = this._type;
	        this._type = item.value;
	        this.refreshTypeName();
	        this.emit('onUpdateAddressType', {
	          id: this.getId(),
	          type: this.getType(),
	          prevType: prevType
	        });
	      }
	    }
	  }, {
	    key: "onAddressWidgetChangedState",
	    value: function onAddressWidgetChangedState(event) {
	      var data = event.getData(),
	          state = data.state;
	      var wasLoading = this._isLoading;
	      this._isLoading = state === BX.Location.Widget.State.DATA_LOADING;

	      if (wasLoading !== this._isLoading) {
	        this.refreshIcon();
	      }

	      if (state === BX.Location.Widget.State.DATA_LOADING) {
	        this.emit('onStartLoadAddress', {
	          id: this.getId()
	        });
	      }

	      if (state === BX.Location.Widget.State.DATA_LOADED) {
	        this.emit('onAddressLoaded', {
	          id: this.getId()
	        });
	      }
	    }
	  }, {
	    key: "onAddressChanged",
	    value: function onAddressChanged(event) {
	      this._isLoading = false;
	      var data = event.getData();
	      this._value = main_core.Type.isObject(data.address) ? data.address.toJson() : '';
	      this.refreshIcon();
	      this.emit('onUpdateAddress', {
	        id: this.getId(),
	        value: this.getValue()
	      });
	    }
	  }, {
	    key: "onError",
	    value: function onError(event) {
	      var data = event.getData();
	      var errors = data.errors;
	      var errorMessage = errors.map(function (error) {
	        return error.message + (error.code.length ? "".concat(error.code) : '');
	      }).join(', ');
	      this._isLoading = false;
	      this.refreshIcon();
	      this.emit('onError', {
	        id: this.getId(),
	        error: errorMessage
	      });
	    }
	  }]);
	  return AddressItem;
	}(main_core_events.EventEmitter);

	exports.EntityEditorBaseAddressField = EntityEditorBaseAddressField;

}((this.BX.Crm = this.BX.Crm || {}),BX,BX.Event,BX.Main));
//# sourceMappingURL=address.bundle.js.map
