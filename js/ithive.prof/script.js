BX.namespace("BX.ItHiveProf");

if(typeof(BX.ItHiveProf.DealEventsHandler) === "undefined")
{
    /**
     * constructor
     * @param settings
     */
    BX.ItHiveProf.DealEventsHandler = function()
    {
        this._settings = {};
    };

    BX.ItHiveProf.DealEventsHandler.prototype =
        {
            /**
             * initialize
             * @param settings
             */
            initialize: function(settings)
            {
                this._settings = settings ? settings : {};
            },
            /**
             * return config object
             * @returns {*}
             */
            getConfig: function()
            {
                return this._settings;
            },
            /**
             * SidePanel onLoad event listener
             */
            sidePanelOnLoadHandler: function(){
                var closeButton = BX.findChild(window.parent.document, {
                        "tag" : "div",
                        "class" : "side-panel-label"
                    }, true
                );
                if(closeButton){
                    var closeButtonText = BX.findChild(closeButton, {
                            "tag" : "span",
                            "class" : "side-panel-label-text"
                        }, true
                    )
                    if(closeButtonText){
                        closeButtonText.innerHTML = BX.message("ITHIVE_PROF_DEAL_BUTTON_CLOSE_TITLE_VALUE");
                    }
                }
            },

            /**
             * Method for deal tabs sort change
             */
            changeTabsPositions: function() {
                var config = this.getConfig();
                if(config){
                    var tabs = BX.findChild(document, {
                            "tag" : "div",
                            "class" : "crm-entity-section-tabs"
                        }, true
                    );
                    if(!tabs){
                        return false
                    }
                    var tabFirstLink = BX.findChild(tabs, {
                            "tag" : "li",
                        }, true
                    )
                    if(tabFirstLink){
                        var PNP_tab = document.querySelector('.crm-entity-section-tabs [data-tab-id="tab_lists_' + config.PNP_LIST_ID + '"]');
                        var PVH_tab = document.querySelector('.crm-entity-section-tabs [data-tab-id="tab_lists_' + config.PVH_LIST_ID + '"]');
                        if(PNP_tab){
                            var PNP_tab_clone = PNP_tab.cloneNode(true);
                            BX.insertAfter(PNP_tab_clone, tabFirstLink);
                            PNP_tab.remove();
                        }
                        if(PVH_tab){
                            var PVH_tab_clone = PVH_tab.cloneNode(true);
                            BX.insertAfter(PVH_tab_clone, tabFirstLink);
                            PVH_tab.remove();
                        }
                    }
                }
            }
        };

    BX.ItHiveProf.DealEventsHandler.config = null;
    BX.ItHiveProf.DealEventsHandler.current = null;

    /**
     * Return current instance
     */
    BX.ItHiveProf.DealEventsHandler.getCurrent = function()
    {
        if(!this.current && this.config){
            this.current = new BX.ItHiveProf.DealEventsHandler();
            this.current.initialize(BX.prop.getObject(this.config, "settings"));
        }
        return this.current;
    };
}

BX.ready(function(){
    var dealHandler = BX.ItHiveProf.DealEventsHandler.getCurrent();
    // close button text replace
    BX.addCustomEvent("SidePanel.Slider:onLoad", dealHandler.sidePanelOnLoadHandler());
    // change tabs
    dealHandler.changeTabsPositions();
});