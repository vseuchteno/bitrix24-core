<?
$MESS["CRM_ADD_MESSAGE"] = "���������� ������� � CRM";
$MESS["CRM_EMAIL_GET_EMAIL"] = "������ � �����";
$MESS["CRM_EMAIL_SUBJECT"] = "���������";
$MESS["CRM_EMAIL_EMAILS"] = "E-mail";
$MESS["CRM_EMAIL_FROM"] = "From";
$MESS["CRM_EMAIL_TO"] = "To";
$MESS["CRM_EMAIL_BANNENED_ATTACHMENTS"] = "��������������� ����� (�������� ������������ ������: %MAX_SIZE% ��)";
$MESS["CRM_EMAIL_BANNENED_ATTACHMENT_INFO"] = "%NAME% (%SIZE% ��)";
$MESS["CRM_MAIL_COMPANY_NAME"] = "�������� ��������: %TITLE%";
$MESS["CRM_MAIL_LEAD_FROM_EMAIL_TITLE"] = "��� �� ������ �� %SENDER%";
$MESS["CRM_MAIL_LEAD_FROM_USER_EMAIL_TITLE"] = "��� �� ����������������� ������ �� %SENDER%";
$MESS["CRM_MAIL_LEAD_FROM_EMAIL_SOURCE"] = "������ �� ������ �� %SENDER%, ������� �� ������� ������� � ������-���� ������������� ����, �������� ��� ��������";
$MESS["CRM_EMAIL_CODE_ALLOCATION_BODY"] = "��������� � ���������";
$MESS["CRM_EMAIL_CODE_ALLOCATION_SUBJECT"] = "��������� � ����";
$MESS["CRM_EMAIL_CODE_ALLOCATION_NONE"] = "�� ������������";
$MESS["CRM_EMAIL_DEFAULT_SUBJECT"] = "(��� ����)";
$MESS["CRM_EMAIL_BAD_RESP_QUEUE"] = "� ������� �� ������������� ����� �� ����� \"#EMAIL#\" ������ ��������� ���������. ����� ������ �� �������������� �� ����, �� ��� ������ ��������� �� �������� ����� ���������� ����������. <a href=\"#CONFIG_URL#\">��������� ��������� �������</a>.";

$MESS["CRM_EMAIL_IMAP_ERROR_EMPLOYE_IN"] = "������ �� ����������� �� ����� ���� ��������� � CRM";
$MESS["CRM_EMAIL_IMAP_ERROR_EMPLOYE_OUT"] = "������ ����������� �� ����� ���� ��������� � CRM";
