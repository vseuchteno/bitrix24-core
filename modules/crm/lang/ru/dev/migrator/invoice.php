<?php
$MESS["CRM_ORDER_STATUS_N"] = "������, ��������� ������";
$MESS["CRM_ORDER_STATUS_P"] = "�������, ����������� � ��������";
$MESS["CRM_ORDER_STATUS_F"] = "��������";
$MESS["CRM_ORDER_STATUS_D"] = "�������";
$MESS["CRM_ORDER_SHIPMENT_STATUS_DN"] = "������� ���������";
$MESS["CRM_ORDER_SHIPMENT_STATUS_DF"] = "��������";
$MESS["CRM_ORDER_SHIPMENT_STATUS_DD"] = "�������";

$MESS["SALE_USER_GROUP_SHOP_BUYER_NAME"] = "��� ����������";
$MESS["SALE_USER_GROUP_SHOP_BUYER_DESC"] = "������ �������������, ���������� ���� ����������� ��������";

$MESS["SALE_USER_GROUP_SHOP_ADMIN_NAME"] = "�������������� ��������";
$MESS["SALE_USER_GROUP_SHOP_MANAGER_NAME"] = "��������� ��������";
$MESS["SALE_USER_GROUP_SHOP_ADMIN_DESC"] = "������ �������������, ������� ����� ����������� �������";
$MESS["SALE_USER_GROUP_SHOP_MANAGER_DESC"] = "������ �������������, ������� ����� �������� � ���������";

//region Enable sale events
$MESS["UP_TYPE_SUBJECT"] = "����������� � ����������� ������";
$MESS["UP_TYPE_SUBJECT_DESC"] = "#USER_NAME# - ��� ������������
#EMAIL# - email ������������
#NAME# - �������� ������
#PAGE_URL# - ��������� �������� ������";
$MESS["SMAIL_FOOTER_BR"] = "� ���������,<br />�������������";
$MESS["SMAIL_FOOTER_SHOP"] = "��������-��������";

$MESS["SALE_CHECK_PRINT_ERROR_TYPE_NAME"] = "����������� �� ������ ��� ������ ����";
$MESS["SALE_CHECK_PRINT_ERROR_TYPE_DESC"] = "#ORDER_ACCOUNT_NUMBER# - ��� ������
#ORDER_DATE# - ���� ������
#ORDER_ID# - ID ������
#CHECK_ID# - ����� ����";
$MESS["SALE_CHECK_PRINT_ERROR_SUBJECT"] = "������ ��� ������ ����";
$MESS["SALE_CHECK_PRINT_ERROR_HTML_TITLE"] = "������ ��� ������ ����";
$MESS["SALE_CHECK_PRINT_ERROR_HTML_SUB_TITLE"] = "������������!";
$MESS["SALE_CHECK_PRINT_ERROR_HTML_TEXT"] = "
�� �����-�� ������� ��� �#CHECK_ID# �� ������ �#ORDER_ACCOUNT_NUMBER# �� #ORDER_DATE# �� ������� �����������!

��������� �� ������, ����� ��������� ������� ��������� ��������:
#LINK_URL#
";
//endregion Enable sale events
