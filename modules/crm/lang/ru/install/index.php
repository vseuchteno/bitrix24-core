<?
$MESS["CRM_INSTALL_NAME"] = "CRM";
$MESS["CRM_TOP_LINKS_ITEM_NAME"] = "CRM";
$MESS["CRM_INSTALL_DESCRIPTION"] = "������ ���� ����������� �������� CRM";
$MESS["CRM_INSTALL_TITLE"] = "��������� ������ crm";
$MESS["CRM_UNINSTALL_TITLE"] = "�������� ������ crm";
$MESS["CRM_PAGE_FUNNEL"] = "������� ������";
$MESS["CRM_GADGET_MY_LEAD_TITLE"] = "��� ����";
$MESS["CRM_GADGET_NEW_LEAD_TITLE"] = "����� ����";
$MESS["CRM_GADGET_CLOSED_DEAL_TITLE"] = "����������� ������";
$MESS["CRM_GADGET_LAST_EVENT_TITLE"] = "��������� �������";
$MESS["CRM_GADGET_NEW_CONTACT_TITLE"] = "����� ��������";
$MESS["CRM_GADGET_NEW_COMPANY_TITLE"] = "����� ��������";
$MESS["CRM_PERM_R"] = "������ �������";
$MESS["CRM_PERM_W"] = "��������/��������� �������";
$MESS["CRM_PERM_D"] = "������ ������";
$MESS["CRM_PERM_X"] = "��������/��������� ������� � ���������� ���������";
$MESS["CRM_PERM_Y"] = "�������� ������� � �������";
$MESS["CRM_PERM_Z"] = "��������� ����";

$MESS["CRM_CONTACT_TYPE_SHARE"] = "����� ��������";
$MESS["CRM_CONTACT_TYPE_JOURNALIST"] = "����������";
$MESS["CRM_CONTACT_TYPE_CLIENT"] = "�������";
$MESS["CRM_CONTACT_TYPE_SUPPLIER"] = "����������";
$MESS["CRM_CONTACT_TYPE_PARTNER"] = "��������";

$MESS["CRM_STATUS_TYPE_SOURCE_SELF"] = "���� �������";
$MESS["CRM_STATUS_TYPE_SOURCE_PARTNER"] = "������������ ������";
$MESS["CRM_STATUS_TYPE_SOURCE_CALL"] = "������";
$MESS["CRM_STATUS_TYPE_SOURCE_WEB"] = "���-����";
$MESS["CRM_STATUS_TYPE_SOURCE_WEB_FORM"] = "���-�����";
$MESS["CRM_STATUS_TYPE_SOURCE_EMAIL"] = "����������� �����";
$MESS["CRM_STATUS_TYPE_SOURCE_CONFERENCE"] = "�����������";
$MESS["CRM_STATUS_TYPE_SOURCE_TRADE_SHOW"] = "��������";
$MESS["CRM_STATUS_TYPE_SOURCE_EMPLOYEE"] = "���������";
$MESS["CRM_STATUS_TYPE_SOURCE_COMPANY"] = "��������";
$MESS["CRM_STATUS_TYPE_SOURCE_HR"] = "HR - �����������";
$MESS["CRM_STATUS_TYPE_SOURCE_MAIL"] = "������";
$MESS["CRM_STATUS_TYPE_SOURCE_OTHER"] = "������";
$MESS["CRM_COMPANY_TYPE_CUSTOMER"] = "������";
$MESS["CRM_COMPANY_TYPE_PARTNER"] = "�������";
$MESS["CRM_COMPANY_TYPE_RESELLER"] = "��������";
$MESS["CRM_COMPANY_TYPE_COMPETITOR"] = "���������";
$MESS["CRM_COMPANY_TYPE_INVESTOR"] = "��������";
$MESS["CRM_COMPANY_TYPE_INTEGRATOR"] = "����������";
$MESS["CRM_COMPANY_TYPE_PROSPECT"] = "�����������";
$MESS["CRM_COMPANY_TYPE_PRESS"] = "���";
$MESS["CRM_COMPANY_TYPE_OTHER"] = "������";
$MESS["CRM_INDUSTRY_IT"] = "�������������� ����������";
$MESS["CRM_INDUSTRY_TELECOM"] = "���������������� � �����";
$MESS["CRM_INDUSTRY_MANUFACTURING"] = "������������";
$MESS["CRM_INDUSTRY_BANKING"] = "���������� ������";
$MESS["CRM_INDUSTRY_CONSULTING"] = "����������";
$MESS["CRM_INDUSTRY_FINANCE"] = "�������";
$MESS["CRM_INDUSTRY_GOVERNMENT"] = "�������������";
$MESS["CRM_INDUSTRY_DELIVERY"] = "��������";
$MESS["CRM_INDUSTRY_ENTERTAINMENT"] = "�����������";
$MESS["CRM_INDUSTRY_NOTPROFIT"] = "�� ��� ��������� �������";
$MESS["CRM_INDUSTRY_OTHER"] = "������";
$MESS["CRM_DEAL_TYPE_SALE"] = "�������";
$MESS["CRM_DEAL_TYPE_COMPLEX"] = "����������� �������";
$MESS["CRM_DEAL_TYPE_GOODS"] = "������� ������";
$MESS["CRM_DEAL_TYPE_SERVICES"] = "������� ������";
$MESS["CRM_DEAL_TYPE_SERVICE"] = "��������� ������������";
$MESS["CRM_DEAL_STATE_PLANNED"] = "� ������";
$MESS["CRM_DEAL_STATE_PROCESS"] = "� ������";
$MESS["CRM_DEAL_STATE_COMPLETE"] = "���������";
$MESS["CRM_DEAL_STATE_CANCELED"] = "��������";
$MESS["CRM_EVENT_TYPE_INFO"] = "����������";
$MESS["CRM_EVENT_TYPE_MESSAGE"] = "��������� email";
$MESS["CRM_EVENT_TYPE_PHONE"] = "���������� ������";
$MESS["CRM_UF_NAME"] = "�������� CRM";
$MESS["CRM_UF_NAME_CAL"] = "�������� CRM";
$MESS["CRM_UF_NAME_LF_TYPE"] = "��� �������� CRM ��� ��";
$MESS["CRM_UF_NAME_LF_ID"] = "ID �������� CRM ��� ��";
$MESS["CRM_ROLE_ADMIN"] = "�������������";
$MESS["CRM_ROLE_DIRECTOR"] = "��������";
$MESS["CRM_ROLE_CHIF"] = "��������� ������";
$MESS["CRM_ROLE_MAN"] = "��������";
$MESS["CRM_SALE_STATUS_A"] = "�����������";
$MESS["CRM_SALE_STATUS_D"] = "��������";
$MESS["CRM_SALE_STATUS_P"] = "�������";
$MESS["CRM_SALE_STATUS_S"] = "��������� �������";
$MESS["CRM_VAT_1"] = "��� ���";
$MESS["CRM_VAT_2"] = "��� 18%";
$MESS["CRM_ORD_PROP_2"] = "��������������";
$MESS["CRM_ORD_PROP_21"] = "�����";
$MESS["CRM_ORD_PROP_4"] = "������";
$MESS["CRM_ORD_PROP_5"] = "����� ��������";
$MESS["CRM_ORD_PROP_6"] = "�.�.�.";
$MESS["CRM_ORD_PROP_7"] = "����������� �����";
$MESS["CRM_ORD_PROP_8"] = "�������� ��������";
$MESS["CRM_ORD_PROP_9"] = "�������";
$MESS["CRM_ORD_PROP_10"] = "���������� ����";
$MESS["CRM_ORD_PROP_11"] = "����";
$MESS["CRM_ORD_PROP_12"] = "����� ��������";
$MESS["CRM_ORD_PROP_13"] = "���";
$MESS["CRM_ORD_PROP_14"] = "���";
$MESS["CRM_ORD_PROP_GROUP_FIZ1"] = "������ ������";
$MESS["CRM_ORD_PROP_GROUP_FIZ2"] = "������ ��� ��������";
$MESS["CRM_ORD_PROP_GROUP_UR1"] = "������ ��������";
$MESS["CRM_ORD_PROP_GROUP_UR2"] = "���������� ����������";

$MESS["CRM_EMAIL_CONFIRM_TYPE_NAME"] = "������������� email-������ �����������";
$MESS["CRM_EMAIL_CONFIRM_TYPE_DESC"] = "

#EMAIL# - Email-����� ��� �������������
#CONFIRM_CODE# - ��� �������������";

$MESS["CRM_EMAIL_CONFIRM_EVENT_NAME"] = "������������� email-������";
$MESS["CRM_EMAIL_CONFIRM_EVENT_DESC"] = "<span style=\"font-size:16px;line-height:20px;\">
��� ������������� email-������ ������� ������ ��� � ����� �������24.<br>

<span style=\"font-size:24px;line-height:70px;\"><b>#CONFIRM_CODE#</b></span><br>

<span style=\"color:#808080;\">
��� ���� ����� ������������ email?<br><br>
<span style=\"font-size:14px;\">��� ����� ��������� ����� �����������, ����� ��������� ��������� ��������, � ��� ������, ���� ������� �������� ����� �� ���������� �����. ����� ����, �� ����� ���� �������� ���������� �������� ������� � �� ���������� ����������, ������� ����� ��������� ������.</span>
</span>";
$MESS["CRM_UNINS_MODULE_SALE"] = "��� ������ ������ CRM ��������� ������ \"��������-�������\".<br />���������� ������� ����������� ������.";