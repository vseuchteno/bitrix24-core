<?
$MESS["CRM_ACTIVITY_VISIT_TITLE"] = "�����";
$MESS["CRM_ACTIVITY_VISIT_BUTTON_FINISH"] = "���������";
$MESS["CRM_ACTIVITY_VISIT_OWNER_SELECT"] = "�������";
$MESS["CRM_ACTIVITY_VISIT_OWNER_CHANGE"] = "�������";
$MESS["CRM_ACTIVITY_VISIT_OWNER_TAKE_PICTURE"] = "������� ����";
$MESS["CRM_ACTIVITY_VISIT_OWNER_RETAKE_PICTURE"] = "��������� ����";
$MESS["CRM_ACTIVITY_VISIT_ERROR_MIC_FAILURE"] = "������ ��������� ������� � ���������. ����� ������:";
$MESS["CRM_ACTIVITY_VISIT_CONSENT_TITLE"] = "������� ������������� �������";
$MESS["CRM_ACTIVITY_VISIT_CONSENT_BODY_1"] = "�������� ��������! �����-������ ����� ������ ��������� � ����� ��������.";
$MESS["CRM_ACTIVITY_VISIT_CONSENT_BODY_2"] = "���������, ��� ������������� ������ ����� �� ������������ ������������ ���������������� ����� ������ � ���������� ����� ��������.";
$MESS["CRM_ACTIVITY_VISIT_CONSENT_AGREED"] = "� ��������";
$MESS["CRM_ACTIVITY_VISIT_CONSENT_CLOSE"] = "�������";
$MESS["CRM_ACTIVITY_VISIT_DEFAULT_CAMERA"] = "������ �� ���������";
$MESS["CRM_ACTIVITY_VISIT_FACEID_AGREEMENT"] = "<div class=\"tracker-agreement-popup-content\">
			<ol class=\"tracker-agreement-popup-list\">
				<li class=\"tracker-agreement-popup-list-item\">
					������ FindFace (����� � ������) ��������������� ��������� N-TECH.LAB LTD �� �������� ��������������� <a target=\"_blank\" href=\"https://saas.findface.pro/files/docs/license_us.pdf\" class=\"tracker-link\">�����������</a> �, <span class=\"tracker-text-bold\">������� ������ �� ���������, �� ��������� � ������������ ���������:</span>
					<ul class=\"tracker-agreement-popup-inner-list\">
						<li class=\"tracker-agreement-popup-inner-list-item\">
							<span class=\"tracker-text-bold\">�� ���������� �������������� ��������� ���������� ���������������� � ������� ������ ������������ ������, ������� �����</span> � �.�., ������������ � ����� ������ (����� � ���������� ����������������), � ������� ����� ����� ������������� �������;
						</li>
						<li class=\"tracker-agreement-popup-inner-list-item\">
							�� <span class=\"tracker-text-bold\">������������ ���������</span> ����������� ��� ����� ������������ ��������, <span class=\"tracker-text-bold\">�� ��� ������ ������� �� �������������</span> � �������������� �������, �������  �� �����������;
						</li>
						<li class=\"tracker-agreement-popup-inner-list-item\">
							�� ������ �� ������ ������������� ������� <span class=\"tracker-text-bold\">�������������������� � ���������������� �������������</span> (��������) � ������, ���� � ��� ��������� ������� � ������������� ��������� �������� � �� �����;
						</li>
						<li class=\"tracker-agreement-popup-inner-list-item\">
							�� ������������, ��� <span class=\"tracker-text-bold\">����� �����������</span> � ������� ������� <span class=\"tracker-text-bold\">���������� ����</span> ���� <span class=\"tracker-text-bold\">�������� � �����������</span> ����������� <span class=\"tracker-text-bold\">����������������</span>;
						</li>
						<li class=\"tracker-agreement-popup-inner-list-item\">
							����� <span class=\"tracker-text-bold\">������������� ������� ��������������</span> ���� �� ���� ����� � ���� � <span class=\"tracker-text-bold\">��� ����������� ���������������.</span>
						</li>
					</ul>
				</li>
				<li class=\"tracker-agreement-popup-list-item\">
					�������� <span class=\"tracker-text-bold\">1�-������� �� ����� ���������������</span> �� ������ �������, �� ������������� ������� �������� ��� ����������� � ��������� ������������, �������� ��� ������������� �����������, ������� ����� ���� �������� � ��� ��������������, � ��� �� <span class=\"racker-text-bold\">�� ����� ������� ������������ �� ��� ������������ � ���������.</span>
				</li>
			</ol>
			<div class=\"tracker-agreement-popup-description\">
				�������� ������� �������, �� ������������ � �������������, ��� �������� <span class=\"tracker-text-bold\">1�-������� �� ��������, �� ��������� ��������� �� ���������, �� ������������ � �� �������� ����������, ����������� ���� ����� ������, � ��� �� �� �������� ����������� �������� ����� ����������,</span> �� ����� � �� ������ ����� � ��������� �� ���������������.
			</div>
		</div>";
$MESS["CRM_ACTIVITY_VISIT_FACEID_SOCIAL_SEACH_TITLE"] = "����� ������� ���������";
$MESS["CRM_ACTIVITY_VISIT_FACEID_SOCIAL_SEACH_IN_PROCESS"] = "���� ����� ������";
?>