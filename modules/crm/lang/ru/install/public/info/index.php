<?
$MESS["CRM_PAGE_TITLE"] = "�������";
$MESS["CRM_PAGE_CONTENT"] = "<h3>��� ����� CRM?</h3>
 
<p><b>������� ���������� ��������������� � ���������</b> (��� <b>CRM</b>) &mdash; �������, ��������������� ��� ��������� ������ ������, ����������� ���������� � ��������� ������������ ��������. � CRM ����������� ���������� � �������� � ������� ��������������� � ���� ��� ������������ ������� �����������.</p>
 
<p>� ������� CRM �������� ������ � ������������� �������� ��������� ����������� �� ���� � ����������� ������. </p>
 
<p>��� ������ ����������� � ���������:</p>
 
<ul> 
  <li><b>�������</b> - ������ � ��������, � ������� ���� ������� �� ������ ���������������� ������������, �������� �������� ������� �������. </li>
 
  <li><b>��������</b> - ������ � ��������, ������� ����� �����-�� ����� �������������� (���� ����� ����� ��������������) � ����� ���������. </li>
 
  <li><b>���</b> - ������ � ����� ���� ����� �������� (�������, e-mail, ����� ������� � ��� �����), ������� ����� ������������� ����������� ��������� � ������. </li>
 
  <li><b>�������</b> - ����� ��������� � ������� �������, ���, ��������, ��������: ���������� ������ ������ ����������� �����. </li>
 
  <li><b>������</b> - ������ � ������ � ��������, ��������� �� ������ ������� ������. </li>
 </ul>
 
<h3>��� ��� ��������?</h3>
 
<p><b>CRM</b> ����� ������������ � ���� ���������:</p>
 
<ol> 
  <li>��� <b>���� ������</b> �� ��������� � ���������,</li>
 
  <li>��� ������������ <b>CRM</b></li>
 </ol>
 
<h4>1. CRM ��� ���� ������</h4>
 
<p>������������� CRM ��� ���� ������ �� ��������� � ��������� ��������� ����� ������� ��������������� � ����. �������� ��������� � ���� ������ �������� ������� � ��������, � ������� � ������� ������� ������� ������� ���������������. ������������� CRM ��� ���� ������ �� ��������� �������� � ���������� ��������� ������� ��������� ����� � ����������� �� � ������.</p>
 
<p><img height='430' border='0' width='900' src='/upload/crm/cim/01.png'  /></p>
 
<h4>2. ������������ CRM</h4>
 
<p>� ������������ CRM �������� ��������� �������� ���, ������� ����������� � ������� ������� ����������, ������������� �� &quot;1�-�������: ���������� ������&quot; ��� ����� ����������. ��� ����� ���������� � ��������� ����� ���� ������������� � ��������, �������� ��� ������. � ������ ���� ������� ��� ���������� ������� ��������� ���� ������. �� ������ ������, ��� ����������� ������� ������ ���������� �������� ��������.</p>
 
<p><img height='430' border='0' width='900' src='/upload/crm/cim/03.png'  /></p>";
?>