<?
$MESS["CRM_CONV_EX_LEAD_NOT_FOUND"] = "��� �� ������.";
$MESS["CRM_CONV_EX_CONTACT_NOT_FOUND"] = "������� �� ������.";
$MESS["CRM_CONV_EX_COMPANY_NOT_FOUND"] = "�������� �� �������.";
$MESS["CRM_CONV_EX_DEAL_NOT_FOUND"] = "������ �� �������.";

$MESS["CRM_CONV_EX_CONTACT_EMPTY_FIELDS"] = "�� ������� ��������� �� ������ ���� ��� ��������.";
$MESS["CRM_CONV_EX_COMPANY_EMPTY_FIELDS"] = "�� ������� ��������� �� ������ ���� ��� ��������.";
$MESS["CRM_CONV_EX_DEAL_EMPTY_FIELDS"] = "�� ������� ��������� �� ������ ���� ��� ������.";

$MESS["CRM_CONV_EX_CONTACT_INVALID_FIELDS"] = "���� �������� ��������� �����������.\r\n��������� ��������:\r\n";
$MESS["CRM_CONV_EX_COMPANY_INVALID_FIELDS"] = "���� �������� ��������� �����������.\r\n��������� ��������:\r\n";
$MESS["CRM_CONV_EX_DEAL_INVALID_FIELDS"] = "���� ������ ��������� �����������.\r\n��������� ��������:\r\n";

$MESS["CRM_CONV_EX_CONTACT_CREATE_DENIED"] = "�������� �������� ���������.";
$MESS["CRM_CONV_EX_COMPANY_CREATE_DENIED"] = "�������� �������� ���������.";
$MESS["CRM_CONV_EX_DEAL_CREATE_DENIED"] = "�������� ������ ���������.";
$MESS["CRM_CONV_EX_INVOICE_CREATE_DENIED"] = "�������� ����� ���������.";

$MESS["CRM_CONV_EX_LEAD_READ_DENIED"] = "������ ���� ���������.";
$MESS["CRM_CONV_EX_CONTACT_READ_DENIED"] = "������ �������� ���������.";
$MESS["CRM_CONV_EX_COMPANY_READ_DENIED"] = "������ �������� ���������.";
$MESS["CRM_CONV_EX_DEAL_READ_DENIED"] = "������ ������ ���������.";

$MESS["CRM_CONV_EX_LEAD_UPDATE_DENIED"] = "���������� ���� ���������.";
$MESS["CRM_CONV_EX_CONTACT_UPDATE_DENIED"] = "���������� �������� ���������.";
$MESS["CRM_CONV_EX_COMPANY_UPDATE_DENIED"] = "���������� �������� ���������.";
$MESS["CRM_CONV_EX_DEAL_UPDATE_DENIED"] = "���������� ������ ���������.";

$MESS["CRM_CONV_EX_CONTACT_CREATE_FAILED"] = "�� ������� ������� �������.\r\n��������� ��������:\r\n";
$MESS["CRM_CONV_EX_COMPANY_CREATE_FAILED"] = "�� ������� ������� ��������.\r\n��������� ��������:\r\n";
$MESS["CRM_CONV_EX_DEAL_CREATE_FAILED"] = "�� ������� ������� ������.\r\n��������� ��������:\r\n";

$MESS["CRM_CONV_EX_ENTITY_NOT_SUPPORTED"] = "��� #ENTITY_TYPE_NAME# �� ��������������.";

$MESS["CRM_CONV_EX_LEAD_AUTOCREATION_DISABLED"] = "�������������� �������� ����� ���������.";
$MESS["CRM_CONV_EX_CONTACT_AUTOCREATION_DISABLED"] = "�������������� �������� ��������� ���������.";
$MESS["CRM_CONV_EX_COMPANY_AUTOCREATION_DISABLED"] = "�������������� �������� �������� ���������.";
$MESS["CRM_CONV_EX_DEAL_AUTOCREATION_DISABLED"] = "�������������� �������� ������ ���������.";
$MESS["CRM_CONV_EX_INVOICE_AUTOCREATION_DISABLED"] = "�������������� �������� ������ ���������.";
$MESS["CRM_CONV_EX_QUOTE_AUTOCREATION_DISABLED"] = "�������������� �������� ����������� ���������.";

$MESS["CRM_CONV_EX_LEAD_HAS_WORKFLOWS"] = "������� ������-��������, ����������� �� ������ ��� �������� ����. �������� ����������� ������� ������������.";
$MESS["CRM_CONV_EX_CONTACT_HAS_WORKFLOWS"] = "������� ������-��������, ����������� �� ������ ��� �������� ��������. �������� ����������� ������� ������������.";
$MESS["CRM_CONV_EX_COMPANY_HAS_WORKFLOWS"] = "������� ������-��������, ����������� �� ������ ��� �������� ��������. �������� ����������� ������� ������������.";
$MESS["CRM_CONV_EX_DEAL_HAS_WORKFLOWS"] = "������� ������-��������, ����������� �� ������ ��� �������� ������. �������� ����������� ������� ������������.";

$MESS["CRM_CONV_EX_INVALID_OPERATION"] = "�������� ���������.";
$MESS["CRM_CONV_EX_NOT_SYNCHRONIZED"] = "�� �������� �������� ��� ������ �� ����. ����������, ��������� ������������� ���������������� �����.";
?>