<?
$MESS["CRM_REQUISITE_ERR_INVALID_ENTITY_TYPE"] = "������������ ��� �������� ��� ����������";
$MESS["CRM_REQUISITE_ERR_INVALID_ENTITY_ID"] = "�� ������� �������� ��� ����������";
$MESS["CRM_REQUISITE_ERR_ON_DELETE"] = "������ ��� �������� ����������";
$MESS["CRM_REQUISITE_ERR_NOTHING_TO_DELETE"] = "�� ������� ��������� ��� ��������";
$MESS["CRM_REQUISITE_ERR_COMPANY_NOT_EXISTS"] = "�� ������� �������� (ID: #ID#)";
$MESS["CRM_REQUISITE_ERR_CONTACT_NOT_EXISTS"] = "�� ������ ������� (ID: #ID#)";
$MESS["CRM_REQUISITE_ERR_INVALID_IMP_PRESET_ID"] = "�������� ������������� ������� ��� ������� ����������";
$MESS["CRM_REQUISITE_ERR_IMP_PRESET_NOT_EXISTS"] = "�� ������ ������ ��� ������� ���������� (ID: #ID#)";
$MESS["CRM_REQUISITE_ERR_DEF_IMP_PRESET_NOT_DEFINED"] = "������ ��� ������� ���������� �������� \"#ENTITY_TYPE#\" �� ".
	"��������";
$MESS["CRM_REQUISITE_ERR_ACCESS_DENIED_COMPANY_UPDATE"] = "��� ������� �� ��������� �������� (ID: #ID#)";
$MESS["CRM_REQUISITE_ERR_ACCESS_DENIED_CONTACT_UPDATE"] = "��� ������� �� ��������� �������� (ID: #ID#)";
$MESS["CRM_REQUISITE_ERR_NO_ADDRESSES_TO_IMPORT"] = "��� ������� ��� �������";
$MESS["CRM_REQUISITE_ERR_IMP_PRESET_HAS_NO_ADDR_FIELD"] = "������ ��� ������� ���������� �� �������� ���� ������".
	" (ID: #ID#)";
$MESS["CRM_REQUISITE_ERR_DUP_CTRL_MODE_SKIP"] = "� ������ �������� ����������, ��������� �� �������������";
$MESS["CRM_REQUISITE_ERR_CREATE_REQUISITE"] = "������ �������� ���������� ��� #ENTITY_TYPE_NAME_GENITIVE# (ID: #ID#)";
$MESS["CRM_REQUISITE_ERR_COMPANY_GENITIVE"] = "��������";
$MESS["CRM_REQUISITE_ERR_CONTACT_GENITIVE"] = "��������";
$MESS["CRM_REQUISITE_FIXED_PRESET_COMPANY"] = "�����������";
$MESS["CRM_REQUISITE_FIXED_PRESET_LEGALENTITY"] = "��. ����";
$MESS["CRM_REQUISITE_FIXED_PRESET_INDIVIDUAL"] = "��";
$MESS["CRM_REQUISITE_FIXED_PRESET_PERSON"] = "���. ����";
$MESS["CRM_REQUISITE_FILTER_PREFIX"] = "��������";
$MESS["CRM_REQUISITE_EXPORT_FIELD_ID"] = "ID";
$MESS["CRM_REQUISITE_EXPORT_FIELD_PRESET_ID"] = "ID �������";
$MESS["CRM_REQUISITE_EXPORT_FIELD_PRESET_NAME"] = "������";
$MESS["CRM_REQUISITE_EXPORT_FIELD_PRESET_COUNTRY_ID"] = "ID ������";
$MESS["CRM_REQUISITE_EXPORT_FIELD_PRESET_COUNTRY_NAME"] = "������";
$MESS["CRM_REQUISITE_EXPORT_FIELD_NAME"] = "��������";
$MESS["CRM_REQUISITE_EXPORT_FIELD_ACTIVE"] = "�������";
$MESS["CRM_REQUISITE_EXPORT_FIELD_ADDRESS_ONLY"] = "������ �����";
$MESS["CRM_REQUISITE_EXPORT_FIELD_SORT"] = "������� ����������";
$MESS["CRM_REQUISITE_EXPORT_ADDRESS_TYPE_LABEL"] = "���";
$MESS["CRM_BANK_DETAIL_FILTER_PREFIX"] = "���������� ��������";
$MESS["CRM_REQUISITE_FIELD_VALIDATOR_LENGTH_MAX"] = "��� �������� ���� \"#FIELD_TITLE#\" ��������� ������������ �����: #MAX_LENGTH#.";
$MESS["CRM_REQUISITE_FIELD_VALIDATOR_LENGTH_MIN"] = "�������� ���� \"#FIELD_TITLE#\" ������������ �������. ����������� �����: #MIN_LENGTH#.";
?>