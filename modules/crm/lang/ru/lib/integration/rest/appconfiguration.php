<?php
$MESS['CRM_ERROR_CONFIGURATION_IMPORT_CONFLICT_FIELDS'] = '� ��� ��� ���������� ���� � ����� #CODE#, ������� ����� ����� ������ ���, ����������, ������� ��� � ��������� ������ ��������';
$MESS['CRM_ERROR_CONFIGURATION_IMPORT_EXCEPTION'] = '������ �������, �������� ������ �������� (#CODE#)';
$MESS['CRM_ERROR_CONFIGURATION_EXPORT_EXCEPTION'] = '������ ��������, �������� ������� �������� (#CODE#)';
$MESS['CRM_ERROR_CONFIGURATION_CLEAR_EXCEPTION'] = '������ ������� ������, ��������� ������� �������� (#CODE#)';
$MESS["CRM_CONFIGURATION_MANIFEST_TITLE_VERTICAL_CRM"] = "���������� CRM";
$MESS["CRM_CONFIGURATION_MANIFEST_DESCRIPTION_VERTICAL_CRM"] = "�������� ������� CRM ��� ������ ������� �� ��������� �����!";
$MESS["CRM_CONFIGURATION_MANIFEST_PAGE_TITLE_VERTICAL_CRM"] = "������� ���������� CRM";
$MESS["CRM_CONFIGURATION_MANIFEST_BLOCK_TITLE_VERTICAL_CRM"] = "������� ���������� CRM";
$MESS["CRM_CONFIGURATION_MANIFEST_ACTION_DESCRIPTION_VERTICAL_CRM"] = "������� \"��������������\", ����� ��������� ��������� ����� CRM � �����";

$MESS["CRM_CONFIGURATION_MANIFEST_TITLE_CRM_FORM"] = "CRM-�����";
$MESS["CRM_CONFIGURATION_MANIFEST_DESCRIPTION_CRM_FORM"] = "�������� ������� CRM-����� ��� ������ ������� �� ��������� �����!";
$MESS["CRM_CONFIGURATION_MANIFEST_PAGE_TITLE_CRM_FORM"] = "������� CRM-����";
$MESS["CRM_CONFIGURATION_MANIFEST_BLOCK_TITLE_CRM_FORM"] = "������� CRM-����";
$MESS["CRM_CONFIGURATION_MANIFEST_ACTION_DESCRIPTION_CRM_FORM"] = "������� \"��������������\", ����� ��������� ���� CRM-����� � �����";
