<?php
$MESS["CRM_CONFIGURATION_MANIFEST_TITLE_VERTICAL_CRM"] = "���������� CRM";
$MESS["CRM_CONFIGURATION_MANIFEST_DESCRIPTION_VERTICAL_CRM"] = "�������� ������� CRM ��� ������ ������� �� ��������� �����!";
$MESS["CRM_CONFIGURATION_MANIFEST_PAGE_TITLE_VERTICAL_CRM"] = "������� ���������� CRM";
$MESS["CRM_CONFIGURATION_MANIFEST_BLOCK_TITLE_VERTICAL_CRM"] = "������� ���������� CRM";
$MESS["CRM_CONFIGURATION_MANIFEST_ACTION_DESCRIPTION_VERTICAL_CRM"] = "������� \"��������������\", ����� ��������� ��������� ����� CRM � �����";

$MESS["CRM_CONFIGURATION_MANIFEST_TITLE_CRM_FORM"] = "CRM-�����";
$MESS["CRM_CONFIGURATION_MANIFEST_DESCRIPTION_CRM_FORM"] = "�������� ������� CRM-����� ��� ������ ������� �� ��������� �����!";
$MESS["CRM_CONFIGURATION_MANIFEST_PAGE_TITLE_CRM_FORM"] = "������� CRM-����";
$MESS["CRM_CONFIGURATION_MANIFEST_BLOCK_TITLE_CRM_FORM"] = "������� CRM-����";
$MESS["CRM_CONFIGURATION_MANIFEST_ACTION_DESCRIPTION_CRM_FORM"] = "������� \"��������������\", ����� ��������� ���� CRM-����� � �����";

$MESS["CRM_CONFIGURATION_MANIFEST_TITLE_CRM_SMART_ROBOTS"] = "����� ��������";
$MESS["CRM_CONFIGURATION_MANIFEST_DESCRIPTION_CRM_SMART_ROBOTS"] = "�������� ������� ����� �������� ��� ������ ������� �� ��������� �����!";
$MESS["CRM_CONFIGURATION_MANIFEST_PAGE_TITLE_CRM_SMART_ROBOTS"] = "������� ����� ���������";
$MESS["CRM_CONFIGURATION_MANIFEST_BLOCK_TITLE_CRM_SMART_ROBOTS"] = "������� ����� ���������";
$MESS["CRM_CONFIGURATION_MANIFEST_PAGE_TITLE_IMPORT_CRM_SMART_ROBOTS"] = "������ ����� ���������";
$MESS["CRM_CONFIGURATION_MANIFEST_BLOCK_TITLE_IMPORT_CRM_SMART_ROBOTS"] = "������ ����� ���������";
$MESS["CRM_CONFIGURATION_MANIFEST_IMPORT_DESCRIPTION_UPLOAD_CRM_SMART_ROBOTS"] = "��������� ���� � ����������� ����� ���������";
$MESS["CRM_CONFIGURATION_MANIFEST_ACTION_DESCRIPTION_CRM_SMART_ROBOTS"] = "������� \"��������������\", ����� ��������� ���� ����� �������� � �����";
