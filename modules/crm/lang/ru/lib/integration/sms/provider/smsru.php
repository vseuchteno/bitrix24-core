<?
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_NAME"] = "�������� SMS.RU";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_CAN_USE_ERROR"] = "��������� �������� SMS.RU �� ��������";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_201"] = "�� ������� ������� �� ������� �����";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_202"] = "����������� ������ ����������";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_203"] = "��� ������ ���������";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_204"] = "��� ����������� �� ����������� � ��������������";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_205"] = "��������� ������� ������� (��������� 8 ���)";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_206"] = "����� �������� ��� ��� �������� ������� ����� �� �������� ���������";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_207"] = "�� ���� ����� (��� ���� �� �������) ������ ���������� ���������, ���� ������� ����� 100 ������� � ������ �����������";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_209"] = "�� �������� ���� ����� (��� ���� �� �������) � ����-����";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_230"] = "�������� ����� ����� ���������� ��������� �� ���� ����� � ����";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_231"] = "�������� ����� ���������� ��������� �� ���� ����� � ������";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_232"] = "�������� ����� ���������� ��������� �� ���� ����� � ����";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_301"] = "������������ ������, ���� ������������ �� ������";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_302"] = "������� �� ����������� (������������ �� ���� ���, ���������� � ��������������� ���)";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_303"] = "��� ������������� �������";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_304"] = "���������� ������� ����� ����� �������������. ����������, ��������� ������ �������";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_305"] = "������� ����� �������� ������ ����, ��������� ������� �������";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_900"] = "��� ������������� ������ �������";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_901"] = "������� ������ ����� ��������, ������ � �������";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_903"] = "� ������ ������ ����������� ������ � ����� ����� ����������";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_905"] = "������������ �� ������ ��� (��� ��� ������ 2� ��������)";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_906"] = "������������ �� ������ ������� (��� ��� ������ 2� ��������)";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_OTHER"] = "��������� ������. ���������� ��������� ������ ��� ���";
?>