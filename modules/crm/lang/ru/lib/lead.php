<?
$MESS["CRM_LEAD_ENTITY_ID_FIELD"] = "ID";
$MESS["CRM_LEAD_ENTITY_TITLE_FIELD"] = "��������";
$MESS["CRM_LEAD_ENTITY_NAME_FIELD"] = "���";
$MESS["CRM_LEAD_ENTITY_LAST_NAME_FIELD"] = "�������";
$MESS["CRM_LEAD_ENTITY_SECOND_NAME_FIELD"] = "��������";
$MESS["CRM_LEAD_ENTITY_BIRTHDATE_FIELD"] = "���� ��������";
$MESS["CRM_LEAD_ENTITY_SHORT_NAME_FIELD"] = "�������";
$MESS["CRM_LEAD_ENTITY_PHONE_FIELD"] = "�������";
$MESS["CRM_LEAD_ENTITY_EMAIL_FIELD"] = "E-mail";
$MESS["CRM_LEAD_ENTITY_WEB_FIELD"] = "����";
$MESS["CRM_LEAD_ENTITY_MESSENGER_FIELD"] = "����������";
$MESS["CRM_LEAD_ENTITY_POST_FIELD"] = "���������";
$MESS["CRM_LEAD_ENTITY_ADDRESS_FIELD"] = "�����";
$MESS["CRM_LEAD_ENTITY_SOURCE_BY_FIELD"] = "��������";
$MESS["CRM_LEAD_ENTITY_COMPANY_TITLE_FIELD"] = "�������� ��������";
$MESS["CRM_LEAD_ENTITY_SOURCE_DESCRIPTION_FIELD"] = "������������� �� ���������";
$MESS["CRM_LEAD_ENTITY_STATUS_ID_FIELD"] = "������ ����";
$MESS["CRM_LEAD_ENTITY_SOURCE_ID_FIELD"] = "�������� ����";
$MESS["CRM_LEAD_ENTITY_STATUS_BY_FIELD"] = "������";
$MESS["CRM_LEAD_ENTITY_STATUS_DESCRIPTION_FIELD"] = "������������� � �������";
$MESS["CRM_LEAD_ENTITY_PRODUCT_ID_FIELD"] = "�����";
$MESS["CRM_LEAD_ENTITY_PRODUCT_BY_FIELD"] = "�����";
$MESS["CRM_LEAD_ENTITY_COMMENTS_FIELD"] = "�����������";
$MESS["CRM_LEAD_ENTITY_OPPORTUNITY_FIELD"] = "�����";
$MESS["CRM_LEAD_ENTITY_COMPANY_ID_FIELD"] = "��������";
$MESS["CRM_LEAD_ENTITY_CONTACT_ID_FIELD"] = "�������";
$MESS["CRM_LEAD_ENTITY_CURRENCY_ID_FIELD"] = "������";
$MESS["CRM_LEAD_ENTITY_CURRENCY_BY_FIELD"] = "������";
$MESS["CRM_LEAD_ENTITY_ASSIGNED_BY_ID_FIELD"] = "�������������";
$MESS["CRM_LEAD_ENTITY_ASSIGNED_BY_FIELD"] = "�������������";
$MESS["CRM_LEAD_ENTITY_CREATED_BY_FIELD"] = "������";
$MESS["CRM_LEAD_ENTITY_CREATED_BY_ID_FIELD"] = "������";
$MESS["CRM_LEAD_ENTITY_MODIFY_BY_FIELD"] = "�������";
$MESS["CRM_LEAD_ENTITY_MODIFY_BY_ID_FIELD"] = "�������";
$MESS["CRM_LEAD_ENTITY_DATE_CREATE_FIELD"] = "���� ��������";
$MESS["CRM_LEAD_ENTITY_DATE_CREATE_SHORT_FIELD"] = "���� ��������";
$MESS["CRM_LEAD_ENTITY_DATE_MODIFY_FIELD"] = "���� ���������";
$MESS["CRM_LEAD_ENTITY_DATE_MODIFY_SHORT_FIELD"] = "���� ���������";
$MESS["CRM_LEAD_ENTITY_DATE_CLOSED_FIELD"] = "���� ��������";
$MESS["CRM_LEAD_ENTITY_DATE_CLOSED_SHORT_FIELD"] = "���� ��������";
$MESS["CRM_LEAD_ENTITY_EVENT_RELATION_FIELD"] = "�������";

$MESS["CRM_LEAD_ENTITY_IS_CONVERT_FIELD"] = "��������������";
$MESS["CRM_LEAD_ENTITY_IS_REJECT_FIELD"] = "��������";
$MESS["CRM_LEAD_ENTITY_IS_WORK_FIELD"] = "� ������";

$MESS["CRM_LEAD_ENTITY_PHONE_MOBILE_FIELD"] = "��������� �������";
$MESS["CRM_LEAD_ENTITY_PHONE_WORK_FIELD"] = "������� �������";
$MESS["CRM_LEAD_ENTITY_EMAIL_HOME_FIELD"] = "������ E-mail";
$MESS["CRM_LEAD_ENTITY_EMAIL_WORK_FIELD"] = "������� E-mail";
$MESS["CRM_LEAD_ENTITY_SKYPE_FIELD"] = "Skype";
$MESS["CRM_LEAD_ENTITY_ICQ_FIELD"] = "ICQ";

$MESS["CRM_LEAD_ENTITY_WEBFORM_ID_FIELD"] = "������ CRM-������";
?>