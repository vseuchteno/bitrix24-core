<?
$MESS["CRM_ENTITY_MERGER_EXCEPTION_CONFLICT_RESOLUTION_NOT_SUPPORTED"] = "����� ���������� ���������� \"#RESOLUTION_CAPTION#\" �� ��������������.";
$MESS["CRM_ENTITY_MERGER_EXCEPTION_CONFLICT_OCCURRED"] = "�������������� ����������� ���������� ��-�� ��������� ������.";
$MESS["CRM_ENTITY_MERGER_EXCEPTION_READ_DENIED"] = "���������� ����� �� ������ \"#TITLE#\" [#ID#].";
$MESS["CRM_ENTITY_MERGER_EXCEPTION_UPDATE_DENIED"] = "���������� ����� �� ���������� \"#TITLE#\" [#ID#].";
$MESS["CRM_ENTITY_MERGER_EXCEPTION_DELETE_DENIED"] = "���������� ����� �� �������� \"#TITLE#\" [#ID#].";
$MESS["CRM_ENTITY_MERGER_EXCEPTION_NOT_FOUND"] = "�� ������� ����� ������� [#ID#].";
$MESS["CRM_ENTITY_MERGER_EXCEPTION_UPDATE_FAILED"] = "�� ������� ��������� \"#TITLE#\" [#ID#].";
$MESS["CRM_ENTITY_MERGER_EXCEPTION_DELETE_FAILED"] = "�� ������� ������� \"#TITLE#\" [#ID#].";
$MESS["CRM_ENTITY_MERGER_EXCEPTION_ERROR"] = "��� ���������� ����������� ��������� ������. �������� �� ������: #ERROR#";
?>