<?
$MESS["CRM_RESTR_MGR_DUP_CTRL_MSG_CONTENT_2"] = "<div class=\"crm-duplicate-tab-content\">
	<h3 class=\"crm-duplicate-tab-title\">����������� ����� ����������</h3>
	<div class=\"crm-duplicate-tab-text\">
		��� �������� ������ �������� (���� ��� ��������) \"�������24\" ������ ������� � ���������� ���������, ������������ ��������� ������.
	</div>
	<div class=\"crm-duplicate-tab-text\">
		� ����������� CRM ������������� ����� �������� ��������� ��� ������� ������, �������� ��������� � ��� ��������� ���� � ���������� ��������� ���������.
	</div>
	<div class=\"crm-duplicate-tab-text\">
		�������� ��� � ������ �������� ����������� � ���� �������24! ����������� CRM + ����������� ��������� � ������ �������� �����������, ������� � ������ \"������+\" �� #TF_PRICE#.
		<a target=\"_blank\" href=\"https://www.bitrix24.ru/pro/crm.php\">������ ���������</a>
	</div>
	<div class=\"crm-history-tab-buttons\">
		<span class=\"webform-button webform-button-create\" onclick=\"#LICENSE_LIST_SCRIPT#\">������� �� ����������� �����</span>
		<span class=\"webform-button webform-button-transparent\" onclick=\"#DEMO_LICENSE_SCRIPT#\">��������� �� 30 ����</span>
	</div>
</div>";
$MESS["CRM_RESTR_MGR_HX_VIEW_MSG_CONTENT_2"] = "<div class=\"crm-history-tab-content\">
	<h3 class=\"crm-history-tab-title\">�������� ������� ��������� �������� � ����������� CRM</h3>
	<div class=\"crm-history-tab-text\">
		�������24 ����� ������ ���� ���������� � ��������� � CRM. ������� ����������� CRM, �� ������� ���������� � ������������ ������, ������� ��� �� ����������� �������������, ������� ��� ������������ ��������, ������� � ������� �������� ������ �������24.
	</div>
	<div class=\"crm-history-tab-text\">��������� ��� ����� ���:</div>
	<img alt=\"������� �������\" class=\"crm-history-tab-img\" src=\"/bitrix/js/crm/images/history-ru.png?1\"/>
	<div class=\"crm-history-tab-text\">
		�������� ������� � ������ �������� ����������� � ���� �������24! ����������� CRM + ����������� ��������� � ������ �������� ����������� ��������, ������� � ������ \"������+\" �� #TF_PRICE#.
		<a target=\"_blank\" href=\"https://www.bitrix24.ru/pro/crm.php\">������ ���������</a>
	</div>
	<div class=\"crm-history-tab-buttons\">
		<span class=\"webform-button webform-button-create\" onclick=\"#LICENSE_LIST_SCRIPT#\">������� �� ����������� �����</span>
		<span class=\"webform-button webform-button-transparent\" onclick=\"#DEMO_LICENSE_SCRIPT#\">��������� �� 30 ����</span>
	</div>
</div>";
$MESS["CRM_RESTR_MGR_POPUP_TITLE"] = "�������� ������ � ����������� CRM \"�������24\"";
$MESS["CRM_RESTR_MGR_POPUP_CONTENT_2"] = "�������� � ����� CRM:
<ul class=\"hide-features-list\">
	<li class=\"hide-features-list-item\">����������� ����� ��������, �������, �������������</li>
	<li class=\"hide-features-list-item\">����������� ����� ����������</li>
	<li class=\"hide-features-list-item\">������� ���� ��������� � ������������ ��������������</li>
	<li class=\"hide-features-list-item\">������ ������� ����������� � ������ � CRM</li>
	<li class=\"hide-features-list-item\">�������� ������ 5000 ������� � CRM</li>
	<li class=\"hide-features-list-item\">������ �� ������ ��������<sup class=\"hide-features-soon\">�����</sup></li>
	<li class=\"hide-features-list-item\">�������� �� ���� ��������<sup class=\"hide-features-soon\">�����</sup></li>
	<li class=\"hide-features-list-item\">
		������� �����<sup class=\"hide-features-soon\">�����</sup>
		<a target=\"_blank\" class=\"hide-features-more\" href=\"https://www.bitrix24.ru/pro/crm.php\">������ ���������</a>
	</li>
</ul>
<strong>����������� CRM + ����������� ��������� � ������ �������� �����������, ������� � ������ \"������+\" �� #TF_PRICE#.</strong>";
$MESS["CRM_RESTR_MGR_DEAL_CATEGORY_POPUP_TITLE"] = "������������� � CRM";
$MESS["CRM_RESTR_MGR_DEAL_CATEGORY_POPUP_CONTENT_2"] = "<div class=\"crm-deal-category-tab-content\">
	<div class=\"crm-deal-category-tab-text\">
		�� ����� �������� ����� ���� ����������� �� ���������� �������������. ����� ������� ������� �� ������ ��������� � ������ ����� ������ (��� ������� ������ ����������� �������), ��������� �� ������ �������� ����.
        � ������ \"�������\" ������������ ����� �������������: #TEAM_CRM_FUNNEL#. � ������� ������ \"��������\" ����� ������������� �� ����������.
	</div>
</div>";
$MESS["CRM_RESTR_MGR_CONDITIONALLY_REQUIRED_FIELD_POPUP_CONTENT_2"] = "<div class=\"crm-conditionally-required-field-tab-content\">
	<div class=\"crm-conditionally-required-field-tab-text\">
		������� ���� ������������ ��� ���������� ������ �� ������ ������ �� ������� \"�������\", \"��������\" � \"CRM+\".
		���������� � ���� ��� ������ ��������� � ��� ����������, � �� ��������� ����� ������ ������ � CRM ��� ������. �������� ���� ������������ ��� ��������� ������ - � ���������� ������ ���������� �� ������ ������ ����������!		
	</div>
</div>";
$MESS["CRM_RESTR_MGR_PERMISSION_CONTROL_POPUP_TITLE"] = "��������� ����� ������� ����������� ����� � ������������ �������";
$MESS["CRM_RESTR_MGR_PERMISSION_CONTROL_POPUP_CONTENT"] = "<div class=\"crm-permission-control-tab-content\">
	<div class=\"crm-permission-control-tab-text\">
		� ���������� ������ ��� ���������� ����� ���������� �����. ��������� �� ���� �� ������������ �������, ����� ���������� ���� ����� ����������, ����� �������� ����� �� �������� � � ����� ������� ��� ������ ��������.	
		��������� ��� ������ � �� ����������� ������� �� <a target=\"_blank\" href=\"https://www.bitrix24.ru/prices/\">�������� ��������� ��������</a>.	
	</div>
</div>";
$MESS["CRM_RESTR_MGR_PERMISSION_CONTROL_POPUP_CONTENT_2"] = "<div class=\"crm-permission-control-tab-content\">
	<div class=\"crm-permission-control-tab-text\">
		�� ����� �������� ����� ��� ���������� ����� ���������� �����. ��������� �� ������ �����, ����� ���������� ���� ����� ����������, ����� �������� ����� �� �������� � � ����� ������� ��� ������ ��������.
		��������� ��� ������ � �� ����������� ������� �� <a target=\"_blank\" href=\"https://www.bitrix24.ru/prices/\">�������� ��������� ��������</a>.	
	</div>
</div>";
$MESS["CRM_RESTR_MGR_INVOICE_RECURRING_POPUP_TITLE"] = "���������� ����� �������� � ������� \"�������\" � \"��������\"";
$MESS["CRM_RESTR_MGR_INVOICE_RECURRING_POPUP_CONTENT"] = "������ ���� � �������24 ����� ��������� �������������. 
���� �� ��������� � ��������, ��������� ����������� ����� ����� ��������, ��������, ��� � �����, ���������� ����� ����������� ��������� ���� �����. 
��������� ���� ������� � ������� ������������ ��� ����������. � ������ ����� CRM ������������� ������� ���� � ��������� ��� �� e-mail �������.
	<ul class=\"hide-features-list\">
		<li class=\"hide-features-list-item\">�������������� �������� �����</li>
		<li class=\"hide-features-list-item\">������� ������ �� ���� �������� ���������� ������</li>
		<li class=\"hide-features-list-item\">�������� ����� ��� ������ �� e-mail ������� <a href=\"https://www.bitrix24.ru/pro/crm.php\" target=\"_blank\" class=\"hide-features-more\">������ ���������</a></li>
	</ul>		
	<strong>���������� ����� �������� � ������� \"�������\" � \"��������\"</strong>";
$MESS["CRM_RESTR_MGR_DEAL_RECURRING_POPUP_TITLE"] = "���������� ������ �������� � ������� \"�������\" � \"��������\"";
$MESS["CRM_RESTR_MGR_DEAL_RECURRING_POPUP_CONTENT"] = "��������� ������ � �������24 ����� �������������! 
���� ��, ��������, ����� ��� ���������� ����������� ��������� ������� � �����-�� ��������, �� ���������� ������ ��������� ���� �����. 
�������� ������, ������� �� ������������ � �����������. � � ��������� ����� ��� ����� ������� ������������� ��� ������� ���������.
	<ul class=\"hide-features-list\">
		<li class=\"hide-features-list-item\">�������������� �������� ������</li>
		<li class=\"hide-features-list-item\">��������� ������������� ������ ����������</li>
		<li class=\"hide-features-list-item\">������� ������ � ������ ���������� ������ <a href=\"https://www.bitrix24.ru/pro/crm.php\" target=\"_blank\" class=\"hide-features-more\">������ ���������</a></li>
	</ul>		
	<strong>���������� ������ �������� � ������� \"�������\" � \"��������\"</strong>";
$MESS["CRM_ST_ROBOTS_POPUP_TITLE"] = "������";
$MESS["CRM_ST_ROBOTS_POPUP_TEXT"] = "
	��������������� ������� � CRM �������24<br><br>
	���������� ��������, �� ������� CRM ����� ����� ������ ������: ������� ������, ����������� �������, ��������� ��������������� ������� � ���������� �����. ������ ��������� ���������, ��� ����� ������� �� ������ ����� ������ � �� ������ �� ������� � ������� ������ �� �����.<br><br>
	�������� ������������� ��������� �� �������� �������� (��������, �� ��������� �����, ���������� �����, ����������� � �������, ������) � ��������� ������, ������� ������� �������� ��� �� ������.<br><br>
	������ � �������� ������ ��� �������� �������� �� ���, ��� �� ����� ������� ����� ����������: �������� ���� ��� � ������ ������ �������!<br><br>
	���������� �������, ����� ������������ ������.<br><br>
	���������� �������� ������ �� ������������ �������.
";
$MESS["CRM_WEBFORM_EDIT_POPUP_LIMITED_TITLE"] = "����������� CRM-�����";
$MESS["CRM_WEBFORM_EDIT_POPUP_LIMITED_TEXT"] = "��������� ������� ����� ������ �� ������������ �������� ������";
$MESS["CRM_BUTTON_EDIT_OPENLINE_MULTI_POPUP_LIMITED_TEXT"] = "���������� �������������� ������ ����� ������ �� ������������ �������� ������.";
$MESS["CRM_WEBFORM_LIST_POPUP_LIMITED_TEXT"] = "�� ����� �������� ����� ���� ����������� �� ���������� �������� CRM-����. ����� �������� ����� �����, ��������� �� ������ �������� ����.
<br><br>
� ������� ������ \"��������\" ����� �������� CRM-���� �� ����������.
<br><br>
������� �� ����������� ����� ��������� �� 30 ����";

?>