<?
$MESS["CRM_LEAD_SETTINGS_VIEW_WIDGET"] = "��������� �� �����";
$MESS["CRM_LEAD_SETTINGS_VIEW_LIST"] = "������� ������ �����";
$MESS["CRM_LEAD_SETTINGS_VIEW_KANBAN"] = "������ �����";
$MESS["CRM_TYPE_TITLE"] = "�������� ������� ������ ������ � CRM";
$MESS["CRM_TYPE_SAVE"] = "���������";
$MESS["CRM_TYPE_CANCEL"] = "��������";
$MESS["CRM_TYPE_CONTINUE"] = "����������";
$MESS["CRM_TYPE_TURN_ON"] = "��������";
$MESS["CRM_ROBOTS_TITLE"] = "������� �����";
$MESS["CRM_ROBOTS_TEXT"] = "������� ����� ������ � CRM ������������� ������������ ������ ����� ��� � ������ � ������� �������.<br/><br/>
� ��� ��� ���� ����������� ������ ��� �����. ��� ��������� �������� ������ ��� ������ �� ������ ������ ����� �������. <br/><br/>
�� �������, ��� ������ �������� ������� �����?";
$MESS["CRM_LEAD_CONVERT_TITLE"] = "����������� �����";
$MESS["CRM_LEAD_CONVERT_TEXT"] = "������� ����� ������ � CRM ������������� ������������ ������ ��� � ������ � �������. <br/><br/>
���� � ��� �������� ���������� ����, ��� ��������� ����� ������ ��� ����� ��������������� � ������ � ��������. <br/><br/>
��� ������ � ������-��������, ����������� �� �������� ������ � ��������, ����� ���������� ��� ��������� ���������.";
$MESS["CRM_LEAD_BATCH_CONVERSION_STATE"] = "#processed# �� #total#";
$MESS["CRM_LEAD_BATCH_CONVERSION_TITLE"] = "����������� �����";
$MESS["CRM_LEAD_BATCH_CONVERSION_COMPLETED"] = "����������� ����� ���������.";
$MESS["CRM_LEAD_BATCH_CONVERSION_COUNT_SUCCEEDED"] = "������� ���������������: #number_leads#.";
$MESS["CRM_LEAD_BATCH_CONVERSION_COUNT_FAILED"] = "�� ������� ���������������: #number_leads#.";
$MESS["CRM_LEAD_BATCH_CONVERSION_NO_NAME"] = "��� �����";
?>