<?php

$MESS["CRM_WEBFORM_OPTIONS_DICT_PERC_FIELD_NAME"] = "���";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PERC_FIELD_LASTNAME"] = "�������";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PERC_FIELD_SECOND_NAME"] = "��������";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PERC_FIELD_PHONE"] = "�������";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PERC_FIELD_EMAIL"] = "Email";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PERC_FIELD_COMPANY_NAME"] = "�������� ��������";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PERC_FIELD_EMAIL"] = "Email";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_TYPE_POPUP"] = "�����";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_TYPE_PANEL"] = "������ �����";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_POS_LEFT"] = "�����";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_POS_CENTER"] = "�� ������";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_POS_RIGHT"] = "������";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_VERT_TOP"] = "������";
$MESS["CRM_WEBFORM_OPTIONS_DICT_VIEW_VERT_BOTTOM"] = "�����";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_EVENT_CHANGE"] = "���������";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_ACTION_SHOW"] = "��������";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_ACTION_HIDE"] = "��������";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_EQUAL"] = "�����";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_NOTEQUAL"] = "�� �����";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_GREATER"] = "������";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_GREATEROREQUAL"] = "������ ��� �����";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_LESS"] = "������";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_LESSOREQUAL"] = "������ ��� �����";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_EMPTY"] = "������";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_ANY"] = "�����";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_CONTAIN"] = "��������";
$MESS["CRM_WEBFORM_OPTIONS_DICT_COND_OP_NOTCONTAIN"] = "�� ��������";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PROPS_DOMAIN"] = "�������� ���";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PROPS_DOMAIN_DESC"] = "�������� ���, � �������� ��������� �����";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PROPS_URL"] = "����� ��������";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PROPS_URL_DESC"] = "����� ��������, � ������� ��������� �����";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PROPS_PARAM"] = "��������";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PROPS_PARAM_DESC"] = "������� �������� ��������� �� �������� ������ ��������, � ������� ��������� �����";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PROPS_RESULT_ID"] = "��� ����������";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PROPS_RESULT_ID_DESC"] = "���������� �����, ����������� ���������� ���������� �����.";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PROPS_FORM_ID"] = "��� �����";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PROPS_FORM_ID_DESC"] = "��� �����";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PROPS_FORM_NAME"] = "�������� �����";
$MESS["CRM_WEBFORM_OPTIONS_DICT_PROPS_FORM_NAME_DESC"] = "�������� �����";
$MESS["CRM_WEBFORM_OPTIONS_DICT_FIELD_FILE_CONTENT_TYPE_IMAGE"] = "��������";
$MESS["CRM_WEBFORM_OPTIONS_DICT_FIELD_FILE_CONTENT_TYPE_VIDEO"] = "�����";
$MESS["CRM_WEBFORM_OPTIONS_DICT_FIELD_FILE_CONTENT_TYPE_AUDIO"] = "�����";