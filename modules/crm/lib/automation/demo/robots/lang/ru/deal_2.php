<?php
$MESS['CRM_AUTOMATION_DEMO_DEAL_2_NOTIFY_TITLE'] = '�����������';
$MESS['CRM_AUTOMATION_DEMO_DEAL_2_CONTROL_TITLE'] = '��������';
$MESS['CRM_AUTOMATION_DEMO_DEAL_2_CALL_TITLE'] = '������������� ������';
$MESS['CRM_AUTOMATION_DEMO_DEAL_2_1_MESSAGE'] = '������� ����� ������ {=Document:ID} {=Document:TITLE}

������������ � �������� ������: ��� ������ ������� ������, ����� ����� ����������, ����� ���������� ������ ������ �� ������ � ������� ������� �������.

������������ ���� �� ������ ����� ������� �� �� ��������� ����������.';
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_2_MESSAGE"] = "�������! �� ������ ������ �� ������ {=Document:ID} {=Document:TITLE}

�� ���������, ��� ���� ������ - ������� ������� ������. ��� ����� ���������� ��������� ���������� ������ ������ �� �������, �������� ��������������� ����, �������� ������ �� �������.";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_3_MESSAGE"] = "��������! ������ {=Document:ID} {=Document:TITLE} ��� 3 ��� ��������� �� ������ {=Document:STAGE_ID_PRINTABLE} 

��������� ���� ���������� ����������� � ������������� �������� �� �������, �� �������� �� ���� �� �����������. 

������������� �� ������  {=Document:ASSIGNED_BY_PRINTABLE}";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_4_MESSAGE"] = "������ {=Document:ID} {=Document:TITLE} �� ����� {=Document:OPPORTUNITY_ACCOUNT} ���������. 

������������� �� ������  {=Document:ASSIGNED_BY_PRINTABLE}";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_5_MESSAGE"] = "�������! �� ������� ��������� �� ������ {=Document:ID} {=Document:TITLE}

�� ���������, ��� ���� ������ - ������� ������� ������. ��� ����� ���������� ��������� ���������� ������ ������ �� �������, �������� ��������������� ����, �������� ������ �� �������.";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_6_MESSAGE"] = "�� ���������, ��� ��������� �������� ������ ���������� ��������� ������� �� ������. 

������ {=Document:ID} {=Document:TITLE} ��� 2 ��� ��������� �� ������ {=Document:STAGE_ID_PRINTABLE} ����� ��������� ������ ���� � ��������� ������.";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_7_MESSAGE"] = "��������! ������ {=Document:ID} {=Document:TITLE} ��� 3 ��� ��������� �� ������ {=Document:STAGE_ID_PRINTABLE} 

��������� ���� ���������� ����������� � ������������� �������� �� �������, �� �������� �� ���� �� �����������. 

������������� �� ������  {=Document:ASSIGNED_BY_PRINTABLE}";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_8_MESSAGE"] = "�������! �� ������� ��������� �� ������ {=Document:ID} {=Document:TITLE} 

�� ������ ����� ���������� ������� �������� ������ ����� ��������: 

���� ���������� �� ������ �������� ������������ ��� ����������, ��������� � ��������� ���� �������. 

���� ���������� �� ��������� � �������� ����� ��������, ����������� ������ �� ��������� ������.";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_9_MESSAGE"] = "��������� ������� �� ������ ����� �� ������ {=Document:ID} {=Document:TITLE}

���� ���� ��� �������, �� �������� ������� ������ ������ ����� � ����������� ������ �� ��������� ����";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_10_MESSAGE"] = "��������! ������ {=Document:ID} {=Document:TITLE} ��� 5 ���� ��������� �� ������ {=Document:STAGE_ID_PRINTABLE} 

��������� ���� ���������� ����������� � ������������� �������� �� �������, �� �������� �� ���� �� �����������. 

������������� �� ������  {=Document:ASSIGNED_BY_PRINTABLE}";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_11_MESSAGE"] = "�������! �� ������� ��������� �� ������ {=Document:ID} {=Document:TITLE}

�� ���������, ��� ���� ������ - ������� ������� ������. ��� ����� ���������� ��������� ���������� ������ ������ �� �������, �������� ��������������� ����, �������� ������ �� �������.";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_12_MESSAGE"] = "��������! ������ {=Document:ID} {=Document:TITLE} ��� 4 ��� ��������� �� ������ {=Document:STAGE_ID_PRINTABLE} 

��������� ���� ���������� ����������� � ������������� �������� �� �������, �� �������� �� ���� �� �����������. 

������������� �� ������  {=Document:ASSIGNED_BY_PRINTABLE}";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_13_MESSAGE"] = "�������! �� ������� ��������� �� ������ {=Document:ID} {=Document:TITLE} 

�� ��������� ���������� ������ �������� �������� ������ �� ������.

��������� � ��������� ���� �������.";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_14_MESSAGE"] = "��������� ������� �� ������ ����� �� ������ {=Document:ID} {=Document:TITLE}

���� ���� ��� �������, �� �������� ������� ������ ������ ����� � ��������� ������";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_15_MESSAGE"] = "�������! �� ������� ��������� �� ������ {=Document:ID} {=Document:TITLE}

��� ���� ��������� � ������ ��������? ���������� ������ � �������� ��������� ������.";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_CALL_SUBJECT"] = "��������� �� ������ �����";