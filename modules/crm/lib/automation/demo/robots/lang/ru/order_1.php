<?php
$MESS['CRM_AUTOMATION_DEMO_ORDER_1_EMAIL_TITLE'] = '��������� ������';
$MESS['CRM_AUTOMATION_DEMO_ORDER_1_NEW_ORDER_TITLE'] = '{=Document:SHOP_TITLE}: ����� ����� N{=Document:ACCOUNT_NUMBER}';
$MESS['CRM_AUTOMATION_DEMO_ORDER_1_NEW_ORDER_PAYMENT_TITLE'] = '{=Document:SHOP_TITLE}: ����������� �� ������ ������ N{=Document:ACCOUNT_NUMBER}';
$MESS['CRM_AUTOMATION_DEMO_ORDER_1_ORDER_PAYED_TITLE'] = '{=Document:SHOP_TITLE}: ����� N{=Document:ACCOUNT_NUMBER} �������';
$MESS['CRM_AUTOMATION_DEMO_ORDER_1_ORDER_CANCELED_TITLE'] = '{=Document:SHOP_TITLE}: ������ ������ N{=Document:ACCOUNT_NUMBER}';
$MESS['CRM_AUTOMATION_DEMO_ORDER_1_ALLOW_DELIVERY_TITLE'] = '��������� �������� ������';
$MESS['CRM_AUTOMATION_DEMO_ORDER_1_FOOTER'] = '� ���������,<br> ������������� #A1#��������-��������#A2#';


$MESS['CRM_AUTOMATION_DEMO_ORDER_1_MAIL_1_TITLE'] = '���� �������� ����� � �������� {=Document:SHOP_TITLE}';
$MESS['CRM_AUTOMATION_DEMO_ORDER_1_MAIL_1_BODY'] = '<p style="margin-top:30px; margin-bottom: 28px; font-weight: bold; font-size: 19px;">
��������� {=Document:CONTACT.NAME} {=Document:CONTACT.SECOND_NAME} {=Document:CONTACT.LAST_NAME},
</p>
<p style="margin-top: 0; margin-bottom: 20px; line-height: 20px;">
��� ����� ����� {=Document:ACCOUNT_NUMBER} �� {=Document:DATE_INSERT} ������.<br>
<br>
��������� ������: {=Document:PRICE_FORMATTED}.<br>
<br>
�� ������ ������� �� ����������� ������ ������ (�� ����� ������ ���������� �� ���������), ����� � ��� ������������ ������ ����� {=Document:SHOP_TITLE}.<br>
<br>
�������� ��������, ��� ��� ����� � ���� ������ ��� ���������� ����� ������ ����� � ������ ������������ ����� {=Document:SHOP_TITLE}.<br>
<br>
��� ����, ����� ������������ �����, �������������� �������� ������ ������, ������� �������� � ����� ������������ ������� ����� {=Document:SHOP_TITLE}.<br>
<br>
����������, ��� ��������� � ������������� ����� {=Document:SHOP_TITLE} ����������� ���������� ����� ������ ������ - {=Document:ACCOUNT_NUMBER}.<br>
<br>
������� �� �������!<br>
</p>';

$MESS['CRM_AUTOMATION_DEMO_ORDER_1_MAIL_2_TITLE'] = '���������� ��� �� ������ ������ �� ����� {=Document:SHOP_TITLE}';
$MESS['CRM_AUTOMATION_DEMO_ORDER_1_MAIL_2_BODY'] = '<p style="margin-top:30px; margin-bottom: 28px; font-weight: bold; font-size: 19px;">
��������� {=Document:CONTACT.NAME} {=Document:CONTACT.SECOND_NAME} {=Document:CONTACT.LAST_NAME},
</p>
<p style="margin-top: 0; margin-bottom: 20px; line-height: 20px;">
���� ��� �������� ����� N{=Document:ACCOUNT_NUMBER} �� {=Document:DATE_INSERT} �� ����� {=Document:PRICE_FORMATTED}.<br>
<br>
� ���������, �� ����������� ���� �������� �� ����� ������ �� ��������� � ���.<br>
<br>
�� ������ ������� �� ����������� ������ ������ (�� ����� ������ ���������� �� ���������), ����� � ��� ������������ ������ ����� {=Document:SHOP_TITLE}.<br>
<br>
�������� ��������, ��� ��� ����� � ���� ������ ��� ���������� ����� ������ ����� � ������ ������������ ����� {=Document:SHOP_TITLE}.<br>
<br>
��� ����, ����� ������������ �����, �������������� �������� ������ ������, ������� �������� � ����� ������������ ������� ����� {=Document:SHOP_TITLE}.<br>
<br>
����������, ��� ��������� � ������������� ����� {=Document:SHOP_TITLE} ����������� ���������� ����� ������ ������ - {=Document:ACCOUNT_NUMBER}.<br>
<br>
������� �� �������!<br>
</p>';
$MESS['CRM_AUTOMATION_DEMO_ORDER_1_MAIL_3_TITLE'] = '�� �������� ����� �� ����� {=Document:SHOP_TITLE}';
$MESS['CRM_AUTOMATION_DEMO_ORDER_1_MAIL_3_BODY'] = '<p style="margin-top:30px; margin-bottom: 28px; font-weight: bold; font-size: 19px;">
����� ����� {=Document:ACCOUNT_NUMBER} �� {=Document:DATE_INSERT} �������.
</p>
<p style="margin-top: 0; margin-bottom: 20px; line-height: 20px;">
��� ��������� ��������� ���������� �� ������ �������� �� ���� {=Document:SHOP_PUBLIC_URL}
</p>';
$MESS['CRM_AUTOMATION_DEMO_ORDER_1_MAIL_4_TITLE'] = '{=Document:SHOP_TITLE}: ������ ������ N{=Document:ACCOUNT_NUMBER}';
$MESS['CRM_AUTOMATION_DEMO_ORDER_1_MAIL_4_BODY'] = '<p style="margin-top:30px; margin-bottom: 28px; font-weight: bold; font-size: 19px;">
����� ����� {=Document:ACCOUNT_NUMBER} �� {=Document:DATE_INSERT} �������.
</p>
<p style="margin-top: 0; margin-bottom: 20px; line-height: 20px;">
{=Document:REASON_CANCELED}<br>
<br>
��� ��������� ��������� ���������� �� ������ �������� �� ���� {=Document:SHOP_PUBLIC_URL}
</p>';

