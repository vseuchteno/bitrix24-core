<?

IncludeModuleLangFile(__FILE__);

/*patchvalidationmutatormark1*/

//AddEventHandler("iblock", "OnIBlockPropertyBuildList", array("CGrain_IBlockPropertyLink", "GetUserTypeDescription"));
//AddEventHandler("main", "OnUserTypeBuildList", Array("CGrain_UserPropertyLink", "GetUserTypeDescription"));

CModule::AddAutoloadClasses(
	"grain.links",
	array(
		"CGrain_LinksTools" => "classes/general/tools.php",
		"CGrain_LinksAdminTools" => "classes/general/admin_tools.php",
	)
);

class CGrain_PropertyLink {

	function GetDataSourceFolders()
	{
		return array(
			BX_ROOT."/modules/grain.links/lists",
			"/local/modules/grain.links/lists",
			BX_ROOT."/php_interface/grain.links",
			"/local/php_interface/grain.links",
		);
	}

	function BasePrepareSettings($arProperty, $key = "SETTINGS")
	{	
		return $arProperty[$key];
	}

	function BaseGetSettingsHTML($name, $val)
	{

		ob_start();
	
?>
<tr><td colSpan="2">
<style>
tr.grain-links-prop-sub-title td{background-color: rgb(224, 232, 234) !important; color: rgb(75, 98, 103) !important; text-align: left !important; font-weight: bold !important; padding: 5px 10px !important;}
#grain_links_data_source_params { padding-top: 8px }
table.grain-links-dsparams { border: 1px solid rgb(208, 215, 216); border-collapse: collapse; width: 100% !important; margin: 0 0 8px 0 !important }
table.grain-links-dsparams td { border: 1px solid rgb(208, 215, 216); padding: 5px 5px !important; background-color: white }
table.grain-links-dsparams td.grain-links-dsparams-col-name { text-align: right }
table.grain-links-dsparams td.grain-links-dsparams-col-input { text-align: left }
table.grain-links-dsparams .grain-links-dsparams-multiple { width: 190px }
table.grain-links-dsparams .grain-links-dsparams-multiple input { display: block }
</style>
</td></tr>
<tr class="grain-links-prop-sub-title"><td colSpan="2"><?=GetMessage('GRAIN_LINKS_PROP_SETTINGS_HEADER_DATA_SOURCE')?></td></tr>
<tr>
	<td colSpan="2" style="padding: 8px 0 0 0; text-align: center">
		<?
		$arDataSourceList = CGrain_LinksAdminTools::GetDataSourceList(true);
		?>
		<select name="<?=$name?>[DATA_SOURCE]" onchange="window.grain_links_dsparams_refresh(true);" id="grain_links_data_source_select">
			<option value=""></option>
			<?foreach($arDataSourceList as $k=>$v):?>
				<option value="<?=$k?>"<?if($val["DATA_SOURCE"]==$k):?> selected="selected"<?endif?>><?=$v?></option>
			<?endforeach?>
		</select>
		<div id="grain_links_data_source_params">
			<?
			CGrain_LinksAdminTools::ShowDataSourceParams($val["DATA_SOURCE"],$name,$val);
			?>
		</div>
		<script>
		window.grain_links_dsparams_addvalues_change = function(obSelect) {
			var obAddValueInput = document.getElementById("grain_links_dspadd_"+obSelect.name);
			if(!obAddValueInput) return;
			if(obSelect.value.length) {
				obAddValueInput.disabled = true;
				obAddValueInput.removeAttribute('name');
				obAddValueInput.value="";
			} else {
				obAddValueInput.disabled = false;
				obAddValueInput.setAttribute('name',obSelect.name);
			}
		};
		window.grain_links_dsparams_addvalues_add = function(obButton)
		{
			var element = obButton.parentNode.getElementsByTagName('input');
			var target_element = false;
			if (element && element.length > 0 && element[0])
			{
				for(var i=0;i<element.length;i++)
					if(element[i].type=="text")
						target_element = element[i];
			    target_element.parentNode.appendChild(target_element.cloneNode(true));
			}
		};
		window.grain_links_dsparams_refresh = function(bReset) {
		
			var obContainer = document.getElementById("grain_links_data_source_params");
			if(!obContainer) return;

			var obSelect = document.getElementById("grain_links_data_source_select");
			if(!obSelect) return;
			var data_source = obSelect.value;
			if(!data_source) {
				obContainer.innerHTML = "";
				return;
			}
		
			var url = '<?=BX_ROOT?>/admin/grain_links_data_source_params.php?lang=<?=LANGUAGE_ID?>&NAME_PREFIX=<?=$name?>&DATA_SOURCE='+data_source;
			var q = [];
		
			if(!bReset) {
				var arElements = obContainer.getElementsByTagName("*");
				for(var i=0;i<arElements.length;i++) {
					// see https://code.google.com/p/form-serialize/
					if (arElements[i].name === "") {
						continue;
					}
					switch (arElements[i].nodeName) {
					case 'INPUT':
						switch (arElements[i].type) {
						case 'text':
						case 'hidden':
						case 'password':
						case 'button':
						case 'reset':
						case 'submit':
							q.push(arElements[i].name + "=" + encodeURIComponent(arElements[i].value));
							break;
						case 'checkbox':
						case 'radio':
							if (arElements[i].checked) {
								q.push(arElements[i].name + "=" + encodeURIComponent(arElements[i].value));
							}						
							break;
						case 'file':
							break;
						}
						break;			 
					case 'TEXTAREA':
						q.push(arElements[i].name + "=" + encodeURIComponent(arElements[i].value));
						break;
					case 'SELECT':
						switch (arElements[i].type) {
						case 'select-one':
							q.push(arElements[i].name + "=" + encodeURIComponent(arElements[i].value));
							break;
						case 'select-multiple':
							for (j = arElements[i].options.length - 1; j >= 0; j = j - 1) {
								if (arElements[i].options[j].selected) {
									q.push(arElements[i].name + "=" + encodeURIComponent(arElements[i].options[j].value));
								}
							}
							break;
						}
						break;
					case 'BUTTON':
						switch (arElements[i].type) {
						case 'reset':
						case 'submit':
						case 'button':
							q.push(arElements[i].name + "=" + encodeURIComponent(arElements[i].value));
							break;
						}
						break;
					}
	
					// disable inputs to prevent change
					switch (arElements[i].nodeName) {
					case 'INPUT':
					case 'TEXTAREA':
					case 'SELECT':
					case 'BUTTON':
						arElements[i].disabled = true;
					break;
					}
					
				}
			}
			
			BX.ajax.post(url, q.join("&"), function(result){
				obContainer.innerHTML = result;
			});
			
		};
		</script>
	</td>
</tr>
<tr class="grain-links-prop-sub-title"><td colSpan="2"><?=GetMessage('GRAIN_LINKS_PROP_SETTINGS_HEADER_INTERFACE')?></td></tr>
<tr>
	<td><?=GetMessage('GRAIN_LINKS_PROP_SETTINGS_INTERFACE_TYPE')?>:</td>
	<td>
		<select name="<?=$name?>[INTERFACE]">
			<option value="ajax"<?if($val["INTERFACE"]=="ajax"):?> selected="selected"<?endif?>><?=GetMessage("GRAIN_LINKS_PROP_SETTINGS_INTERFACE_AJAX")?></option>
			<option value="select"<?if($val["INTERFACE"]=="select"):?> selected="selected"<?endif?>><?=GetMessage("GRAIN_LINKS_PROP_SETTINGS_INTERFACE_SELECT")?></option>
			<option value="selectsearch"<?if($val["INTERFACE"]=="selectsearch"):?> selected="selected"<?endif?>><?=GetMessage("GRAIN_LINKS_PROP_SETTINGS_INTERFACE_SELECTSEARCH")?></option>
			<option value="search"<?if($val["INTERFACE"]=="search"):?> selected="selected"<?endif?>><?=GetMessage("GRAIN_LINKS_PROP_SETTINGS_INTERFACE_SEARCH")?></option>
		</select>
	</td>
</tr>
<tr>
	<td><?=GetMessage('GRAIN_LINKS_PROP_SETTINGS_INTERFACE_SHOW_URL')?>:</td>
	<td><input type="checkbox" name="<?=$name?>[SHOW_URL]" value="Y"<?if($val["SHOW_URL"]=="Y"):?> checked="checked"<?endif?> /></td>
</tr>
<tr>
	<td><?=GetMessage('GRAIN_LINKS_PROP_SETTINGS_INTERFACE_SHOW_VALUE')?>:</td>
	<td><input type="checkbox" name="<?=$name?>[SHOW_VALUE]" value="Y"<?if($val["SHOW_VALUE"]=="Y"):?> checked="checked"<?endif?> /></td>
</tr>
<tr>
	<td><?=GetMessage('GRAIN_LINKS_PROP_SETTINGS_INTERFACE_AJAX_SEND_POST')?> <a href="#" onclick="this.parentNode.lastChild.style.display=this.parentNode.lastChild.style.display=='none'?'':'none'; return false;">?</a> :<span style="display: none; color: #555; font-size: 0.8em"><br /><?=GetMessage('GRAIN_LINKS_PROP_SETTINGS_INTERFACE_AJAX_SEND_POST_NOTE')?></span></td>
	<td><input type="checkbox" name="<?=$name?>[AJAX_SEND_POST]" value="Y"<?if($val["AJAX_SEND_POST"]=="Y"):?> checked="checked"<?endif?> /></td>
</tr>
<tr>
	<td><?=GetMessage('GRAIN_LINKS_PROP_SETTINGS_INCLUDE_SCRIPTS')?> <a href="#" onclick="this.parentNode.lastChild.style.display=this.parentNode.lastChild.style.display=='none'?'':'none'; return false;">?</a> :<span style="display: none; color: #555; font-size: 0.8em"><br /><?=GetMessage('GRAIN_LINKS_PROP_SETTINGS_INCLUDE_SCRIPTS_NOTE')?></span></td>
	<td><input type="checkbox" name="<?=$name?>[INCLUDE_SCRIPTS]" value="Y"<?if($val["INCLUDE_SCRIPTS"]=="Y"):?> checked="checked"<?endif?> /></td>
</tr>
<tr class="grain-links-prop-sub-title"><td colSpan="2"><?=GetMessage('GRAIN_LINKS_PROP_SETTINGS_HEADER_TEMPLATES')?></td></tr>
<tr>
	<td><?=GetMessage('GRAIN_LINKS_PROP_SETTINGS_PUBLIC_EDIT_TEMPLATE')?>:</td>
	<td><input type="text" name="<?=$name?>[PUBLIC_EDIT_TEMPLATE]" value="<?=$val["PUBLIC_EDIT_TEMPLATE"]?>" size="30" /></td>
</tr>
<tr>
	<td><?=GetMessage('GRAIN_LINKS_PROP_SETTINGS_ADMIN_EDIT_TEMPLATE')?>:</td>
	<td><input type="text" name="<?=$name?>[ADMIN_EDIT_TEMPLATE]" value="<?=$val["ADMIN_EDIT_TEMPLATE"]?>" size="30" /></td>
</tr>
<tr>
	<td><?=GetMessage('GRAIN_LINKS_PROP_SETTINGS_ADMIN_FILTER_EDIT_TEMPLATE')?>:</td>
	<td><input type="text" name="<?=$name?>[ADMIN_FILTER_EDIT_TEMPLATE]" value="<?=$val["ADMIN_FILTER_EDIT_TEMPLATE"]?>" size="30" /></td>
</tr>

<?
		$result .= ob_get_contents();
		ob_end_clean();
		return $result;
	}


	function BaseGetEditFormHTML($settings, $multiple=false, $value, $name, $bUserFields=false, $bEditInList = false)
	{

		$template_name = "";
		$admin_section = "N";
		if(strlen(trim($settings["PUBLIC_EDIT_TEMPLATE"]))>0) $template_name = trim($settings["PUBLIC_EDIT_TEMPLATE"]);
		if(substr($GLOBALS["APPLICATION"]->GetCurPage(),0,14)==BX_ROOT."/admin/") {
			$admin_section = "Y";
			if(strlen(trim($settings["ADMIN_EDIT_TEMPLATE"]))>0) $template_name = trim($settings["ADMIN_EDIT_TEMPLATE"]);
		}

		$s = '';
		ob_start();

		$module_mode = CModule::IncludeModuleEx("grain.links");
		
		if($module_mode==MODULE_DEMO) 
			require($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/grain.links/install/trial/trial.php");
		elseif($module_mode==MODULE_DEMO_EXPIRED)
			require($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/grain.links/install/trial/expired.php");
		
		$arParameters = is_array($settings)?$settings:Array(); // when iblock property type changed, there may be empty string and it cause fatal error when unset
		
		unset($arParameters["INTERFACE"]);
		unset($arParameters["PUBLIC_EDIT_TEMPLATE"]);
		unset($arParameters["ADMIN_EDIT_TEMPLATE"]);
		unset($arParameters["ADMIN_FILTER_EDIT_TEMPLATE"]);
		
		$arParameters["INPUT_NAME"] = $name;
		$arParameters["MULTIPLE"] = $multiple?"Y":"N";
		$arParameters["USE_SEARCH"] = in_array($settings["INTERFACE"],Array("search","selectsearch"))?"Y":"N";
		$arParameters["USE_SEARCH_COUNT"] = "";
		$arParameters["EMPTY_SHOW_ALL"] = in_array($settings["INTERFACE"],Array("select","selectsearch"))?"Y":"N";
		$arParameters["NAME_TRUNCATE_LEN"] = "";
		$arParameters["USE_AJAX"] = $settings["INTERFACE"]=="ajax"?"Y":"N";
		$arParameters["AJAX_SEND_POST"] = $settings["AJAX_SEND_POST"]=="Y"?"Y":"N";
		$arParameters["SHOW_VALUE"] = $settings["SHOW_VALUE"]=="Y"?"Y":"N";
		if($bUserFields && $bEditInList)
			$arParameters["AJAX_SEND_POST"] = "Y";
		$arParameters["VALUE"] = $value;
		$arParameters["ADMIN_SECTION"] = $admin_section;
		$arParameters["LEAVE_EMPTY_INPUTS"] = "Y";
		$arParameters["USE_VALUE_ID"] = $bUserFields?"N":"Y";

		if($admin_section=="Y") {
			$bPublicNoTemplate = false;
		} else {
			$bPublicNoTemplate = true;
			$arIncludedFiles = get_included_files();
			foreach($arIncludedFiles as $filename)
				if($filename==str_replace("//","/",$_SERVER["DOCUMENT_ROOT"].BX_ROOT."/header.php"))
					$bPublicNoTemplate = false;
		}

		if($bEditInList || $bPublicNoTemplate || $settings["INCLUDE_SCRIPTS"]=="Y") { 
			$arParameters["INCLUDE_JS"] = "Y";
			$arParameters["INCLUDE_CSS"] = "Y";
		}

		if($bEditInList) {
			$arParameters["BIND_DELAY"] = "500";
		}

		$GLOBALS["APPLICATION"]->IncludeComponent(
			"grain:links.edit",
			$template_name,
			$arParameters,
			null,
			array('HIDE_ICONS' => 'Y')
		);

		$s .= ob_get_contents();
		ob_end_clean();
		
		return  $s;

	}


	function BaseGetFilterHTML($settings, $arValue, $name, $arProperty=false)
	{

		$template_name = "";
		$admin_section = "N";
		if(strlen(trim($settings["PUBLIC_EDIT_TEMPLATE"]))>0) $template_name = trim($settings["PUBLIC_EDIT_TEMPLATE"]);
		if(substr($GLOBALS["APPLICATION"]->GetCurPage(),0,14)==BX_ROOT."/admin/") {
			$admin_section = "Y";
			if(strlen(trim($settings["ADMIN_FILTER_EDIT_TEMPLATE"]))>0) $template_name = trim($settings["ADMIN_FILTER_EDIT_TEMPLATE"]);
		}

		$s = '';
		ob_start();

		// field params
		$FIELD_IDENTIFIER = $name."_fi"; 
		$INPUT_NAME = $name;
			
		$arParameters = $settings;
		unset($arParameters["INTERFACE"]);
		unset($arParameters["PUBLIC_EDIT_TEMPLATE"]);
		unset($arParameters["ADMIN_EDIT_TEMPLATE"]);
		unset($arParameters["ADMIN_FILTER_EDIT_TEMPLATE"]);

		$bNewInterface = true;
		
		if($bNewInterface && $arProperty && $arProperty['IBLOCK_ID']) 
		{
			$arIblock = \Bitrix\Iblock\IblockTable::getById($arProperty['IBLOCK_ID'])->fetch();
			$sTableID = (defined("CATALOG_PRODUCT")? "tbl_product_list_": "tbl_iblock_list_").md5($arIblock['IBLOCK_TYPE_ID'].".".$arIblock['ID']);
			$filterFields = array(array(
				"id" => $name,
				"name" => "doesnotmatter",
				"type" => "custom",
				"value" => "",
				"filterable" => ""
			));
	        $filterOption = new \Bitrix\Main\UI\Filter\Options($sTableID);
	        $filterData = $filterOption->getFilter($filterFields);
			$arValue = array($filterData[$name]);
		}
		
		
		$arParameters["INPUT_NAME"] = $name;
		$arParameters["MULTIPLE"] = "Y";
		$arParameters["USE_SEARCH"] = in_array($settings["INTERFACE"],Array("search","selectsearch"))?"Y":"N";
		$arParameters["USE_SEARCH_COUNT"] = "";
		$arParameters["EMPTY_SHOW_ALL"] = in_array($settings["INTERFACE"],Array("select","selectsearch"))?"Y":"N";
		$arParameters["NAME_TRUNCATE_LEN"] = "";
		$arParameters["USE_AJAX"] = $settings["INTERFACE"]=="ajax"?"Y":"N";
		$arParameters["VALUE"] = $arValue;
		$arParameters["ADMIN_SECTION"] = $admin_section;
		$arParameters["LEAVE_EMPTY_INPUTS"] = "N";
		$arParameters["USE_VALUE_ID"] = "N";
		$arParameters["SCRIPTS_ONLY"] = "Y"; // only include scripts

		// instance params
		$TEMPLATE_IDENTIFIER = "GRAIN_LINKS_EDIT_DEFAULT";
		$INSTANCE_IDENTIFIER = $GLOBALS["APPLICATION"]->IncludeComponent(
			"grain:links.edit",
			$template_name,
			$arParameters,
			null,
			array('HIDE_ICONS' => 'Y')
		);

		$arParameters["USE_AJAX"] = $arParameters["USE_AJAX"] == "Y";
		$arParameters["MULTIPLE"] = $arParameters["MULTIPLE"] == "Y";
		$arParameters["ADMIN_SECTION"] = $arParameters["ADMIN_SECTION"] == "Y";
		$arParameters["SHOW_URL"] = $settings["SHOW_URL"]=="Y";
		$arParameters["AJAX_SEND_POST"] = "N";

		$SELECTED = CGrain_LinksTools::GetSelected($arParameters,$arParameters["VALUE"]);		
		$JS_SELECTED = Array();
		foreach($SELECTED as $value => $arItem) 
			$JS_SELECTED[$value] = true;

		$selectedText = "";
		$values = array();
		foreach($SELECTED as $value=>$arItem)
		{
			$selectedText .= ($selectedText?", ":"").$arItem["NAME"];
			$values = $value;
		}

		?>
		<?if($bNewInterface):?>
		
		<script>
			
		<?=$TEMPLATE_IDENTIFIER?>.params.text_classname = 'grain-links-filter-input';
		<?=$TEMPLATE_IDENTIFIER?>.params.text_placeholded_classname = 'grain-links-filter-input-placeholded';
		<?=$TEMPLATE_IDENTIFIER?>.params.dropdown_classname = '<?=$TEMPLATE_IDENTIFIER?>-dropdown <?=$TEMPLATE_IDENTIFIER?>-dropdown-admin popup-window'; // using "popup-window" class to not to hide filter block on item select
				
	    <?=$TEMPLATE_IDENTIFIER?>.selectitem = function(instance_id,a) {
						
			var field_id = this.opened_field_id;
			
			if(this.instance_params[instance_id].multiple && this.selected[instance_id][field_id][a.rel]) {
				this.destroy(instance_id);
				return;
			}
			
			var container = window.top.document.getElementById(this.field_params[instance_id][field_id].values_id);
			if(container) {
				//if(!this.instance_params[instance_id].multiple) 
					container.innerHTML = "";
				container.innerHTML += (container.innerHTML?", ":"") + this.lists[instance_id]['v'+a.rel].NAME;
				container.parentNode.style.display = '';			
			}
	
			var input_h = window.top.document.getElementById('<?=$INPUT_NAME?>');
			if(input_h) {
			    //if(!this.instance_params[instance_id].multiple)
					this.selected[instance_id][field_id] = {};
				this.selected[instance_id][field_id][a.rel] = true;
				input_h.setAttribute('value',a.rel);
			}
			
			if(typeof(this.instance_params[instance_id].on_after_select)=="function") 
				this.instance_params[instance_id].on_after_select(a.rel,this.lists[instance_id]['v'+a.rel].NAME,this.lists[instance_id]['v'+a.rel].URL,this,instance_id);
	
			this.destroy(instance_id);
			
	    };
	    <?=$TEMPLATE_IDENTIFIER?>.clearfilter = function(instance_id,field_id) {
			
			var container = window.top.document.getElementById(this.field_params[instance_id][field_id].values_id);
			if(container) {
				container.innerHTML = '';
				container.parentNode.style.display = 'none';			
			}
	
			var input_h = window.top.document.getElementById('<?=$INPUT_NAME?>');
			if(input_h)
				input_h.setAttribute('value','');

			this.selected[instance_id][field_id] = {};
	    };
	    </script>

		<style>
		.grain-links-filter {
			display: flex;
			flex-wrap: wrap;
			height: 100%;
			box-sizing: border-box;
			padding: 3px 0;
			margin-left: -7px;
		}
		.grain-links-filter .grain-links-filter-search {
			-webkit-box-flex: 1;
			flex: 1 0 0;
			padding: 0 0 0 5px;
			height: 30px;
		}
		.grain-links-filter .grain-links-filter-values {
			-webkit-box-flex: 0;
			-ms-flex-positive: 0;
			flex-grow: 0;
			flex-basis: min-content;
			margin: 0 3px 3px 0;
    		position: relative;
			display: inline-block;
			float: left;
			padding: 0 30px 0 13px;
			margin: 0 3px 3px 0;
			height: 30px;
			border-radius: 2px;
			background: #bcedfc;
			font: 15px/30px "Helvetica Neue", Arial, Helvetica, sans-serif;
			vertical-align: middle;
			-webkit-transition: background 0.2s linear;
			transition: background 0.2s linear;
			z-index: 2;			
		}
		.grain-links-filter .grain-links-filter-values .grain-links-filter-values-item {
			display: inline-block;
		    max-width: 250px;
		    color: #333;
		    white-space: nowrap;
		    overflow: hidden;
		    text-overflow: ellipsis;			
		}
		.grain-links-filter .grain-links-filter-values .grain-links-filter-values-clear {
			position: absolute;
		    right: 0;
		    top: 0;
		    width: 20px;
		    height: 100%;
		    -webkit-transition: opacity .3s ease;
		    transition: opacity .3s ease;
		    opacity: .3;
		    cursor: pointer;
			height: 30px;
		    width: 25px;
		    background-image: url('data:image/svg+xml;charset=US-ASCII,%3Csvg%20xmlns%3D%22http%3A//www.w3.org/2000/svg%22%20width%3D%2210%22%20height%3D%2210%22%20viewBox%3D%220%200%2010%2010%22%3E%3Cpath%20fill%3D%22%23535C69%22%20fill-rule%3D%22evenodd%22%20d%3D%22M5.065%203.682L2.377.994%201.01%202.362%203.696%205.05.99%207.757l1.368%201.37%202.708-2.71%202.7%202.703%201.37-1.368-2.702-2.7%202.682-2.684L7.748%201%205.065%203.682z%22/%3E%3C/svg%3E');
		    background-repeat: no-repeat;
		    background-position: center;		    
		}
		.grain-links-filter .grain-links-filter-input {
			width: 100%;
			padding: 0;
			height: 30px;
			font: 15px/30px "Helvetica Neue", Arial, Helvetica, sans-serif;
			box-sizing: border-box;			
		}
		.grain-links-filter .grain-links-filter-input.grain-links-filter-input-placeholded {
			color: #999;			
		}
		.grain-links-filter .grain-links-filter-input, .grain-links-filter .grain-links-filter-input:focus {
			border: none;
		}
			
		</style>

		<div class="grain-links-filter">
			<span class="grain-links-filter-values"<?if(count($values)<=0):?> style="display: none;"<?endif?>>
				<div class="grain-links-filter-values-item" id="<?=$FIELD_IDENTIFIER?>_values"><?=$selectedText?></div>
				<div class="grain-links-filter-values-clear" id="<?=$FIELD_IDENTIFIER?>_values_clear"></div>
			</span>
			<span class="grain-links-filter-search">
				<input
					type="text"
					name=""
					class="grain-links-filter-input grain-links-filter-input-placeholded" 
					id="<?=$FIELD_IDENTIFIER?>" 
					value=""
					<?if(!($arParameters["USE_SEARCH"]=="Y" || $arParameters["USE_AJAX"]=="Y")):?> readonly="readonly"<?endif?> />
			</span>
		</div>
		<input 
		    type="hidden" 
			id="<?=$INPUT_NAME?>" 
		    name="<?=$INPUT_NAME?>" 
		    value=""<? /* value gets by replace */ ?> />

		<script>
		<?=$TEMPLATE_IDENTIFIER?>.ibind(
			'<?=$INSTANCE_IDENTIFIER?>',
			'<?=$FIELD_IDENTIFIER?>',
			{
				values_id: '<?=$FIELD_IDENTIFIER?>_values',
				input_name: '<?=$INPUT_NAME?>'
			},
			<?=CUtil::PhpToJsObject($JS_SELECTED)?>,
			500 // timeout
		);
		setTimeout(function(){
			window.top.document.getElementById('<?=$FIELD_IDENTIFIER?>_values_clear').onclick = function(){ <?=$TEMPLATE_IDENTIFIER?>.clearfilter('<?=$INSTANCE_IDENTIFIER?>','<?=$FIELD_IDENTIFIER?>'); }; 
		}, 700);
		</script>
		
		<?else:?>

		<input
			type="text"
			name="<?=$name?>"
			class="<?=$TEMPLATE_IDENTIFIER?>-text <?=$TEMPLATE_IDENTIFIER?>-text-placeholded" 
			id="<?=$FIELD_IDENTIFIER?>" 
			value=""
			<?if(!($arParameters["USE_SEARCH"]=="Y" || $arParameters["USE_AJAX"]=="Y")):?> readonly="readonly"<?endif?> />
		<div>
		<span class="<?=$TEMPLATE_IDENTIFIER?>-values<?if($arParameters["MULTIPLE"]=="Y"):?>-multiple<?endif;echo " ".$TEMPLATE_IDENTIFIER;?>-values<?if($arParameters["MULTIPLE"]=="Y"):?>-multiple<?endif?>-admin" id="<?=$FIELD_IDENTIFIER?>_values">
		    <?foreach($SELECTED as $value=>$arItem):?>
		    	<div 
		    		class="<?=$TEMPLATE_IDENTIFIER?>-values<?if($arParameters["MULTIPLE"]=="Y"):?>-multiple<?endif?>-value" 
		    		id="<?=$INSTANCE_IDENTIFIER?>_<?=$FIELD_IDENTIFIER?>_value_<?=$value?>" 
		    	>
		    		<?if($arParameters["SHOW_URL"]=="Y" && array_key_exists("URL",$arItem)):?><a class="<?=$TEMPLATE_IDENTIFIER?>-values<?if($arParameters["MULTIPLE"]=="Y"):?>-multiple<?endif?>-value-link" href="<?=$arItem["URL"]?>"><?endif;echo $arItem["NAME"];if($arParameters["SHOW_URL"]=="Y" && array_key_exists("URL",$arItem)):?></a><?endif?>
		    		<a 
		    			class="<?=$TEMPLATE_IDENTIFIER?>-values<?if($arParameters["MULTIPLE"]=="Y"):?>-multiple<?endif?>-value-delete" 
		    			href="#" 
		    			onclick="<?=$TEMPLATE_IDENTIFIER?>.deleteitem('<?=$INSTANCE_IDENTIFIER?>','<?=$FIELD_IDENTIFIER?>',this); return false;" 
		    			rel="<?=$value?>"
		    		>x</a>
		    		<input 
		    			type="hidden" 
		    			id="<?=$INSTANCE_IDENTIFIER?>_hidden_<?=$value?>" 
		    			name="<?echo $INPUT_NAME;if($arParameters["MULTIPLE"]=="Y") echo "[]";?>" 
		    			value="<?=$value?>" />
		    	</div>
		    <?endforeach?>
		</span>
		</div>
		<script>
		<?=$TEMPLATE_IDENTIFIER?>.ibind(
			'<?=$INSTANCE_IDENTIFIER?>',
			'<?=$FIELD_IDENTIFIER?>',
			{
				values_id: '<?=$FIELD_IDENTIFIER?>_values',
				input_name: '<?=$INPUT_NAME?>'
			},
			<?=CUtil::PhpToJsObject($JS_SELECTED)?>,
			500 // timeout
		);
		</script>		
		
		<?endif?>
		<?

		$s .= ob_get_contents();
		ob_end_clean();
		
		return  $s;

	}


	function BaseGetViewHTML($settings, $multiple=false, $value, $name) {

		if((!$multiple && strlen($value)>0) || ($multiple && is_array($value) && count($value)>0))
		{

			$arParameters = $settings;
			
			unset($arParameters["INTERFACE"]);
			unset($arParameters["PUBLIC_EDIT_TEMPLATE"]);
			unset($arParameters["ADMIN_EDIT_TEMPLATE"]);
			unset($arParameters["ADMIN_FILTER_EDIT_TEMPLATE"]);
		
			$arParameters["MULTIPLE"] = $multiple;
			$arParameters["ADMIN_SECTION"] = substr($GLOBALS["APPLICATION"]->GetCurPage(),0,14)==BX_ROOT."/admin/";
			$arParameters["SHOW_URL"] = $settings["SHOW_URL"]=="Y";
			$arParameters["USE_AJAX"] = $arParameters["USE_AJAX"] == "Y";
			$arParameters["VALUE"] = $value;

			$arData = CGrain_LinksTools::GetSelected($arParameters,$value);

			$s = '';

			foreach($arData as $arElement) {
				if(strlen($s)>0) $s .= " / ";
				if($arParameters["SHOW_URL"]=="Y" && array_key_exists("URL",$arElement))
					$s .= "<a href=\"".$arElement["URL"]."\" target=\"_blank\">".$arElement["NAME"]."</a>";
				else
					$s .= $arElement["NAME"];
			}
	
			return  $s;

		}
		else return "";
	
	}


	function BaseOnSearchContent($settings,$value) {

		$settings["SHOW_URL"] = "N";
		$search_text = self::BaseGetViewHTML($settings, false, $value, "");

		/*
		$handle = fopen($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/debug.txt", "a");
		fwrite($handle, "\$settings = ".print_r($settings,true)."\n\n");
		fwrite($handle, "\$value = ".print_r($value,true)."\n\n");
		fwrite($handle, "\$search_text = ".$search_text);
		fwrite($handle, "\n\n=============\n\n");
		fclose($handle);
		*/
		
		return $search_text;
	
	}


	function BaseCheckFields($settings,$id,$name,$value)
	{
		$arErrors = array();

		return $arErrors;
	}


}

class CGrain_IBlockPropertyLink
{

	function GetUserTypeDescription()
	{
		return array(
			"PROPERTY_TYPE" =>"S",
			"USER_TYPE" =>"grain_link",
			"DESCRIPTION" => GetMessage("GRAIN_LINKS_PROP_NAME"),
			"GetPublicViewHTML"	=>array("CGrain_IBlockPropertyLink","GetPublicViewHTML"),
			//"GetPublicEditHTML" =>array("CGrain_IBlockPropertyLink","GetPublicEditHTML"),
			"GetAdminListViewHTML" =>array("CGrain_IBlockPropertyLink","GetAdminListViewHTML"),
			"GetPropertyFieldHtml" =>array("CGrain_IBlockPropertyLink","GetPropertyFieldHtml"),
			"GetPropertyFieldHtmlMulty" => array('CGrain_IBlockPropertyLink','GetPropertyFieldHtmlMulty'),
			"GetPublicEditHTML"	=>array("CGrain_IBlockPropertyLink","GetPublicEditHTML"),
			"GetPublicEditHTMLMulty" =>array("CGrain_IBlockPropertyLink","GetPublicEditHTMLMulty"),
			"CheckFields"		=>array("CGrain_IBlockPropertyLink","CheckFields"),
			"PrepareSettings" => array("CGrain_IBlockPropertyLink", "PrepareSettings"),
			"GetSettingsHTML" => array("CGrain_IBlockPropertyLink", "GetSettingsHTML"),
			"GetAdminFilterHTML" => array("CGrain_IBlockPropertyLink", "GetAdminFilterHTML"),
			"GetPublicFilterHTML" => array("CGrain_IBlockPropertyLink", "GetPublicFilterHTML"),
			"AddFilterFields" => array("CGrain_IBlockPropertyLink", "AddFilterFields"),
			"GetSearchContent" => array("CGrain_IBlockPropertyLink", "GetSearchContent"),
		);
	}

	function GetPublicViewHTML($arProperty, $value, $strHTMLControlName)
	{
		return CGrain_PropertyLink::BaseGetViewHTML($arProperty["USER_TYPE_SETTINGS"], false, $value["VALUE"], $strHTMLControlName["VALUE"]);
	}

/*
	function GetPublicEditHTML($arProperty, $value, $strHTMLControlName)
	{
		return CGrain_PropertyLink::BaseGetEditFormHTML($arProperty["USER_TYPE_SETTINGS"], $value["VALUE"], $strHTMLControlName["VALUE"]);
	}
*/

	function GetAdminListViewHTML($arProperty, $value, $strHTMLControlName)
	{
		return CGrain_PropertyLink::BaseGetViewHTML($arProperty["USER_TYPE_SETTINGS"], false, $value["VALUE"], $strHTMLControlName["VALUE"]);
	}

	function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
	{
		return CGrain_PropertyLink::BaseGetEditFormHTML($arProperty["USER_TYPE_SETTINGS"], false, $value["VALUE"], $strHTMLControlName["VALUE"],false,self::__IsListPage());
	}

	function GetPropertyFieldHtmlMulty($arProperty, $arValues, $strHTMLControlName)
	{
		$values = Array();
		foreach($arValues as $intPropertyValueID => $arOneValue) $values[$intPropertyValueID] = array_key_exists("VALUE",$arOneValue)?$arOneValue["VALUE"]:$arOneValue;
		return CGrain_PropertyLink::BaseGetEditFormHTML($arProperty["USER_TYPE_SETTINGS"], true, $values, $strHTMLControlName["VALUE"],false,self::__IsListPage());
	}

	function GetPublicEditHTML($arProperty, $value, $strHTMLControlName)
	{
		return CGrain_PropertyLink::BaseGetEditFormHTML($arProperty["USER_TYPE_SETTINGS"], false, $value["VALUE"], $strHTMLControlName["VALUE"],false,self::__IsListPage());
	}

	function GetPublicEditHTMLMulty($arProperty, $arValues, $strHTMLControlName)
	{ 
		$values = Array();
		foreach($arValues as $intPropertyValueID => $arOneValue) $values[$intPropertyValueID] = array_key_exists("VALUE",$arOneValue)?$arOneValue["VALUE"]:$arOneValue;
		return CGrain_PropertyLink::BaseGetEditFormHTML($arProperty["USER_TYPE_SETTINGS"], true, $values, $strHTMLControlName["VALUE"],false,self::__IsListPage());
	}


	function CheckFields($arProperty, $value)
	{
		return CGrain_PropertyLink::BaseCheckFields(unserialize($arProperty["USER_TYPE_SETTINGS"]),false,$arProperty["NAME"],$value["VALUE"]);
	}

	function PrepareSettings($arProperty)
	{
		return CGrain_PropertyLink::BasePrepareSettings($arProperty, "USER_TYPE_SETTINGS");
	}

	function GetSettingsHTML($arProperty, $strHTMLControlName, $arPropertyFields)
	{
		$result = '';
		$arPropertyFields = array(
			"HIDE" => array("ROW_COUNT", "WITH_DESCRIPTION", "DEFAULT_VALUE", "MULTIPLE_CNT"), //will hide the field
			//"SET" => array("FILTRABLE" => "N"), //if set then hidden field will get this value
			"USER_TYPE_SETTINGS_TITLE" => GetMessage("GRAIN_LINKS_PROP_SETTINGS_HEADER"),
		);

		$arProperty["USER_TYPE_SETTINGS"] = CGrain_IBlockPropertyLink::PrepareSettings($arProperty);
		$val = $arProperty["USER_TYPE_SETTINGS"];

		return CGrain_PropertyLink::BaseGetSettingsHTML($strHTMLControlName["NAME"], $val);
	}

	function GetAdminFilterHTML($arProperty, $strHTMLControlName)
	{
		if(is_array($_REQUEST[$strHTMLControlName["VALUE"]]))
			$arValue = $_REQUEST[$strHTMLControlName["VALUE"]];
		elseif(is_array($GLOBALS[$strHTMLControlName["VALUE"]]))
			$arValue = $GLOBALS[$strHTMLControlName["VALUE"]];
		else 
			$arValue = Array();

		return CGrain_PropertyLink::BaseGetFilterHTML($arProperty["USER_TYPE_SETTINGS"],$arValue,$strHTMLControlName["VALUE"]);
	}

	function GetPublicFilterHTML($arProperty, $strHTMLControlName)
	{
		if(is_array($_REQUEST[$strHTMLControlName["VALUE"]]))
			$arValue = $_REQUEST[$strHTMLControlName["VALUE"]];
		elseif(is_array($GLOBALS[$strHTMLControlName["VALUE"]]))
			$arValue = $GLOBALS[$strHTMLControlName["VALUE"]];
		else 
			$arValue = Array();

		return CGrain_PropertyLink::BaseGetFilterHTML($arProperty["USER_TYPE_SETTINGS"],$arValue,$strHTMLControlName["VALUE"],$arProperty);
	}

	function AddFilterFields($arProperty, $strHTMLControlName, &$arFilter, &$filtered)
	{
		$filtered = false;
		if(is_array($_REQUEST[$strHTMLControlName["VALUE"]]))
			$arValue = $_REQUEST[$strHTMLControlName["VALUE"]];
		elseif(is_array($GLOBALS[$strHTMLControlName["VALUE"]]))
			$arValue = $GLOBALS[$strHTMLControlName["VALUE"]];
		else 
			$arValue = Array();
		if(count($arValue)>0) {
			$arFilter["PROPERTY_".$arProperty["ID"]] = $arValue;
			$filtered = true;
		}
	}

	function GetSearchContent($arProperty, $value, $strHTMLControlName)
	{
		return CGrain_PropertyLink::BaseOnSearchContent($arProperty["USER_TYPE_SETTINGS"],$value['VALUE']);

	}
	
	function __IsListPage() {
		return in_array($GLOBALS["APPLICATION"]->GetCurPage(),Array(
			"/bitrix/admin/iblock_list_admin.php",
			"/bitrix/admin/iblock_element_admin.php",
			"/bitrix/admin/cat_product_list.php",
		));
	}

}

class CGrain_UserPropertyLink
{
	function GetUserTypeDescription()
	{
		return array(
			"USER_TYPE_ID" => "grain_link",
			"CLASS_NAME" => "CGrain_UserPropertyLink",
			"DESCRIPTION" => GetMessage("GRAIN_LINKS_PROP_NAME"),
			"BASE_TYPE" => "string"
		);
	}

	function GetDBColumnType($arUserField)
	{
		global $DB;
		switch(strtolower($DB->type))
		{
			case "mysql":
				return "text";
			case "oracle":
				return "varchar2(2000 char)";
			case "mssql":
				return "varchar(2000)";
		}
	}

	function PrepareSettings($arProperty)
	{
		return CGrain_PropertyLink::BasePrepareSettings($arProperty, "SETTINGS");
	}

	function GetSettingsHTML($arUserField = array(), $arHtmlControl, $bVarsFromForm)
	{
		if(!is_array($arUserField))
			$arUserField = array();

		$arUserField["SETTINGS"] = $bVarsFromForm ? $GLOBALS[$arHtmlControl["NAME"]] : CGrain_UserPropertyLink::PrepareSettings($arUserField);
		return CGrain_PropertyLink::BaseGetSettingsHTML($arHtmlControl["NAME"], $arUserField["SETTINGS"]);
	}


	function GetEditFormHTML($arUserField, $arHtmlControl)
	{
		$value = htmlspecialcharsback($arHtmlControl["VALUE"]); // Unserialize array
		return CGrain_PropertyLink::BaseGetEditFormHTML($arUserField["SETTINGS"], false, $value, $arHtmlControl["NAME"], true);
	}


	function GetEditFormHTMLMulty($arUserField, $arHtmlControl)
	{
		$value = Array();
		if(is_array($arHtmlControl["VALUE"])) 
			foreach($arHtmlControl["VALUE"] as $value_id=>$one_value)
				$value[$value_id] = htmlspecialcharsback($one_value);
		return CGrain_PropertyLink::BaseGetEditFormHTML($arUserField["SETTINGS"], true, $value, preg_replace("/\[\]$/","",$arHtmlControl["NAME"]), true);
	}


	function GetAdminListEditHTML($arUserField, $arHtmlControl)
	{
		$value = htmlspecialcharsback($arHtmlControl["VALUE"]); // Unserialize array
		return CGrain_PropertyLink::BaseGetEditFormHTML($arUserField["SETTINGS"], false, $value, $arHtmlControl["NAME"], true, true);
	}


	function GetAdminListEditHTMLMulty($arUserField, $arHtmlControl)
	{
		$value = Array();
		if(is_array($arHtmlControl["VALUE"])) 
			foreach($arHtmlControl["VALUE"] as $value_id=>$one_value)
				$value[$value_id] = htmlspecialcharsback($one_value);
		return CGrain_PropertyLink::BaseGetEditFormHTML($arUserField["SETTINGS"], true, $value, preg_replace("/\[\]$/","",$arHtmlControl["NAME"]), true, true);
	}


	function GetAdminListViewHTML($arUserField, $arHtmlControl)
	{
		$value = htmlspecialcharsback($arHtmlControl["VALUE"]);
		return CGrain_PropertyLink::BaseGetViewHTML($arUserField["SETTINGS"], false, $value, $arHtmlControl["NAME"]);
	}


	function GetAdminListViewHTMLMulty($arUserField, $arHtmlControl)
	{
		$value=Array();
		if(is_array($arHtmlControl["VALUE"])) foreach($arHtmlControl["VALUE"] as $val)
			$value[] = htmlspecialcharsback($val);
		return CGrain_PropertyLink::BaseGetViewHTML($arUserField["SETTINGS"], true, $value, $arHtmlControl["NAME"]);
	}


	function CheckFields($arUserField, $value)
	{
		return CGrain_PropertyLink::BaseCheckFields($arUserField["SETTINGS"],$arUserField["FIELD_NAME"]."[CODE]",$arUserField["EDIT_FORM_LABEL"],$value);
	}


	function GetFilterHTML($arUserField, $arHtmlControl)
	{ 
		if(is_array($arHtmlControl["VALUE"]))
			$arValue = $arHtmlControl["VALUE"];
		else 
			$arValue = Array();

		return CGrain_PropertyLink::BaseGetFilterHTML($arUserField["SETTINGS"],$arValue,$arHtmlControl["NAME"]);
	}


	function OnSearchIndex($arUserField)
	{
		$value = CGrain_PropertyLink::htmlspecialcharsback($arUserField["VALUE"]); 
		return CGrain_PropertyLink::BaseOnSearchContent($arUserField["SETTINGS"],$value);
	}


	function GetPublicViewHTML($arUserField, $arHtmlControl)
	{
		$value = htmlspecialcharsback($arHtmlControl["VALUE"]);
		return CGrain_PropertyLink::BaseGetViewHTML($arUserField["SETTINGS"], $arUserField["MULTIPLE"]=="Y", $value, $arHtmlControl["NAME"]);
	}


}

/*patchvalidationmutatormark2*/

?>