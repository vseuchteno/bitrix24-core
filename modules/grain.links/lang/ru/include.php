<?

$MESS["GRAIN_LINKS_PROP_NAME"] = "Универсальная привязка";

$MESS ['GRAIN_LINKS_PROP_SETTINGS_HEADER'] = "Данные и интерфейс";
$MESS ['GRAIN_LINKS_PROP_SETTINGS_HEADER_DATA_SOURCE'] = "Источник данных";
$MESS ['GRAIN_LINKS_PROP_SETTINGS_HEADER_INTERFACE'] = "Интерфейс";
$MESS ['GRAIN_LINKS_PROP_SETTINGS_HEADER_TEMPLATES'] = "Оригинальные шаблоны компонента редактирования (grain:links.edit)";

$MESS ['GRAIN_LINKS_PROP_SETTINGS_ADDITIONAL_VALUES'] = "(другое)";

$MESS ['GRAIN_LINKS_PROP_SETTINGS_INTERFACE_TYPE'] = "Тип интерфейса";
$MESS ['GRAIN_LINKS_PROP_SETTINGS_INTERFACE_AJAX'] = "Использовать аякс";
$MESS ['GRAIN_LINKS_PROP_SETTINGS_INTERFACE_SELECT'] = "Простой выбор из списка";
$MESS ['GRAIN_LINKS_PROP_SETTINGS_INTERFACE_SELECTSEARCH'] = "Выбор из списка с возможностью поиска";
$MESS ['GRAIN_LINKS_PROP_SETTINGS_INTERFACE_SEARCH'] = "Только поиск";
$MESS ['GRAIN_LINKS_PROP_SETTINGS_INTERFACE_SHOW_URL'] = "Показывать ссылки на элементы";
$MESS ['GRAIN_LINKS_PROP_SETTINGS_INTERFACE_SHOW_VALUE'] = "Показывать значение (в квадратных скобках)";
$MESS ['GRAIN_LINKS_PROP_SETTINGS_INTERFACE_AJAX_SEND_POST'] = "Отправлять данные POST при аякс";
$MESS ['GRAIN_LINKS_PROP_SETTINGS_INTERFACE_AJAX_SEND_POST_NOTE'] = "В некоторых случаях это позволит увеличить надежность аякс. Включайте данную опцию только если аякс при обычных условиях не работает.";
$MESS ['GRAIN_LINKS_PROP_SETTINGS_INCLUDE_SCRIPTS'] = "Прямое подключение скриптов";
$MESS ['GRAIN_LINKS_PROP_SETTINGS_INCLUDE_SCRIPTS_NOTE'] = "Используйте эту опцию, если по-другому редактирование привязок не работает";

$MESS ['GRAIN_LINKS_PROP_SETTINGS_PUBLIC_EDIT_TEMPLATE'] = "Для публичной части";
$MESS ['GRAIN_LINKS_PROP_SETTINGS_ADMIN_EDIT_TEMPLATE'] = "Для административной части";
$MESS ['GRAIN_LINKS_PROP_SETTINGS_ADMIN_FILTER_EDIT_TEMPLATE'] = "Для фильтров в адм. части";

$MESS ['GRAIN_LINKS_DATA_SOURCE_ARRAY_SIMPLE'] = "Простой массив";
$MESS ['GRAIN_LINKS_DATA_SOURCE_ARRAY_EXTENDED'] = "Сложный массив";
$MESS ['GRAIN_LINKS_DATA_SOURCE_ARRAY_BITRIX'] = "Стандартный массив Битрикс";
$MESS ['GRAIN_LINKS_DATA_SOURCE_HTML_SELECT'] = "Html-код тега select";

?>