<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(CModule::IncludeModule("advertising")) {

	$rsAdvType = CAdvType::GetList();
	$arAdvTypes = Array();
	while($arAdvType = $rsAdvType->GetNext())
		$arAdvTypes[$arAdvType["SID"]] = $arAdvType["NAME"];

	$rsContracts = CAdvContract::GetList();
	$arContracts = Array();
	while($arContract = $rsContracts->GetNext())
		$arContracts[$arContract["ID"]] = $arContract["NAME"];

	$arSorts = Array("ASC"=>GetMessage("GRAIN_LINKS_LIST_ADVERTISING_BANNER_SORT_ASC"), "DESC"=>GetMessage("GRAIN_LINKS_LIST_ADVERTISING_BANNER_SORT_DESC"));
	$arSortFields = Array(
	    "S_ID"=>GetMessage("GRAIN_LINKS_LIST_ADVERTISING_BANNER_SORT_FID"),
	    "S_NAME"=>GetMessage("GRAIN_LINKS_LIST_ADVERTISING_BANNER_SORT_FNAME"),
		"S_CTR"=>GetMessage("GRAIN_LINKS_LIST_ADVERTISING_BANNER_SORT_FCTR"),
		"S_CONTRACT_ID"=>GetMessage("GRAIN_LINKS_LIST_ADVERTISING_BANNER_SORT_FCONTRACT"),
		"S_TYPE_SID"=>GetMessage("GRAIN_LINKS_LIST_ADVERTISING_BANNER_SORT_FTYPE"),
	    "S_SHOW_COUNT"=>GetMessage("GRAIN_LINKS_LIST_ADVERTISING_BANNER_SORT_FSHOWCOUNT"),
	    "S_MAX_SHOW_COUNT"=>GetMessage("GRAIN_LINKS_LIST_ADVERTISING_BANNER_SORT_FMAXSHOWCOUNT"),
	    "S_CLICK_COUNT"=>GetMessage("GRAIN_LINKS_LIST_ADVERTISING_BANNER_SORT_FCLICKCOUNT"),
	    "S_MAX_CLICK_COUNT"=>GetMessage("GRAIN_LINKS_LIST_ADVERTISING_BANNER_SORT_FMAXCLICKCOUNT"),
	    "S_WEIGHT"=>GetMessage("GRAIN_LINKS_LIST_ADVERTISING_BANNER_SORT_FWEIGHT"),
	    "S_DATE_LAST_CLICK"=>GetMessage("GRAIN_LINKS_LIST_ADVERTISING_BANNER_SORT_FLASTCLICK"),
	);

}

$arComponentParameters["PARAMETERS"]["TYPE_SID"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_ADVERTISING_BANNER_PARAM_TYPE_SID"),
	"TYPE" => "LIST",
	"VALUES" => $arAdvTypes,
	"DEFAULT" => "",
	"MULTIPLE" => "Y",
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["CONTRACT_ID"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_ADVERTISING_BANNER_PARAM_CONTRACT_ID"),
	"TYPE" => "LIST",
	"VALUES" => $arContracts,
	"DEFAULT" => "",
	"MULTIPLE" => "Y",
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["ACTIVE"] = array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_ADVERTISING_BANNER_PARAM_ACTIVE"),
	"TYPE" => "CHECKBOX",
    "DEFAULT" => "N",
);

$arComponentParameters["PARAMETERS"]["PUBLISHED"] = array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_ADVERTISING_BANNER_PARAM_PUBLISHED"),
	"TYPE" => "CHECKBOX",
    "DEFAULT" => "N",
);

$arComponentParameters["PARAMETERS"]["CHECK_PERMISSIONS"] = array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_ADVERTISING_BANNER_PARAM_CHECK_PERMISSIONS"),
	"TYPE" => "CHECKBOX",
    "DEFAULT" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_BY"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_ADVERTISING_BANNER_PARAM_SORT_BY"),
	"TYPE" => "LIST",
	"DEFAULT" => "S_NAME",
	"VALUES" => $arSortFields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_ADVERTISING_BANNER_PARAM_SORT_ORDER"),
	"TYPE" => "LIST",
	"DEFAULT" => "ASC",
	"VALUES" => $arSorts,
);

$arComponentParameters["PARAMETERS"]["BANNER_URL"] = Array(
	"NAME" => GetMessage("GRAIN_LINKS_LIST_ADVERTISING_BANNER_PARAM_BANNER_URL"),
	"TYPE" => "STRING",
	"PARENT" => "DATA_SOURCE",
);

?>