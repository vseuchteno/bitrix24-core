<?

if(CModule::IncludeModule("advertising")) {

	/* prepare parameters */

	$arTypes = $arParams["TYPE_SID"];
	$arParams["TYPE_SID"] = Array();
	foreach($arTypes as $type_id)
	    if(strlen($type_id)>0)
	    	$arParams["TYPE_SID"][] = $type_id;
	
	$arBanners = $arParams["CONTRACT_ID"];
	$arParams["CONTRACT_ID"] = Array();
	foreach($arBanners as $contract_id)
	    if(intval($contract_id)>0)
	    	$arParams["CONTRACT_ID"][] = intval($contract_id);

	$arParams["ACTIVE"] = $arParams["ACTIVE"]=="Y";
	$arParams["PUBLISHED"] = $arParams["PUBLISHED"]=="Y";
	$arParams["CHECK_PERMISSIONS"] = $arParams["CHECK_PERMISSIONS"]=="Y";
	
	$arParams["SORT_BY"] = trim($arParams["SORT_BY"]);
	if(strlen($arParams["SORT_BY"])<=0)
	    $arParams["SORT_BY"] = "S_NAME";
	$arParams["SORT_ORDER"] = strtoupper($arParams["SORT_ORDER"]);
	if($arParams["SORT_ORDER"]!="DESC")
	     $arParams["SORT_ORDER"]="ASC";
	    
	/* build list */
	
	$arFilter = Array();

	if($arParams["TYPE_SID"]) {
		$arFilter["TYPE_SID"] = "";
		foreach($arParams["TYPE_SID"] as $sid) {
			if(strlen($arFilter["TYPE_SID"])>0) $arFilter["TYPE_SID"] .= "|";
			$arFilter["TYPE_SID"] .= $sid;
		}
	}

	if($arParams["CONTRACT_ID"]) {
		$arFilter["CONTRACT_ID"] = "";
		foreach($arParams["CONTRACT_ID"] as $contract_id) {
			if(strlen($arFilter["CONTRACT_ID"])>0) $arFilter["CONTRACT_ID"] .= "|";
			$arFilter["CONTRACT_ID"] .= $contract_id;
		}
	}

	if($arParams["ACTIVE"])
		$arFilter["ACTIVE"] = "Y";
	if($arParams["PUBLISHED"])
		$arFilter["STATUS_SID"] = "PUBLISHED";
	
	if($arResult["AJAX_RETURN"]) {
		$arFilter["NAME"] = $arResult["AJAX_SEARCH_QUERY"];
		$arFilter["NAME_EXACT_MATCH"] = "N";
	}
	
	if($arResult["SELECTED_VALUE"]) {
		$arFilter["ID"] = "";
		if(is_array($arResult["SELECTED_VALUE"])) {
			foreach($arResult["SELECTED_VALUE"] as $value) {
				if(strlen($arFilter["ID"])>0) $arFilter["ID"] .= "|";
				$arFilter["ID"] .= $value;
			}
		} else $arFilter["ID"] = $arResult["SELECTED_VALUE"];
	}

	$rsBanners = CAdvBanner::GetList(($by=$arParams["SORT_BY"]), ($order=$arParams["SORT_ORDER"]),$arFilter,$is_filtered,$arParams["CHECK_PERMISSIONS"]?"Y":"N");
	
	while($arBanner=$rsBanners->GetNext()) {
	
	    $arItem = Array(
	    	"NAME" => $arBanner["NAME"],
	    );
	    
	    if($arParams["SHOW_URL"] && $arParams["BANNER_URL"]) {
	    
	    	$arBanner["SITE_SERVER_NAME"] = defined("SITE_SERVER_NAME")?SITE_SERVER_NAME:"";
	    	$arBanner["SITE_ID"] = defined("SITE_ID")?SITE_ID:"";
	    	$arBanner["SITE_DIR"] = defined("SITE_DIR")?SITE_DIR:"";
	    	
	    	$arItem["URL"] = $arParams["BANNER_URL"];
	    	foreach($arBanner as $FIELD_NAME=>$FIELD_VALUE)
	    		if(substr($FIELD_NAME,0,1)!="~")
	    			$arItem["URL"] = str_replace("#".$FIELD_NAME."#",$FIELD_VALUE,$arItem["URL"]);
	
	    	$arItem["URL"] = preg_replace("'/+'s", "/", $arItem["URL"]);
	    	$arItem["URL"] = htmlspecialcharsbx($arItem["URL"]);
	    	
	    }
	    
	    $arResult["DATA"][$arBanner["ID"]] = $arItem;
	    
	}
	
}
?>