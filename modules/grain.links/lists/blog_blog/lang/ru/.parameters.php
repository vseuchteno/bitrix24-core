<?

$MESS["GRAIN_LINKS_LIST_BLOG_BLOG_PARAM_GROUP_ID"] = "Группы блогов";
$MESS["GRAIN_LINKS_LIST_BLOG_BLOG_PARAM_ACTIVE"] = "Только активные";

$MESS["GRAIN_LINKS_LIST_BLOG_BLOG_PARAM_SORT_BY1"] = "Поле для первой сортировки";
$MESS["GRAIN_LINKS_LIST_BLOG_BLOG_PARAM_SORT_ORDER1"] = "Направление для первой сортировки";
$MESS["GRAIN_LINKS_LIST_BLOG_BLOG_PARAM_SORT_BY2"] = "Поле для второй сортировки";
$MESS["GRAIN_LINKS_LIST_BLOG_BLOG_PARAM_SORT_ORDER2"] = "Направление для второй сортировки";

$MESS["GRAIN_LINKS_LIST_BLOG_BLOG_SORT_ASC"] = "По возрастанию";
$MESS["GRAIN_LINKS_LIST_BLOG_BLOG_SORT_DESC"] = "По убыванию";
$MESS["GRAIN_LINKS_LIST_BLOG_BLOG_SORT_FID"] = "ID";
$MESS["GRAIN_LINKS_LIST_BLOG_BLOG_SORT_FNAME"] = "Название";
$MESS["GRAIN_LINKS_LIST_BLOG_BLOG_SORT_FDATECRT"] = "Дата создания";
$MESS["GRAIN_LINKS_LIST_BLOG_BLOG_SORT_FDATEUPD"] = "Дата последнего изменения";
$MESS["GRAIN_LINKS_LIST_BLOG_BLOG_SORT_FURL"] = "Адрес блога";
$MESS["GRAIN_LINKS_LIST_BLOG_BLOG_SORT_FLPD"] = "Дата последнего сообщения";

$MESS["GRAIN_LINKS_LIST_BLOG_BLOG_PARAM_BLOG_URL"] = "Шаблон пути к блогу";

?>