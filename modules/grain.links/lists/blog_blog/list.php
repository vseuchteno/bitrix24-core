<?
if(CModule::IncludeModule("blog")) {

	/* prepare parameters */

	$arGroups = $arParams["GROUP_ID"];
	$arParams["GROUP_ID"] = Array();
	foreach($arGroups as $group_id)
		if(intval($group_id)>0)
			$arParams["GROUP_ID"][] = intval($group_id);
	
	$arParams["ACTIVE"] = $arParams["ACTIVE"]=="Y";
	
	$arParams["SORT_BY1"] = trim($arParams["SORT_BY1"]);
	if(strlen($arParams["SORT_BY1"])<=0)
		$arParams["SORT_BY1"] = "NAME";
	$arParams["SORT_ORDER1"] = strtoupper($arParams["SORT_ORDER1"]);
	if($arParams["SORT_ORDER1"]!="DESC")
		 $arParams["SORT_ORDER1"]="ASC";
	if(strlen($arParams["SORT_BY2"])<=0)
		$arParams["SORT_BY2"] = "ID";
	$arParams["SORT_ORDER2"] = strtoupper($arParams["SORT_ORDER2"]);
	if($arParams["SORT_ORDER2"]!="ASC")
		 $arParams["SORT_ORDER2"]="DESC";
		
	/* build list */
	
	$arSort = array(
		$arParams["SORT_BY1"]=>$arParams["SORT_ORDER1"],
		$arParams["SORT_BY2"]=>$arParams["SORT_ORDER2"],
	);
	if(!array_key_exists("ID", $arSort))
		$arSort["ID"] = "DESC";
	
	$arFilter = array (
		"GROUP_ID" => $arParams["GROUP_ID"],
	);

	if($arParams["ACTIVE"])
		$arFilter["ACTIVE"] = "Y";

	if($arResult["AJAX_RETURN"]) $arFilter["%NAME"] = $arResult["AJAX_SEARCH_QUERY"];
	
	if($arResult["SELECTED_VALUE"]) $arFilter["ID"] = $arResult["SELECTED_VALUE"];

	$rsBlogs = CBlog::GetList($arSort,$arFilter);
	
	while($arBlog=$rsBlogs->GetNext()) {

		$arItem = Array(
			"NAME" => $arBlog["NAME"],
		);
		
		if($arParams["SHOW_URL"] && $arParams["BLOG_URL"]) {
		
			$arBlog["SITE_SERVER_NAME"] = defined("SITE_SERVER_NAME")?SITE_SERVER_NAME:"";
			$arBlog["SITE_ID"] = defined("SITE_ID")?SITE_ID:"";
			$arBlog["SITE_DIR"] = defined("SITE_DIR")?SITE_DIR:"";
			
			$arItem["URL"] = $arParams["BLOG_URL"];
			foreach($arBlog as $FIELD_NAME=>$FIELD_VALUE)
				if(substr($FIELD_NAME,0,1)!="~")
					$arItem["URL"] = str_replace("#".$FIELD_NAME."#",$FIELD_VALUE,$arItem["URL"]);

			$arItem["URL"] = preg_replace("'/+'s", "/", $arItem["URL"]);
			$arItem["URL"] = htmlspecialcharsbx($arItem["URL"]);
			
		}
		
		$arResult["DATA"][$arBlog["ID"]] = $arItem;
		
	}
	
}
?>