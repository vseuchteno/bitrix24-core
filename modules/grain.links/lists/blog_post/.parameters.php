<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


if(CModule::IncludeModule("blog")) {
	
	$rsGroup = CBlogGroup::GetList(Array("NAME"=>"ASC"), Array(), false, false, Array("ID","NAME"));
	$arGroups = Array();
	while ($arGroup = $rsGroup->GetNext())
		$arGroups[$arGroup["ID"]] = $arGroup["NAME"];

	$arFilter = Array();
	if($arCurrentValues["GROUP_ID"])
		$arFilter["GROUP_ID"] = $arCurrentValues["GROUP_ID"];
	$arTemp = $arFilter["GROUP_ID"];
	$arFilter["GROUP_ID"] = Array();
	foreach($arTemp as $temp_id)
		if(intval($temp_id)>0)
			$arFilter["GROUP_ID"][] = intval($temp_id);
	
	$rsBlogs = CBlog::GetList(Array("NAME"=>"ASC"),$arFilter);
	$arBlogs = Array();
	while($arBlog = $rsBlogs->Fetch())
		$arBlogs[$arBlog["ID"]] = $arBlog["NAME"];

	$arSorts = Array("ASC"=>GetMessage("GRAIN_LINKS_LIST_BLOG_POST_SORT_ASC"), "DESC"=>GetMessage("GRAIN_LINKS_LIST_BLOG_POST_SORT_DESC"));
	$arSortFields = Array(
		"ID"=>GetMessage("GRAIN_LINKS_LIST_BLOG_POST_SORT_FID"),
		"TITLE"=>GetMessage("GRAIN_LINKS_LIST_BLOG_POST_SORT_FNAME"),
		"DATE_CREATE"=>GetMessage("GRAIN_LINKS_LIST_BLOG_POST_SORT_FDATECRT"),
		"DATE_PUBLISH"=>GetMessage("GRAIN_LINKS_LIST_BLOG_POST_SORT_FDATEPUB"),
		"NUM_COMMENTS"=>GetMessage("GRAIN_LINKS_LIST_BLOG_POST_SORT_FCOMMCOUNT"),
		"VIEWS"=>GetMessage("GRAIN_LINKS_LIST_BLOG_POST_SORT_FVIEWS"),
	);

}

$arComponentParameters["PARAMETERS"]["GROUP_ID"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_BLOG_POST_PARAM_GROUP_ID"),
	"TYPE" => "LIST",
	"VALUES" => $arGroups,
	"DEFAULT" => "",
	"MULTIPLE" => "Y",
	"ADDITIONAL_VALUES" => "Y",
	"REFRESH" => "Y",
);

$arComponentParameters["PARAMETERS"]["BLOG_ID"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_BLOG_POST_PARAM_BLOG_ID"),
	"TYPE" => "LIST",
	"VALUES" => $arBlogs,
	"DEFAULT" => "",
	"MULTIPLE" => "Y",
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["USER_LOGIN"] = Array(
	"NAME" => GetMessage("GRAIN_LINKS_LIST_BLOG_POST_PARAM_USER_LOGIN"),
	"TYPE" => "STRING",
	"MULTIPLE" => "Y",
	"PARENT" => "DATA_SOURCE",
);

$arComponentParameters["PARAMETERS"]["OWN"] = array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_BLOG_POST_PARAM_OWN"),
	"TYPE" => "CHECKBOX",
    "DEFAULT" => "N",
);

$arComponentParameters["PARAMETERS"]["ACTIVE"] = array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_BLOG_POST_PARAM_ACTIVE"),
	"TYPE" => "CHECKBOX",
    "DEFAULT" => "N",
);

$arComponentParameters["PARAMETERS"]["SORT_BY1"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_BLOG_POST_PARAM_SORT_BY1"),
	"TYPE" => "LIST",
	"DEFAULT" => "DATE_PUBLISH",
	"VALUES" => $arSortFields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER1"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_BLOG_POST_PARAM_SORT_ORDER1"),
	"TYPE" => "LIST",
	"DEFAULT" => "DESC",
	"VALUES" => $arSorts,
);

$arComponentParameters["PARAMETERS"]["SORT_BY2"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_BLOG_POST_PARAM_SORT_BY2"),
	"TYPE" => "LIST",
	"DEFAULT" => "TITLE",
	"VALUES" => $arSortFields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER2"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_BLOG_POST_PARAM_SORT_ORDER2"),
	"TYPE" => "LIST",
	"DEFAULT" => "ASC",
	"VALUES" => $arSorts,
);

$arComponentParameters["PARAMETERS"]["POST_URL"] = Array(
	"NAME" => GetMessage("GRAIN_LINKS_LIST_BLOG_POST_PARAM_POST_URL"),
	"TYPE" => "STRING",
	"PARENT" => "DATA_SOURCE",
);

?>