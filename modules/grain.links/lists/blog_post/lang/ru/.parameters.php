<?

$MESS["GRAIN_LINKS_LIST_BLOG_POST_PARAM_GROUP_ID"] = "Группы блогов (только для выборки блогов ниже)";
$MESS["GRAIN_LINKS_LIST_BLOG_POST_PARAM_BLOG_ID"] = "Блоги";
$MESS["GRAIN_LINKS_LIST_BLOG_POST_PARAM_USER_LOGIN"] = "Логины пользователей";
$MESS["GRAIN_LINKS_LIST_BLOG_POST_PARAM_OWN"] = "Только свои";
$MESS["GRAIN_LINKS_LIST_BLOG_POST_PARAM_ACTIVE"] = "Только активные";

$MESS["GRAIN_LINKS_LIST_BLOG_POST_PARAM_SORT_BY1"] = "Поле для первой сортировки";
$MESS["GRAIN_LINKS_LIST_BLOG_POST_PARAM_SORT_ORDER1"] = "Направление для первой сортировки";
$MESS["GRAIN_LINKS_LIST_BLOG_POST_PARAM_SORT_BY2"] = "Поле для второй сортировки";
$MESS["GRAIN_LINKS_LIST_BLOG_POST_PARAM_SORT_ORDER2"] = "Направление для второй сортировки";

$MESS["GRAIN_LINKS_LIST_BLOG_POST_SORT_ASC"] = "По возрастанию";
$MESS["GRAIN_LINKS_LIST_BLOG_POST_SORT_DESC"] = "По убыванию";
$MESS["GRAIN_LINKS_LIST_BLOG_POST_SORT_FID"] = "ID";
$MESS["GRAIN_LINKS_LIST_BLOG_POST_SORT_FNAME"] = "Название";
$MESS["GRAIN_LINKS_LIST_BLOG_POST_SORT_FDATECRT"] = "Дата создания";
$MESS["GRAIN_LINKS_LIST_BLOG_POST_SORT_FDATEPUB"] = "Дата публикации";
$MESS["GRAIN_LINKS_LIST_BLOG_POST_SORT_FCOMMCOUNT"] = "Количество комментариев";
$MESS["GRAIN_LINKS_LIST_BLOG_POST_SORT_FVIEWS"] = "Количество просмотров";

$MESS["GRAIN_LINKS_LIST_BLOG_POST_PARAM_POST_URL"] = "Шаблон пути к сообщению";

?>