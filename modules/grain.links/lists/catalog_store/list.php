<?
if(CModule::IncludeModule("sale")) {

	/* prepare parameters */

	$arParams["ACTIVE"] = $arParams["ACTIVE"]=="Y";
	
	$arParams["SORT_BY1"] = trim($arParams["SORT_BY1"]);
	if(strlen($arParams["SORT_BY1"])<=0)
		$arParams["SORT_BY1"] = "SORT";
	$arParams["SORT_ORDER1"] = strtoupper($arParams["SORT_ORDER1"]);
	if($arParams["SORT_ORDER1"]!="DESC")
		 $arParams["SORT_ORDER1"]="ASC";
	if(strlen($arParams["SORT_BY2"])<=0)
		$arParams["SORT_BY2"] = "ADDRESS";
	$arParams["SORT_ORDER2"] = strtoupper($arParams["SORT_ORDER2"]);
	if($arParams["SORT_ORDER2"]!="DESC")
		 $arParams["SORT_ORDER2"]="ASC";
		
	/* build list */
	
	$arSort = array(
		$arParams["SORT_BY1"]=>$arParams["SORT_ORDER1"],
		$arParams["SORT_BY2"]=>$arParams["SORT_ORDER2"],
	);
	if(!array_key_exists("ID", $arSort))
		$arSort["ID"] = "DESC";
	
	$arFilter=Array();
	
	if($arParams["ACTIVE"])
		$arFilter["ACTIVE"] = "Y";

	if($arResult["AJAX_RETURN"]) $arFilter["%TITLE"] = $arResult["AJAX_SEARCH_QUERY"];
	
	if($arResult["SELECTED_VALUE"]) $arFilter["ID"] = $arResult["SELECTED_VALUE"];

	$rsStore = CCatalogStore::GetList($arSort,$arFilter,false,false,Array("ID","TITLE","ADDRESS"));
	
	while($arStore=$rsStore->GetNext()) {

		$arItem = Array(
			"NAME" => $arStore["TITLE"],
		);
				
		$arResult["DATA"][$arStore["ID"]] = $arItem;
		
	}
	
}
?>