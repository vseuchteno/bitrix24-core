<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(CModule::IncludeModule("forum")) {

	$arGroups = Array("0"=>GetMessage("GRAIN_LINKS_LIST_FORUM_FORUM_PARAM_PARENT_GROUP_ROOT"));
	$rsGroups = CForumGroup::GetListEx(Array("left_margin"=>"asc"),Array("LID"=>LANGUAGE_ID));	
	while($arGroup=$rsGroups->GetNext())
		$arGroups[$arGroup["ID"]] = str_repeat(".",$arGroup["DEPTH_LEVEL"]).$arGroup["NAME"];

	$arSorts = Array("ASC"=>GetMessage("GRAIN_LINKS_LIST_FORUM_FORUM_SORT_ASC"), "DESC"=>GetMessage("GRAIN_LINKS_LIST_FORUM_FORUM_SORT_DESC"));
	$arSortFields = Array(
		"ID"=>GetMessage("GRAIN_LINKS_LIST_FORUM_FORUM_SORT_FID"),
		"NAME"=>GetMessage("GRAIN_LINKS_LIST_FORUM_FORUM_SORT_FNAME"),
		"SORT"=>GetMessage("GRAIN_LINKS_LIST_FORUM_FORUM_SORT_FSORT"),
		"TOPICS"=>GetMessage("GRAIN_LINKS_LIST_FORUM_FORUM_SORT_FTOPICS"),
		"POSTS"=>GetMessage("GRAIN_LINKS_LIST_FORUM_FORUM_SORT_FPOSTS"),
		"LAST_POST_DATE"=>GetMessage("GRAIN_LINKS_LIST_FORUM_FORUM_SORT_FLPD"),
	);

}

$arComponentParameters["PARAMETERS"]["GROUP_ID"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_FORUM_FORUM_PARAM_GROUP_ID"),
	"TYPE" => "LIST",
	"VALUES" => $arGroups,
	"DEFAULT" => '',
	"ADDITIONAL_VALUES" => "Y",
	"MULTIPLE" => "Y",
	//"REFRESH" => "Y",
);

$arComponentParameters["PARAMETERS"]["ACTIVE"] = array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_FORUM_FORUM_PARAM_ACTIVE"),
	"TYPE" => "CHECKBOX",
    "DEFAULT" => "N",
);

$arComponentParameters["PARAMETERS"]["CHECK_PERMISSIONS"] = array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_FORUM_FORUM_PARAM_CHECK_PERMISSIONS"),
	"TYPE" => "CHECKBOX",
    "DEFAULT" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_BY1"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_FORUM_FORUM_PARAM_SORT_BY1"),
	"TYPE" => "LIST",
	"DEFAULT" => "NAME",
	"VALUES" => $arSortFields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER1"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_FORUM_FORUM_PARAM_SORT_ORDER1"),
	"TYPE" => "LIST",
	"DEFAULT" => "ASC",
	"VALUES" => $arSorts,
);

$arComponentParameters["PARAMETERS"]["SORT_BY2"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_FORUM_FORUM_PARAM_SORT_BY2"),
	"TYPE" => "LIST",
	"DEFAULT" => "SORT",
	"VALUES" => $arSortFields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER2"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_FORUM_FORUM_PARAM_SORT_ORDER2"),
	"TYPE" => "LIST",
	"DEFAULT" => "ASC",
	"VALUES" => $arSorts,
);

$arComponentParameters["PARAMETERS"]["FORUM_URL"] = Array(
	"NAME" => GetMessage("GRAIN_LINKS_LIST_FORUM_FORUM_PARAM_FORUM_URL"),
	"TYPE" => "STRING",
	"PARENT" => "DATA_SOURCE",
);

?>