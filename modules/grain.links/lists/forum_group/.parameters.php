<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(CModule::IncludeModule("forum")) {

	$arGroups = Array("0"=>GetMessage("GRAIN_LINKS_LIST_FORUM_GROUP_PARAM_PARENT_GROUP_ROOT"));
	$rsGroups = CForumGroup::GetListEx(Array("left_margin"=>"asc"),Array("LID"=>LANGUAGE_ID));	
	while($arGroup=$rsGroups->GetNext())
		$arGroups[$arGroup["ID"]] = str_repeat(".",$arGroup["DEPTH_LEVEL"]).$arGroup["NAME"];

	$arSorts = Array("ASC"=>GetMessage("GRAIN_LINKS_LIST_FORUM_GROUP_SORT_ASC"), "DESC"=>GetMessage("GRAIN_LINKS_LIST_FORUM_GROUP_SORT_DESC"));
	$arSortFields = Array(
		"ID"=>GetMessage("GRAIN_LINKS_LIST_FORUM_GROUP_SORT_FID"),
		"NAME"=>GetMessage("GRAIN_LINKS_LIST_FORUM_GROUP_SORT_FNAME"),
		"SORT"=>GetMessage("GRAIN_LINKS_LIST_FORUM_GROUP_SORT_FSORT"),
	);

}

$arComponentParameters["PARAMETERS"]["PARENT_GROUP"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_FORUM_GROUP_PARAM_PARENT_GROUP"),
	"TYPE" => "LIST",
	"VALUES" => $arGroups,
	"DEFAULT" => '',
	"ADDITIONAL_VALUES" => "Y",
	//"REFRESH" => "Y",
);

$arComponentParameters["PARAMETERS"]["INCLUDE_SUBGROUPS"] = array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_FORUM_GROUP_PARAM_INCLUDE_SUBGROUPS"),
	"TYPE" => "CHECKBOX",
    "DEFAULT" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_BY1"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_FORUM_GROUP_PARAM_SORT_BY1"),
	"TYPE" => "LIST",
	"DEFAULT" => "NAME",
	"VALUES" => $arSortFields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER1"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_FORUM_GROUP_PARAM_SORT_ORDER1"),
	"TYPE" => "LIST",
	"DEFAULT" => "ASC",
	"VALUES" => $arSorts,
);

$arComponentParameters["PARAMETERS"]["SORT_BY2"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_FORUM_GROUP_PARAM_SORT_BY2"),
	"TYPE" => "LIST",
	"DEFAULT" => "SORT",
	"VALUES" => $arSortFields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER2"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_FORUM_GROUP_PARAM_SORT_ORDER2"),
	"TYPE" => "LIST",
	"DEFAULT" => "ASC",
	"VALUES" => $arSorts,
);

$arComponentParameters["PARAMETERS"]["GROUP_URL"] = Array(
	"NAME" => GetMessage("GRAIN_LINKS_LIST_FORUM_GROUP_PARAM_GROUP_URL"),
	"TYPE" => "STRING",
	"PARENT" => "DATA_SOURCE",
);

?>