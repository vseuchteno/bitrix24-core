<?
if(CModule::IncludeModule("forum")) {

	/* prepare parameters */
	
	$arParams["PARENT_GROUP"] = intval($arParams["PARENT_GROUP"]);
	$arParams["INCLUDE_SUBGROUPS"] = $arParams["INCLUDE_SUBGROUPS"]=="Y";
	
	$arParams["SORT_BY1"] = trim($arParams["SORT_BY1"]);
	if(strlen($arParams["SORT_BY1"])<=0)
		$arParams["SORT_BY1"] = "NAME";
	$arParams["SORT_ORDER1"] = strtoupper($arParams["SORT_ORDER1"]);
	if($arParams["SORT_ORDER1"]!="DESC")
		 $arParams["SORT_ORDER1"]="ASC";
	if(strlen($arParams["SORT_BY2"])<=0)
		$arParams["SORT_BY2"] = "SORT";
	$arParams["SORT_ORDER2"] = strtoupper($arParams["SORT_ORDER2"]);
	if($arParams["SORT_ORDER2"]!="DESC")
		 $arParams["SORT_ORDER2"]="ASC";
		
	/* build list */
	
	$arSort = Array();
	if($arParams["INCLUDE_SUBGROUPS"])
		$arSort["LEFT_MARGIN"] = "ASC";
	$arSort[$arParams["SORT_BY1"]] = $arParams["SORT_ORDER1"];
	$arSort[$arParams["SORT_BY2"]] = $arParams["SORT_ORDER2"];
	if(!array_key_exists("ID", $arSort))
		$arSort["ID"] = "DESC";
	
	$arFilter = Array(
		"LID"=>LANGUAGE_ID,
	);

	if(!$arParams["INCLUDE_SUBGROUPS"]) {
		if($arParams["PARENT_GROUP"]) 
			$arFilter["PARENT_ID"] = $arParams["PARENT_GROUP"];
		else
			$arFilter["PARENT_ID"] = 0;			
	} 

	if($arResult["SELECTED_VALUE"] && !$arParams["INCLUDE_SUBGROUPS"]) 
		$arFilter["ID"] = $arResult["SELECTED_VALUE"];
	
	$rsGroups = CForumGroup::GetListEx($arSort,$arFilter);

	$arParentGroup = false;
	
	while($arGroup=$rsGroups->GetNext()) {

		if($arParams["PARENT_GROUP"] && $arParams["INCLUDE_SUBGROUPS"]) {
			if(!$arParentGroup && $arParams["PARENT_GROUP"]==$arGroup["ID"]) {
				$arParentGroup = $arGroup; 
				continue;
			} elseif(!$arParentGroup) {
				continue;
			} elseif($arParentGroup && $arGroup["DEPTH_LEVEL"]<=$arParentGroup["DEPTH_LEVEL"]) {
				break;
			}
		} 
	
		if($arParams["INCLUDE_SUBGROUPS"] && $arResult["SELECTED_VALUE"]) {
			if(
				is_array($arResult["SELECTED_VALUE"]) && !in_array($arGroup["ID"],$arResult["SELECTED_VALUE"])
				|| !is_array($arResult["SELECTED_VALUE"]) && $arGroup["ID"]!=$arResult["SELECTED_VALUE"]
			)
				continue;
		} 
					
		$parentDepthLevel = false;
		if($arParams["PARENT_GROUP"] && $arParentGroup)
			$parentDepthLevel = $arParentGroup["DEPTH_LEVEL"];
		elseif(!$arParams["PARENT_GROUP"])
			$parentDepthLevel = 0;
		
		$arItem = Array(
			"NAME" => ($parentDepthLevel!==false?str_repeat(".",$arGroup["DEPTH_LEVEL"]-$parentDepthLevel-1):"").$arGroup["NAME"],
		);
		
		if($arParams["SHOW_URL"] && $arParams["GROUP_URL"]) {
		
			$arGroup["SITE_SERVER_NAME"] = defined("SITE_SERVER_NAME")?SITE_SERVER_NAME:"";
			$arGroup["SITE_ID"] = defined("SITE_ID")?SITE_ID:"";
			$arGroup["SITE_DIR"] = defined("SITE_DIR")?SITE_DIR:"";
			
			$arItem["URL"] = $arParams["GROUP_URL"];
			foreach($arGroup as $FIELD_NAME=>$FIELD_VALUE)
				if(substr($FIELD_NAME,0,1)!="~")
					$arItem["URL"] = str_replace("#".$FIELD_NAME."#",$FIELD_VALUE,$arItem["URL"]);

			$arItem["URL"] = preg_replace("'/+'s", "/", $arItem["URL"]);
			$arItem["URL"] = htmlspecialcharsbx($arItem["URL"]);
			
		}
		
		$arResult["DATA"][$arGroup["ID"]] = $arItem;
			
	}
	
	if($arResult["AJAX_RETURN"]) {
		foreach($arResult["DATA"] as $key => $arItem) 
			if(!preg_match("/".preg_quote($arResult["AJAX_SEARCH_QUERY"])."/i".(BX_UTF?"u":""),$arItem["NAME"]))
				unset($arResult["DATA"][$key]);
	}
	
}
?>