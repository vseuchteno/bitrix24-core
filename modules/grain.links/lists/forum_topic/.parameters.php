<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(CModule::IncludeModule("forum")) {

	$arGroups = Array("0"=>GetMessage("GRAIN_LINKS_LIST_FORUM_TOPIC_PARAM_PARENT_GROUP_ROOT"));
	$rsGroups = CForumGroup::GetListEx(Array("left_margin"=>"asc"),Array("LID"=>LANGUAGE_ID));	
	while($arGroup=$rsGroups->GetNext())
		$arGroups[$arGroup["ID"]] = str_repeat(".",$arGroup["DEPTH_LEVEL"]).$arGroup["NAME"];

	$arFilter = Array();
	if($arCurrentValues["GROUP_ID"])
		$arFilter["FORUM_GROUP_ID"] = intval($arCurrentValues["GROUP_ID"]);
	$rsForums = CForumNew::GetList(Array("NAME"=>"ASC"),$arFilter);
	$arForums = Array();
	while($arForum = $rsForums->Fetch())
		$arForums[$arForum["ID"]] = $arForum["NAME"];

	$arSorts = Array("ASC"=>GetMessage("GRAIN_LINKS_LIST_FORUM_TOPIC_SORT_ASC"), "DESC"=>GetMessage("GRAIN_LINKS_LIST_FORUM_TOPIC_SORT_DESC"));
	$arSortFields = Array(
		"ID"=>GetMessage("GRAIN_LINKS_LIST_FORUM_TOPIC_SORT_FID"),
		"TITLE"=>GetMessage("GRAIN_LINKS_LIST_FORUM_TOPIC_SORT_FTITLE"),
		"STATE"=>GetMessage("GRAIN_LINKS_LIST_FORUM_TOPIC_SORT_FSTATE"),
		"VIEWS"=>GetMessage("GRAIN_LINKS_LIST_FORUM_TOPIC_SORT_FVIEWS"),
		"POSTS"=>GetMessage("GRAIN_LINKS_LIST_FORUM_TOPIC_SORT_FPOSTS"),
		"LAST_POST_DATE"=>GetMessage("GRAIN_LINKS_LIST_FORUM_TOPIC_SORT_FLPD"),
	);

}

$arComponentParameters["PARAMETERS"]["GROUP_ID"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_FORUM_TOPIC_PARAM_GROUP_ID"),
	"TYPE" => "LIST",
	"VALUES" => $arGroups,
	"DEFAULT" => '',
	"ADDITIONAL_VALUES" => "Y",
	"REFRESH" => "Y",
);

$arComponentParameters["PARAMETERS"]["FORUM_ID"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_BLOG_POST_PARAM_FORUM_ID"),
	"TYPE" => "LIST",
	"VALUES" => $arForums,
	"DEFAULT" => "",
	"MULTIPLE" => "Y",
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["APPROVED"] = array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_FORUM_TOPIC_PARAM_APPROVED"),
	"TYPE" => "CHECKBOX",
    "DEFAULT" => "N",
);

$arComponentParameters["PARAMETERS"]["SORT_BY1"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_FORUM_TOPIC_PARAM_SORT_BY1"),
	"TYPE" => "LIST",
	"DEFAULT" => "TITLE",
	"VALUES" => $arSortFields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER1"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_FORUM_TOPIC_PARAM_SORT_ORDER1"),
	"TYPE" => "LIST",
	"DEFAULT" => "ASC",
	"VALUES" => $arSorts,
);

$arComponentParameters["PARAMETERS"]["SORT_BY2"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_FORUM_TOPIC_PARAM_SORT_BY2"),
	"TYPE" => "LIST",
	"DEFAULT" => "LAST_POST_DATE",
	"VALUES" => $arSortFields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER2"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_FORUM_TOPIC_PARAM_SORT_ORDER2"),
	"TYPE" => "LIST",
	"DEFAULT" => "DESC",
	"VALUES" => $arSorts,
);

$arComponentParameters["PARAMETERS"]["TOPIC_URL"] = Array(
	"NAME" => GetMessage("GRAIN_LINKS_LIST_FORUM_TOPIC_PARAM_TOPIC_URL"),
	"TYPE" => "STRING",
	"PARENT" => "DATA_SOURCE",
);

?>