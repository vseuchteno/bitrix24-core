<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$rsGroup = CGroup::GetList(($by="c_sort"), ($order="desc"));
$arGroups = Array();
while ($arGroup = $rsGroup->GetNext())
    $arGroups[$arGroup["ID"]] = $arGroup["NAME"];

$arSorts = Array("ASC"=>GetMessage("GRAIN_LINKS_LIST_GROUP_SORT_ASC"), "DESC"=>GetMessage("GRAIN_LINKS_LIST_GROUP_SORT_DESC"));
$arSortFields = Array(
    "ID"=>GetMessage("GRAIN_LINKS_LIST_GROUP_SORT_FID"),
    "NAME"=>GetMessage("GRAIN_LINKS_LIST_GROUP_SORT_FNAME"),
	"C_SORT"=>GetMessage("GRAIN_LINKS_LIST_GROUP_SORT_FSORT"),
	"TIMESTAMP_X"=>GetMessage("GRAIN_LINKS_LIST_GROUP_SORT_FTSTMP"),
	"USERS"=>GetMessage("GRAIN_LINKS_LIST_GROUP_SORT_FUSERCNT"),
);

$arComponentParameters["PARAMETERS"]["ACTIVE"] = array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_GROUP_PARAM_ACTIVE"),
	"TYPE" => "CHECKBOX",
    "DEFAULT" => "N",
);

$arComponentParameters["PARAMETERS"]["SORT_BY"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_GROUP_PARAM_SORT_BY"),
	"TYPE" => "LIST",
	"DEFAULT" => "C_SORT",
	"VALUES" => $arSortFields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_GROUP_PARAM_SORT_ORDER"),
	"TYPE" => "LIST",
	"DEFAULT" => "ASC",
	"VALUES" => $arSorts,
);

$arComponentParameters["PARAMETERS"]["GROUP_URL"] = Array(
	"NAME" => GetMessage("GRAIN_LINKS_LIST_GROUP_PARAM_GROUP_URL"),
	"TYPE" => "STRING",
	"PARENT" => "DATA_SOURCE",
);

$arComponentParameters["PARAMETERS"]["HIDE_GROUPS"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_GROUP_PARAM_HIDE_GROUPS"),
	"TYPE" => "LIST",
	"VALUES" => $arGroups,
	"DEFAULT" => "",
	"MULTIPLE" => "Y",
	"ADDITIONAL_VALUES" => "Y",
);

?>