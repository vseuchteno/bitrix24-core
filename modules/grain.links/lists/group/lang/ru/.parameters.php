<?

$MESS["GRAIN_LINKS_LIST_GROUP_PARAM_ACTIVE"] = "Только активные";

$MESS["GRAIN_LINKS_LIST_GROUP_PARAM_SORT_BY"] = "Поле для сортировки";
$MESS["GRAIN_LINKS_LIST_GROUP_PARAM_SORT_ORDER"] = "Направление для сортировки";

$MESS["GRAIN_LINKS_LIST_GROUP_SORT_ASC"] = "По возрастанию";
$MESS["GRAIN_LINKS_LIST_GROUP_SORT_DESC"] = "По убыванию";
$MESS["GRAIN_LINKS_LIST_GROUP_SORT_FID"] = "ID";
$MESS["GRAIN_LINKS_LIST_GROUP_SORT_FNAME"] = "Название";
$MESS["GRAIN_LINKS_LIST_GROUP_SORT_FSORT"] = "Индекс сортировки";
$MESS["GRAIN_LINKS_LIST_GROUP_SORT_FTSTMP"] = "Дата изменения";
$MESS["GRAIN_LINKS_LIST_GROUP_SORT_FUSERCNT"] = "Количество пользователей";

$MESS["GRAIN_LINKS_LIST_GROUP_PARAM_GROUP_URL"] = "Шаблон пути к группе";
$MESS["GRAIN_LINKS_LIST_GROUP_PARAM_HIDE_GROUPS"] = "Не показывать группы";

?>