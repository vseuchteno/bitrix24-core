<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(\Bitrix\Main\Loader::includeModule("highloadblock"))
{
	$hlblocks = array();
	$result = \Bitrix\Highloadblock\HighloadBlockTable::getList();
	while($hlblock=$result->fetch())
		$hlblocks[$hlblock['ID']] = $hlblock['NAME']." [".$hlblock['ID']."]";
}

$displayFields = array(
	"ID"=>GetMessage("GRAIN_LINKS_LIST_HLBLOCK_FID"),
    "NAME"=>GetMessage("GRAIN_LINKS_LIST_HLBLOCK_FNAME"),
    "LANG_NAME"=>GetMessage("GRAIN_LINKS_LIST_HLBLOCK_FLANG_NAME"),
    "TABLE_NAME"=>GetMessage("GRAIN_LINKS_LIST_HLBLOCK_FTABLE_NAME"),
);

$valueFields = array(
	"ID"=>GetMessage("GRAIN_LINKS_LIST_HLBLOCK_FID"),
    "NAME"=>GetMessage("GRAIN_LINKS_LIST_HLBLOCK_FNAME"),
    "TABLE_NAME"=>GetMessage("GRAIN_LINKS_LIST_HLBLOCK_FTABLE_NAME"),
);

$sorts = Array("ASC"=>GetMessage("GRAIN_LINKS_LIST_HLBLOCK_SORT_ASC"), "DESC"=>GetMessage("GRAIN_LINKS_LIST_HLBLOCK_SORT_DESC"));
$sortFields = Array(
    "ID"=>GetMessage("GRAIN_LINKS_LIST_HLBLOCK_FID"),
    "NAME"=>GetMessage("GRAIN_LINKS_LIST_HLBLOCK_FNAME"),
    "TABLE_NAME"=>GetMessage("GRAIN_LINKS_LIST_HLBLOCK_FTABLE_NAME"),
);

$arComponentParameters["PARAMETERS"]["DISPLAY_FIELD"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_HLBLOCK_PARAM_DISPLAY_FIELD"),
	"TYPE" => "LIST",
	"DEFAULT" => "NAME",
	"VALUES" => $displayFields,
);

$arComponentParameters["PARAMETERS"]["VALUE_FIELD"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_HLBLOCK_PARAM_VALUE_FIELD"),
	"TYPE" => "LIST",
	"DEFAULT" => "ID",
	"VALUES" => $valueFields,
);

$arComponentParameters["PARAMETERS"]["BLOCK_ID"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_HLBLOCK_PARAM_BLOCK_ID"),
	"TYPE" => "LIST",
	"DEFAULT" => array(),
	"MULTIPLE" => "Y",
	"VALUES" => $hlblocks,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_BY1"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_HLBLOCK_PARAM_SORT_BY1"),
	"TYPE" => "LIST",
	"DEFAULT" => "NAME",
	"VALUES" => $sortFields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER1"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_HLBLOCK_PARAM_SORT_ORDER1"),
	"TYPE" => "LIST",
	"DEFAULT" => "ASC",
	"VALUES" => $sorts,
);

$arComponentParameters["PARAMETERS"]["SORT_BY2"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_HLBLOCK_PARAM_SORT_BY2"),
	"TYPE" => "LIST",
	"DEFAULT" => "ID",
	"VALUES" => $sortFields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER2"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_HLBLOCK_PARAM_SORT_ORDER2"),
	"TYPE" => "LIST",
	"DEFAULT" => "DESC",
	"VALUES" => $sorts,
);

$arComponentParameters["PARAMETERS"]["BLOCK_URL"] = Array(
	"NAME" => GetMessage("GRAIN_LINKS_LIST_HLBLOCK_PARAM_BLOCK_URL"),
	"TYPE" => "STRING",
	"PARENT" => "DATA_SOURCE",
);

?>