<?

$MESS["GRAIN_LINKS_LIST_HLBLOCK_PARAM_SORT_BY1"] = "Поле для первой сортировки";
$MESS["GRAIN_LINKS_LIST_HLBLOCK_PARAM_SORT_ORDER1"] = "Направление для первой сортировки";
$MESS["GRAIN_LINKS_LIST_HLBLOCK_PARAM_SORT_BY2"] = "Поле для второй сортировки";
$MESS["GRAIN_LINKS_LIST_HLBLOCK_PARAM_SORT_ORDER2"] = "Направление для второй сортировки";

$MESS["GRAIN_LINKS_LIST_HLBLOCK_SORT_ASC"] = "По возрастанию";
$MESS["GRAIN_LINKS_LIST_HLBLOCK_SORT_DESC"] = "По убыванию";
$MESS["GRAIN_LINKS_LIST_HLBLOCK_FID"] = "ID";
$MESS["GRAIN_LINKS_LIST_HLBLOCK_FNAME"] = "Название сущности";
$MESS["GRAIN_LINKS_LIST_HLBLOCK_FLANG_NAME"] = "Название";
$MESS["GRAIN_LINKS_LIST_HLBLOCK_FTABLE_NAME"] = "Название таблицы БД";

$MESS["GRAIN_LINKS_LIST_HLBLOCK_PARAM_DISPLAY_FIELD"] = "Поле для отображения";
$MESS["GRAIN_LINKS_LIST_HLBLOCK_PARAM_VALUE_FIELD"] = "Поле для значения";
$MESS["GRAIN_LINKS_LIST_HLBLOCK_PARAM_BLOCK_ID"] = "Только избранные хайлоадблоки";
$MESS["GRAIN_LINKS_LIST_HLBLOCK_PARAM_BLOCK_URL"] = "Шаблон пути к хайлоадблоку";

?>