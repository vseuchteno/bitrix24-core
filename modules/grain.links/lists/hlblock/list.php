<?
if(\Bitrix\Main\Loader::includeModule("highloadblock"))
{
	/* prepare parameters */

	$arParams["SORT_BY1"] = trim($arParams["SORT_BY1"]);
	if(strlen($arParams["SORT_BY1"])<=0)
		$arParams["SORT_BY1"] = "NAME";
	$arParams["SORT_ORDER1"] = strtoupper($arParams["SORT_ORDER1"]);
	if($arParams["SORT_ORDER1"]!="DESC")
		 $arParams["SORT_ORDER1"]="ASC";
	if(strlen($arParams["SORT_BY2"])<=0)
		$arParams["SORT_BY2"] = "ID";
	$arParams["SORT_ORDER2"] = strtoupper($arParams["SORT_ORDER2"]);
	if($arParams["SORT_ORDER2"]!="ASC")
		$arParams["SORT_ORDER2"]="DESC";

	if(!$arParams["DISPLAY_FIELD"])
		$arParams["DISPLAY_FIELD"] = "NAME";
	if(!$arParams["VALUE_FIELD"])
		$arParams["VALUE_FIELD"] = "ID";
	
	$tmpBlockId = $arParams["BLOCK_ID"];
	$arParams["BLOCK_ID"] = array();
	foreach($tmpBlockId as $blockId)
		if(intval($blockId))
			$arParams["BLOCK_ID"][] = intval($blockId);
	unset($tmpBlockId);
	
	/* build list */
	
	$order = array(
		$arParams["SORT_BY1"]=>$arParams["SORT_ORDER1"],
		$arParams["SORT_BY2"]=>$arParams["SORT_ORDER2"],
	);
	if(!array_key_exists("ID", $order))
		$order["ID"] = "DESC";
	
	$filter = array();
	
	if($arParams["BLOCK_ID"])
		$filter['ID'] = $arParams["BLOCK_ID"];
	
	if($arResult["AJAX_RETURN"]) $filter['%'.$arParams["DISPLAY_FIELD"]] = $arResult["AJAX_SEARCH_QUERY"];
	
	if($arResult["SELECTED_VALUE"]) $filter['='.$arParams["VALUE_FIELD"]] = $arResult["SELECTED_VALUE"];

	$result = \Bitrix\Highloadblock\HighloadBlockTable::getList(array(
		'order' => $order,
		'filter' => $filter,
		'select' => array('*','LANG_NAME'=>'LANG.NAME')
	));
	
	while($hlblock=$result->fetch())
	{
		$item = Array(
			"NAME" => $hlblock[$arParams["DISPLAY_FIELD"]]?$hlblock[$arParams["DISPLAY_FIELD"]]:$hlblock["NAME"],
		);
		
		if($arParams["SHOW_URL"]) 
		{
			$item["URL"] = str_replace(
				array("#SERVER_NAME#", "#SITE_DIR#", "#BLOCK_ID#", "#NAME#", "#LANG_NAME#"),
				array($arParams["ADMIN_SECTION"]?"":SITE_SERVER_NAME, $arParams["ADMIN_SECTION"]?"":SITE_DIR, $hlblock["ID"], $hlblock["NAME"], $hlblock["LANG_NAME"]),
				$arParams["~BLOCK_URL"]
			);
			$item["URL"] = preg_replace("'/+'s", "/", $item["URL"]);
			$item["URL"] = htmlspecialcharsbx($item["URL"]);
		}
		
		$arResult["DATA"][$hlblock[$arParams["VALUE_FIELD"]]] = $item;
	}
}
?>