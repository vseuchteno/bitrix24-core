<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(\Bitrix\Main\Loader::includeModule("highloadblock"))
{
	$hlblocks = array(""=>"");
	$result = \Bitrix\Highloadblock\HighloadBlockTable::getList();
	while($hlblock=$result->fetch())
		$hlblocks[$hlblock['ID']] = $hlblock['NAME']." [".$hlblock['ID']."]";
	
	$fields = array("ID"=>GetMessage("GRAIN_LINKS_LIST_HLBLOCK_FID"));
	if($arCurrentValues["BLOCK_ID"])
	{
		$rsUserFields = \Bitrix\Main\UserFieldTable::getList(array(
			'order' => array('SORT'=>'ASC'),
			'filter' => array('ENTITY_ID'=>'HLBLOCK_'.$arCurrentValues["BLOCK_ID"]),
		));
		while($userField=$rsUserFields->fetch())
		{
			$fields[$userField['FIELD_NAME']] = $userField['FIELD_NAME'];		
		}		
	}
}

$sorts = Array("ASC"=>GetMessage("GRAIN_LINKS_LIST_HLBLOCK_SORT_ASC"), "DESC"=>GetMessage("GRAIN_LINKS_LIST_HLBLOCK_SORT_DESC"));

$arComponentParameters["PARAMETERS"]["BLOCK_ID"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_HLBLOCK_ELEMENT_PARAM_BLOCK_ID"),
	"TYPE" => "LIST",
	"DEFAULT" => "",
	"VALUES" => $hlblocks,
	"ADDITIONAL_VALUES" => "Y",
	"REFRESH" => "Y",
);

$arComponentParameters["PARAMETERS"]["DISPLAY_FIELD"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_HLBLOCK_ELEMENT_PARAM_DISPLAY_FIELD"),
	"TYPE" => "LIST",
	"DEFAULT" => "NAME",
	"VALUES" => $fields,
);

$arComponentParameters["PARAMETERS"]["VALUE_FIELD"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_HLBLOCK_ELEMENT_PARAM_VALUE_FIELD"),
	"TYPE" => "LIST",
	"DEFAULT" => "ID",
	"VALUES" => $fields,
);

$arComponentParameters["PARAMETERS"]["SORT_BY1"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_HLBLOCK_ELEMENT_PARAM_SORT_BY1"),
	"TYPE" => "LIST",
	"DEFAULT" => "NAME",
	"VALUES" => $fields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER1"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_HLBLOCK_ELEMENT_PARAM_SORT_ORDER1"),
	"TYPE" => "LIST",
	"DEFAULT" => "ASC",
	"VALUES" => $sorts,
);

$arComponentParameters["PARAMETERS"]["SORT_BY2"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_HLBLOCK_ELEMENT_PARAM_SORT_BY2"),
	"TYPE" => "LIST",
	"DEFAULT" => "ID",
	"VALUES" => $fields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER2"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_HLBLOCK_ELEMENT_PARAM_SORT_ORDER2"),
	"TYPE" => "LIST",
	"DEFAULT" => "DESC",
	"VALUES" => $sorts,
);

$arComponentParameters["PARAMETERS"]["BLOCK_URL"] = Array(
	"NAME" => GetMessage("GRAIN_LINKS_LIST_HLBLOCK_ELEMENT_PARAM_BLOCK_URL"),
	"TYPE" => "STRING",
	"PARENT" => "DATA_SOURCE",
);

?>