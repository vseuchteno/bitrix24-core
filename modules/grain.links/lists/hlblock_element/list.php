<?
if(\Bitrix\Main\Loader::includeModule("highloadblock"))
{
	/* prepare parameters */

	$arParams["SORT_BY1"] = trim($arParams["SORT_BY1"]);
	if(strlen($arParams["SORT_BY1"])<=0)
		$arParams["SORT_BY1"] = "ID";
	$arParams["SORT_ORDER1"] = strtoupper($arParams["SORT_ORDER1"]);
	if($arParams["SORT_ORDER1"]!="ASC")
		 $arParams["SORT_ORDER1"]="DESC";
	if(strlen($arParams["SORT_BY2"])<=0)
		$arParams["SORT_BY2"] = "ID";
	$arParams["SORT_ORDER2"] = strtoupper($arParams["SORT_ORDER2"]);
	if($arParams["SORT_ORDER2"]!="ASC")
		$arParams["SORT_ORDER2"]="DESC";

	if(!$arParams["DISPLAY_FIELD"])
		$arParams["DISPLAY_FIELD"] = "ID";
	if(!$arParams["VALUE_FIELD"])
		$arParams["VALUE_FIELD"] = "ID";
	
	$arParams["BLOCK_ID"] = intval($arParams["BLOCK_ID"]);
	
	/* build list */	

	$resultBlock = \Bitrix\Highloadblock\HighloadBlockTable::getList(array(
		'filter' => array('=ID'=>$arParams["BLOCK_ID"]),
		'select' => array('*','LANG_NAME'=>'LANG.NAME')
	));
		
	if($hlblock=$resultBlock->fetch())
	{
		$entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
		$entityDataClass = $entity->getDataClass();

		$order = array(
			$arParams["SORT_BY1"]=>$arParams["SORT_ORDER1"],
			$arParams["SORT_BY2"]=>$arParams["SORT_ORDER2"],
		);
		if(!array_key_exists("ID", $order))
			$order["ID"] = "DESC";

		$filter = array();
	
		if($arResult["AJAX_RETURN"]) $filter['%'.$arParams["DISPLAY_FIELD"]] = $arResult["AJAX_SEARCH_QUERY"];
		
		if($arResult["SELECTED_VALUE"]) $filter['='.$arParams["VALUE_FIELD"]] = $arResult["SELECTED_VALUE"];
	
		$result = $entityDataClass::getList(array(
			'order' => $order,
			'filter' => $filter,
		));
	
		while($row=$result->fetch())
		{
			$item = Array(
				"NAME" => $row[$arParams["DISPLAY_FIELD"]]?$row[$arParams["DISPLAY_FIELD"]]:$row["ID"],
			);
			
			if($arParams["SHOW_URL"]) 
			{
				$item["URL"] = str_replace(
					array("#SERVER_NAME#", "#SITE_DIR#", "#BLOCK_ID#", "#NAME#", "#LANG_NAME#"),
					array($arParams["ADMIN_SECTION"]?"":SITE_SERVER_NAME, $arParams["ADMIN_SECTION"]?"":SITE_DIR, $hlblock["ID"], $hlblock["NAME"], $hlblock["LANG_NAME"]),
					$arParams["~BLOCK_URL"]
				);

				foreach($row as $fieldName=>$fieldValue)
				{
					$item["URL"] = str_replace(
						'#'.$fieldName.'#',
						$fieldValue,
						$item["URL"]
					);
				}
				$item["URL"] = preg_replace("'/+'s", "/", $item["URL"]);
				$item["URL"] = htmlspecialcharsbx($item["URL"]);
			}
			
			$arResult["DATA"][$row[$arParams["VALUE_FIELD"]]] = $item;
		}
		
	}
}
?>