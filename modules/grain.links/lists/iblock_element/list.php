<?
if(CModule::IncludeModule("iblock")) {

	/* prepare parameters */
	
	$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
	$arParams["PARENT_SECTION"] = intval($arParams["PARENT_SECTION"]);
	$arParams["INCLUDE_SUBSECTIONS"] = $arParams["INCLUDE_SUBSECTIONS"]=="Y";
	$arParams["CHECK_PERMISSIONS"] = $arParams["CHECK_PERMISSIONS"]=="Y";
	$arParams["ACTIVE"] = $arParams["ACTIVE"]=="Y";
	$arParams["CHECK_DATES"] = $arParams["CHECK_DATES"]=="Y";
	
	$arParams["SORT_BY1"] = trim($arParams["SORT_BY1"]);
	if(strlen($arParams["SORT_BY1"])<=0)
		$arParams["SORT_BY1"] = "ACTIVE_FROM";
	$arParams["SORT_ORDER1"] = strtoupper($arParams["SORT_ORDER1"]);
	if($arParams["SORT_ORDER1"]!="ASC")
		 $arParams["SORT_ORDER1"]="DESC";
	if(strlen($arParams["SORT_BY2"])<=0)
		$arParams["SORT_BY2"] = "SORT";
	$arParams["SORT_ORDER2"] = strtoupper($arParams["SORT_ORDER2"]);
	if($arParams["SORT_ORDER2"]!="DESC")
		 $arParams["SORT_ORDER2"]="ASC";
		
	/* build list */
	
	$arSort = array(
		$arParams["SORT_BY1"]=>$arParams["SORT_ORDER1"],
		$arParams["SORT_BY2"]=>$arParams["SORT_ORDER2"],
	);
	if(!array_key_exists("ID", $arSort))
		$arSort["ID"] = "DESC";
	
	$arFilter = array (
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
	);

	if(!$arParams["ADMIN_SECTION"])
		$arFilter["IBLOCK_LID"]=SITE_ID;

	if($arParams["ACTIVE"])
		$arFilter["ACTIVE"] = "Y";
	if($arParams["CHECK_DATES"])
		$arFilter["ACTIVE_DATE"] = "Y";

	if($arParams["PARENT_SECTION"])
		$arFilter["SECTION_ID"] = $arParams["PARENT_SECTION"];
	if($arParams["INCLUDE_SUBSECTIONS"])
		$arFilter["INCLUDE_SUBSECTIONS"] = "Y";

	if($arParams["CHECK_PERMISSIONS"])
		$arFilter["CHECK_PERMISSIONS"] = "Y";

	if($arResult["AJAX_RETURN"]) $arFilter["%NAME"] = $arResult["AJAX_SEARCH_QUERY"];
	
	if($arResult["SELECTED_VALUE"]) $arFilter["ID"] = $arResult["SELECTED_VALUE"];
	
	$arSelect = Array(
		"ID",
		"IBLOCK_ID",
		"NAME",
	);
	
	if($arParams["SHOW_URL"]) $arSelect[] = "DETAIL_PAGE_URL";
	
	$rsElements = CIBlockElement::GetList($arSort,$arFilter,false,false,$arSelect);
	if($arParams["SHOW_URL"]) $rsElements->SetUrlTemplates($arParams["DETAIL_URL"]);
	
	while($arElement=$rsElements->GetNext()) {
	
		$arItem = Array(
			"NAME" => $arElement["NAME"],
		);
		
		if($arParams["SHOW_URL"]) $arItem["URL"] = $arElement["DETAIL_PAGE_URL"];
		
		$arResult["DATA"][$arElement["ID"]] = $arItem;
		
	}
	
}
?>