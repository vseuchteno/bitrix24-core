<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


if(CModule::IncludeModule("iblock")) {

	$arTypesEx = CIBlockParameters::GetIBlockTypes();
	
	$arSorts = Array("ASC"=>GetMessage("GRAIN_LINKS_LIST_IBLOCK_IBLOCK_SORT_ASC"), "DESC"=>GetMessage("GRAIN_LINKS_LIST_IBLOCK_IBLOCK_SORT_DESC"));
	$arSortFields = Array(
		"ID"=>GetMessage("GRAIN_LINKS_LIST_IBLOCK_IBLOCK_SORT_FID"),
		"NAME"=>GetMessage("GRAIN_LINKS_LIST_IBLOCK_IBLOCK_SORT_FNAME"),
		"ELEMENT_CNT"=>GetMessage("GRAIN_LINKS_LIST_IBLOCK_IBLOCK_FELEMENT_CNT"),
		"SORT"=>GetMessage("GRAIN_LINKS_LIST_IBLOCK_IBLOCK_SORT_FSORT"),
		"TIMESTAMP_X"=>GetMessage("GRAIN_LINKS_LIST_IBLOCK_IBLOCK_SORT_FTSAMP")
	);

}

$arComponentParameters["PARAMETERS"]["IBLOCK_TYPE"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_IBLOCK_IBLOCK_PARAM_IBLOCK_TYPE"),
	"TYPE" => "LIST",
	"VALUES" => $arTypesEx,
	"DEFAULT" => "",
	"REFRESH" => "Y",
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["ACTIVE"] = array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_IBLOCK_IBLOCK_PARAM_ACTIVE"),
	"TYPE" => "CHECKBOX",
    "DEFAULT" => "N",
);

$arComponentParameters["PARAMETERS"]["CHECK_PERMISSIONS"] = array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_IBLOCK_IBLOCK_PARAM_CHECK_PERMISSIONS"),
	"TYPE" => "CHECKBOX",
    "DEFAULT" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_BY1"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_IBLOCK_IBLOCK_PARAM_SORT_BY1"),
	"TYPE" => "LIST",
	"DEFAULT" => "TIMESTAMP_X",
	"VALUES" => $arSortFields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER1"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_IBLOCK_IBLOCK_PARAM_SORT_ORDER1"),
	"TYPE" => "LIST",
	"DEFAULT" => "DESC",
	"VALUES" => $arSorts,
);

$arComponentParameters["PARAMETERS"]["SORT_BY2"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_IBLOCK_IBLOCK_PARAM_SORT_BY2"),
	"TYPE" => "LIST",
	"DEFAULT" => "SORT",
	"VALUES" => $arSortFields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER2"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_IBLOCK_IBLOCK_PARAM_SORT_ORDER2"),
	"TYPE" => "LIST",
	"DEFAULT" => "ASC",
	"VALUES" => $arSorts,
);

$arComponentParameters["PARAMETERS"]["IBLOCK_URL"] = CIBlockParameters::GetPathTemplateParam(
	"LIST",
	"IBLOCK_URL",
	GetMessage("GRAIN_LINKS_LIST_IBLOCK_IBLOCK_PARAM_IBLOCK_URL"),
	"",
	"DATA_SOURCE"
);

?>