<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


if(CModule::IncludeModule("iblock")) {

	$arTypesEx = CIBlockParameters::GetIBlockTypes(Array("-"=>" "));
	
	$arIBlocks=Array();
	$arFilter = Array("TYPE" => ($arCurrentValues["IBLOCK_TYPE"]!="-"?$arCurrentValues["IBLOCK_TYPE"]:""));
	//if(defined("SITE_ID")) 
		//$arFilter["SITE_ID"] = SITE_ID;
	$rsIBlock = CIBlock::GetList(Array("SORT"=>"ASC"),$arFilter);
	while($arRes = $rsIBlock->Fetch())
		$arIBlocks[$arRes["ID"]] = $arRes["NAME"];

}

$valueFields = array(
	"ID"=>GetMessage("GRAIN_LINKS_LIST_IBLOCK_PROPERTY_PARAM_VALUE_FIELD_ID"),
	"XML_ID"=>GetMessage("GRAIN_LINKS_LIST_IBLOCK_PROPERTY_PARAM_VALUE_FIELD_XML_ID"),	
    "CODE"=>GetMessage("GRAIN_LINKS_LIST_IBLOCK_PROPERTY_PARAM_VALUE_FIELD_CODE"),
);

$arComponentParameters["PARAMETERS"]["IBLOCK_TYPE"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_IBLOCK_PROPERTY_PARAM_IBLOCK_TYPE"),
	"TYPE" => "LIST",
	"VALUES" => $arTypesEx,
	"DEFAULT" => "news",
	"REFRESH" => "Y",
);

$arComponentParameters["PARAMETERS"]["IBLOCK_ID"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_IBLOCK_PROPERTY_PARAM_IBLOCK_ID"),
	"TYPE" => "LIST",
	"MULTIPLE" => "Y",
	"VALUES" => $arIBlocks,
	"DEFAULT" => '',
	"ADDITIONAL_VALUES" => "Y",
	"REFRESH" => "Y",
);

$arComponentParameters["PARAMETERS"]["VALUE_FIELD"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_IBLOCK_PROPERTY_PARAM_VALUE_FIELD"),
	"TYPE" => "LIST",
	"DEFAULT" => "ID",
	"VALUES" => $valueFields,
);

$arComponentParameters["PARAMETERS"]["ACTIVE"] = array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_IBLOCK_PROPERTY_PARAM_ACTIVE"),
	"TYPE" => "CHECKBOX",
    "DEFAULT" => "N",
);

?>