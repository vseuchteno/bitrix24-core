<?

$MESS["GRAIN_LINKS_LIST_IBLOCK_PROPERTY_PARAM_IBLOCK_TYPE"] = "Type of information block";
$MESS["GRAIN_LINKS_LIST_IBLOCK_PROPERTY_PARAM_IBLOCK_ID"] = "Information block code";
$MESS["GRAIN_LINKS_LIST_IBLOCK_PROPERTY_PARAM_VALUE_FIELD"] = "Use as value";
$MESS["GRAIN_LINKS_LIST_IBLOCK_PROPERTY_PARAM_VALUE_FIELD_ID"] = "ID";
$MESS["GRAIN_LINKS_LIST_IBLOCK_PROPERTY_PARAM_VALUE_FIELD_XML_ID"] = "External code";
$MESS["GRAIN_LINKS_LIST_IBLOCK_PROPERTY_PARAM_VALUE_FIELD_CODE"] = "Symbolic code";
$MESS["GRAIN_LINKS_LIST_IBLOCK_PROPERTY_PARAM_ACTIVE"] = "Check properties activity";

?>