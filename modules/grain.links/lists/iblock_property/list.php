<?
if(\Bitrix\Main\Loader::IncludeModule("iblock")) 
{
	/* prepare parameters */
	
	$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
	$arParams["ACTIVE"] = $arParams["ACTIVE"]=="Y";
	$arParams["VALUE_FIELD"] = in_array($arParams["VALUE_FIELD"], array('ID','XML_ID','CODE'))?$arParams["VALUE_FIELD"]:'ID';
			
	/* build list */
		
	$arFilter = array (
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
	);

	if($arResult["SELECTED_VALUE"]) $arFilter[$arParams["VALUE_FIELD"]] = $arResult["SELECTED_VALUE"];
			
	$rsProperty = $rsProperty = \Bitrix\Iblock\PropertyTable::getList(array(
		'filter' => $arFilter,
		'select' => array('ID','NAME','XML_ID','CODE'),
	));
	
	while($arProperty=$rsProperty->fetch()) 
	{
		$arItem = Array(
			"NAME" => $arProperty["NAME"],
		);
		$arResult["DATA"][$arProperty[$arParams["VALUE_FIELD"]]] = $arItem;	
	}
	
}
?>