<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arSorts = Array("ASC"=>GetMessage("GRAIN_LINKS_LIST_IBLOCK_TYPE_SORT_ASC"), "DESC"=>GetMessage("GRAIN_LINKS_LIST_IBLOCK_TYPE_SORT_DESC"));
$arSortFields = Array(
    "ID"=>GetMessage("GRAIN_LINKS_LIST_IBLOCK_TYPE_SORT_FID"),
    "NAME"=>GetMessage("GRAIN_LINKS_LIST_IBLOCK_TYPE_SORT_FNAME"),
    "SORT"=>GetMessage("GRAIN_LINKS_LIST_IBLOCK_TYPE_SORT_FSORT"),
);

$arComponentParameters["PARAMETERS"]["SORT_BY1"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_IBLOCK_TYPE_PARAM_SORT_BY1"),
	"TYPE" => "LIST",
	"DEFAULT" => "SORT",
	"VALUES" => $arSortFields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER1"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_IBLOCK_TYPE_PARAM_SORT_ORDER1"),
	"TYPE" => "LIST",
	"DEFAULT" => "ASC",
	"VALUES" => $arSorts,
);

$arComponentParameters["PARAMETERS"]["SORT_BY2"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_IBLOCK_TYPE_PARAM_SORT_BY2"),
	"TYPE" => "LIST",
	"DEFAULT" => "NAME",
	"VALUES" => $arSortFields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER2"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_IBLOCK_TYPE_PARAM_SORT_ORDER2"),
	"TYPE" => "LIST",
	"DEFAULT" => "ASC",
	"VALUES" => $arSorts,
);

$arComponentParameters["PARAMETERS"]["IBLOCK_TYPE_URL"] = Array(
	"NAME" => GetMessage("GRAIN_LINKS_LIST_IBLOCK_TYPE_PARAM_IBLOCK_TYPE_URL"),
	"TYPE" => "STRING",
	"PARENT" => "DATA_SOURCE",
);

?>