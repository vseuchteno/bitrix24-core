<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arSorts = Array("ASC"=>GetMessage("GRAIN_LINKS_LIST_LANGUAGE_SORT_ASC"), "DESC"=>GetMessage("GRAIN_LINKS_LIST_LANGUAGE_SORT_DESC"));
$arSortFields = Array(
    "LID"=>GetMessage("GRAIN_LINKS_LIST_LANGUAGE_SORT_FID"),
    "NAME"=>GetMessage("GRAIN_LINKS_LIST_LANGUAGE_SORT_FNAME"),
    "ACTIVE"=>GetMessage("GRAIN_LINKS_LIST_LANGUAGE_SORT_FACTIVE"),
    "DEF"=>GetMessage("GRAIN_LINKS_LIST_LANGUAGE_SORT_FDEF"),
);

$arComponentParameters["PARAMETERS"]["SORT_BY"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_LANGUAGE_PARAM_SORT_BY"),
	"TYPE" => "LIST",
	"DEFAULT" => "NAME",
	"VALUES" => $arSortFields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_LANGUAGE_PARAM_SORT_ORDER"),
	"TYPE" => "LIST",
	"DEFAULT" => "ASC",
	"VALUES" => $arSorts,
);

$arComponentParameters["PARAMETERS"]["ACTIVE"] = array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_LANGUAGE_PARAM_ACTIVE"),
	"TYPE" => "CHECKBOX",
    "DEFAULT" => "N",
);

?>