<?

/* prepare parameters */

$arParams["ACTIVE"] = $arParams["ACTIVE"]=="Y";

$arParams["SORT_BY"] = trim($arParams["SORT_BY"]);
if(strlen($arParams["SORT_BY"])<=0)
    $arParams["SORT_BY"] = "NAME";
$arParams["SORT_ORDER"] = strtoupper($arParams["SORT_ORDER"]);
if($arParams["SORT_ORDER"]!="DESC")
     $arParams["SORT_ORDER"]="ASC";

if($arParams["SHOW_URL"])
	$arParams["SHOW_URL"] = false;
    
/* build list */

$arFilter = Array();

if($arParams["ACTIVE"])
    $arFilter["ACTIVE"]="Y";

if($arResult["AJAX_RETURN"]) $arFilter["NAME"] = "%".$arResult["AJAX_SEARCH_QUERY"]."%";

//if($arResult["SELECTED_VALUE"]) $arFilter["LID"] = $arResult["SELECTED_VALUE"];

$rsLanguages = CLanguage::GetList($by=$arParams["SORT_BY"],$order=$arParams["SORT_ORDER"],$arFilter);

while($arLanguage=$rsLanguages->GetNext()) {

    $arItem = Array(
    	"NAME" => $arLanguage["NAME"],
    );
        
    $arResult["DATA"][$arLanguage["LID"]] = $arItem;
    
}

if($arResult["SELECTED_VALUE"]){
	foreach($arResult["DATA"] as $key => $arItem) 
		if(
			(is_array($arResult["SELECTED_VALUE"]) && !in_array($key,$arResult["SELECTED_VALUE"]))
			|| (!is_array($arResult["SELECTED_VALUE"]) && $arResult["SELECTED_VALUE"]!=$key)
		)
			unset($arResult["DATA"][$key]);
}

?>