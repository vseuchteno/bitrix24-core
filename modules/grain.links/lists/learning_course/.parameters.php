<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(CModule::IncludeModule("learning")) {

	$rsCourses = CCourse::GetList();
	$arCourses = Array();
	while($arCourse = $rsCourses->GetNext())
		$arCourses[$arCourse["ID"]] = $arCourse["NAME"];

	$arSorts = Array("ASC"=>GetMessage("GRAIN_LINKS_LIST_LEARNING_COURSE_SORT_ASC"), "DESC"=>GetMessage("GRAIN_LINKS_LIST_LEARNING_COURSE_SORT_DESC"));
	$arSortFields = Array(
	    "ID"=>GetMessage("GRAIN_LINKS_LIST_LEARNING_COURSE_SORT_FID"),
	    "NAME"=>GetMessage("GRAIN_LINKS_LIST_LEARNING_COURSE_SORT_FNAME"),
		"SORT"=>GetMessage("GRAIN_LINKS_LIST_LEARNING_COURSE_SORT_FSORT"),
		"TIMESTAMP_X"=>GetMessage("GRAIN_LINKS_LIST_LEARNING_COURSE_SORT_FDATEMOD"),
	);

}

$arComponentParameters["PARAMETERS"]["SORT_BY1"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_LEARNING_COURSE_PARAM_SORT_BY1"),
	"TYPE" => "LIST",
	"DEFAULT" => "NAME",
	"VALUES" => $arSortFields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER1"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_LEARNING_COURSE_PARAM_SORT_ORDER1"),
	"TYPE" => "LIST",
	"DEFAULT" => "ASC",
	"VALUES" => $arSorts,
);

$arComponentParameters["PARAMETERS"]["SORT_BY2"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_LEARNING_COURSE_PARAM_SORT_BY2"),
	"TYPE" => "LIST",
	"DEFAULT" => "ID",
	"VALUES" => $arSortFields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER2"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_LEARNING_COURSE_PARAM_SORT_ORDER2"),
	"TYPE" => "LIST",
	"DEFAULT" => "DESC",
	"VALUES" => $arSorts,
);

$arComponentParameters["PARAMETERS"]["ACTIVE"] = array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_LEARNING_COURSE_PARAM_ACTIVE"),
	"TYPE" => "CHECKBOX",
    "DEFAULT" => "N",
);

$arComponentParameters["PARAMETERS"]["CHECK_PERMISSIONS"] = array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_LEARNING_COURSE_PARAM_CHECK_PERMISSIONS"),
	"TYPE" => "CHECKBOX",
    "DEFAULT" => "Y",
);

$arComponentParameters["PARAMETERS"]["COURSE_URL"] = Array(
	"NAME" => GetMessage("GRAIN_LINKS_LIST_LEARNING_COURSE_PARAM_COURSE_URL"),
	"TYPE" => "STRING",
	"PARENT" => "DATA_SOURCE",
);

$arComponentParameters["PARAMETERS"]["HIDE_COURSES"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_LEARNING_COURSE_PARAM_HIDE_COURSES"),
	"TYPE" => "LIST",
	"VALUES" => $arCourses,
	"DEFAULT" => "",
	"MULTIPLE" => "Y",
	"ADDITIONAL_VALUES" => "Y",
);

?>