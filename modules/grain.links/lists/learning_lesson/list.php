<?

if(CModule::IncludeModule("learning")) {

	/* prepare parameters */
	
	$arLessons = $arParams["COURSE_ID"];
	$arParams["COURSE_ID"] = Array();
	foreach($arLessons as $course_id)
	    if(intval($course_id)>0)
	    	$arParams["COURSE_ID"][] = intval($course_id);

	$arParams["ACTIVE"] = $arParams["ACTIVE"]=="Y";
	$arParams["CHECK_PERMISSIONS"] = $arParams["CHECK_PERMISSIONS"]=="Y";
	
	$arParams["SORT_BY1"] = trim($arParams["SORT_BY1"]);
	if(strlen($arParams["SORT_BY1"])<=0)
		$arParams["SORT_BY1"] = "SORT";
	$arParams["SORT_ORDER1"] = strtoupper($arParams["SORT_ORDER1"]);
	if($arParams["SORT_ORDER1"]!="DESC")
		 $arParams["SORT_ORDER1"]="ASC";
	if(strlen($arParams["SORT_BY2"])<=0)
		$arParams["SORT_BY2"] = "LESSON_ID";
	$arParams["SORT_ORDER2"] = strtoupper($arParams["SORT_ORDER2"]);
	if($arParams["SORT_ORDER2"]!="ASC")
		 $arParams["SORT_ORDER2"]="DESC";
	    
	/* build list */

	$arSort = array(
		$arParams["SORT_BY1"]=>$arParams["SORT_ORDER1"],
		$arParams["SORT_BY2"]=>$arParams["SORT_ORDER2"],
	);
	if(!array_key_exists("LESSON_ID", $arSort))
		$arSort["LESSON_ID"] = "DESC";
	
	$arFilter = Array();

	if($arParams["ACTIVE"])
		$arFilter["ACTIVE"] = "Y";
	$arFilter["CHECK_PERMISSIONS"] = $arParams["CHECK_PERMISSIONS"]?"Y":"N";

	$arMap = CLearnLesson::GetCourseToLessonMap();

	foreach($arMap as $map=>$id) {

		if(substr($map,0,1)!="C")
			continue;
		
		$course_id = intval(substr($map,1));
		
		if(count($arParams["COURSE_ID"])>0 && !in_array($course_id,$arParams["COURSE_ID"]))
			continue;
		
		$oTree = CLearnLesson::GetTree($id,$arSort,$arFilter,true);
		$arTree = $oTree->GetTreeAsListOldMode();
        
		foreach($arTree as $arLesson) {

			if($arResult["SELECTED_VALUE"]) {
				if(
					(is_array($arResult["SELECTED_VALUE"]) && !in_array($arLesson["ID"],$arResult["SELECTED_VALUE"]))
					|| (!is_array($arResult["SELECTED_VALUE"]) && $arResult["SELECTED_VALUE"]!=$arLesson["ID"])
				)
					continue; 
			}
		
		    $arItem = Array(
		    	"NAME" => str_repeat(".",$arLesson["DEPTH_LEVEL"]-1).$arLesson["NAME"],
		    );
		    
		    if($arParams["SHOW_URL"] && $arParams["LESSON_URL"]) {
		    
		    	$arLesson["SITE_SERVER_NAME"] = defined("SITE_SERVER_NAME")?SITE_SERVER_NAME:"";
		    	$arLesson["SITE_ID"] = defined("SITE_ID")?SITE_ID:"";
		    	$arLesson["SITE_DIR"] = defined("SITE_DIR")?SITE_DIR:"";
		    	
		    	$arItem["URL"] = $arParams["LESSON_URL"];
		    	foreach($arLesson as $FIELD_NAME=>$FIELD_VALUE)
		    		if(substr($FIELD_NAME,0,1)!="~")
		    			$arItem["URL"] = str_replace("#".$FIELD_NAME."#",$FIELD_VALUE,$arItem["URL"]);
		
		    	$arItem["URL"] = preg_replace("'/+'s", "/", $arItem["URL"]);
		    	$arItem["URL"] = htmlspecialcharsbx($arItem["URL"]);
		    	
		    }
		    
		    $arResult["DATA"][$arLesson["ID"]] = $arItem;
		    
		}

	}

	if($arResult["AJAX_RETURN"])
		foreach($arResult["DATA"] as $key => $arItem) 
			if(!preg_match("/".preg_quote($arResult["AJAX_SEARCH_QUERY"])."/i".(BX_UTF?"u":""),$arItem["NAME"]))
				unset($arResult["DATA"][$key]);

}
?>