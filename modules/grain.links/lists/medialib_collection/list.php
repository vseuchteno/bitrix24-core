<?
if(CModule::IncludeModule("fileman")) {

	/* prepare parameters */

	$arParams["PARENT_COLLECTION"] = intval($arParams["PARENT_COLLECTION"]);
	$arParams["INCLUDE_SUBLEVELS"] = $arParams["INCLUDE_SUBLEVELS"]=="Y";
	$arParams["ACTIVE"] = $arParams["ACTIVE"]=="Y";
	
	$arParams["SORT_BY1"] = trim($arParams["SORT_BY1"]);
	if(strlen($arParams["SORT_BY1"])<=0)
		$arParams["SORT_BY1"] = "NAME";
	$arParams["SORT_ORDER1"] = strtoupper($arParams["SORT_ORDER1"]);
	if($arParams["SORT_ORDER1"]!="DESC")
		 $arParams["SORT_ORDER1"]="ASC";
	if(strlen($arParams["SORT_BY2"])<=0)
		$arParams["SORT_BY2"] = "DATE_UPDATE";
	$arParams["SORT_ORDER2"] = strtoupper($arParams["SORT_ORDER2"]);
	if($arParams["SORT_ORDER2"]!="ASC")
		 $arParams["SORT_ORDER2"]="DESC";
		
	/* build list */
	
	$arSort = Array();
	$arSort[$arParams["SORT_BY1"]] = $arParams["SORT_ORDER1"];
	$arSort[$arParams["SORT_BY2"]] = $arParams["SORT_ORDER2"];
	if(!array_key_exists("ID", $arSort))
		$arSort["ID"] = "DESC";

	$arFilter = Array();
	
	if($arParams["ACTIVE"])
		$arFilter["ACTIVE"] = "Y";
	if($arParams["TYPE"])
		$arFilter["ML_TYPE"] = $arParams["TYPE"];

	CMedialib::Init();

	if(!$arParams["INCLUDE_SUBLEVELS"]) {
	
		$arFilter["PARENT_ID"] = $arParams["PARENT_COLLECTION"]?$arParams["PARENT_COLLECTION"]:0;
	
		if($arResult["AJAX_RETURN"]) 
			$arFilter["NAME"] = "%".$arResult["AJAX_SEARCH_QUERY"]."%";
	
		if($arResult["SELECTED_VALUE"]) 
			$arFilter["ID"] = $arResult["SELECTED_VALUE"];
			
		$rsCollections = CMedialibCollection::GetList(array('arOrder'=>$arSort,'arFilter' => $arFilter));
	
	} else {
	
		if(!class_exists("CGrain_MedialibCollectionListTools")) {
			class CGrain_MedialibCollectionListTools {
				function GetRecursive($collection_id=0,&$arCollections,$arOrder,$arFilter,$DEPTH_LEVEL=1) {
					$arFilterTmp=$arFilter;
					$arFilterTmp["PARENT_ID"] = $collection_id;
					$rsCollections = CMedialibCollection::GetList(array('arOrder'=>$arOrder,'arFilter' => $arFilterTmp));
					foreach($rsCollections as $arCollection) {
						$arCollection["DEPTH_LEVEL"]=$DEPTH_LEVEL;
						$arCollections[] = $arCollection;
						self::GetRecursive($arCollection["ID"],$arCollections,$arOrder,$arFilter,$DEPTH_LEVEL+1);
					}
				}
			}
		}

		$rsCollections = Array();
		$parent_collection = $arParams["PARENT_COLLECTION"]?$arParams["PARENT_COLLECTION"]:0;
		CGrain_MedialibCollectionListTools::GetRecursive($parent_collection,$rsCollections,$arSort,$arFilter);
	
	}
	
	foreach($rsCollections as $arCollection) {
		
		$arItem = Array(
			"NAME" => (array_key_exists("DEPTH_LEVEL",$arCollection)?str_repeat(".",$arCollection["DEPTH_LEVEL"]-1):"").$arCollection["NAME"],
		);
		
		$arResult["DATA"][$arCollection["ID"]] = $arItem;
			
	}
	
	if($arResult["AJAX_RETURN"] && $arParams["INCLUDE_SUBLEVELS"]) {
		foreach($arResult["DATA"] as $key => $arItem) 
			if(!preg_match("/".preg_quote($arResult["AJAX_SEARCH_QUERY"])."/i".(BX_UTF?"u":""),$arItem["NAME"]))
				unset($arResult["DATA"][$key]);
	} elseif($arResult["SELECTED_VALUE"] && $arParams["INCLUDE_SUBLEVELS"]){
		foreach($arResult["DATA"] as $key => $arItem) 
			if(
				(is_array($arResult["SELECTED_VALUE"]) && !in_array($key,$arResult["SELECTED_VALUE"]))
				|| (!is_array($arResult["SELECTED_VALUE"]) && $arResult["SELECTED_VALUE"]!=$key)
			)
				unset($arResult["DATA"][$key]);
	}

}
?>