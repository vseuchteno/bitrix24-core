<?
if(CModule::IncludeModule("sale")) {

	/* prepare parameters */

	if(!in_array($arParams["TYPE"],Array("ALL","CUSTOM","AUTO")))
		$arParams["TYPE"] = "ALL";
	
	$arParams["ACTIVE"] = $arParams["ACTIVE"]=="Y";
	
	$arParams["SORT_BY"] = trim($arParams["SORT_BY"]);
	if(strlen($arParams["SORT_BY"])<=0)
		$arParams["SORT_BY"] = "SORT";
	$arParams["SORT_ORDER"] = strtoupper($arParams["SORT_ORDER"]);
	if($arParams["SORT_ORDER"]!="DESC")
		 $arParams["SORT_ORDER"]="ASC";
		
	/* build list */
	
	// custom handlers
	
	if($arParams["TYPE"]=="ALL" || $arParams["TYPE"]=="CUSTOM") {
	
		$arFilter = Array();
		if(!$arParams["ADMIN_SECTION"])
			$arFilter["LID"]=SITE_ID;
		if($arParams["ACTIVE"])
			$arFilter["ACTIVE"] = "Y";
		if($arResult["AJAX_RETURN"]) 
			$arFilter["%NAME"] = $arResult["AJAX_SEARCH_QUERY"];
		if($arResult["SELECTED_VALUE"]) 
			$arFilter["ID"] = $arResult["SELECTED_VALUE"];

		$rsDelivery = CSaleDelivery::GetList(Array($arParams["SORT_BY"]=>$arParams["SORT_ORDER"]),$arFilter,false,false,Array("ID","NAME","SORT"));	
		
		while($arDelivery=$rsDelivery->GetNext()) {

			$arItem = Array(
				"NAME" => $arDelivery["NAME"],
			);
						
			$arResult["DATA"][$arDelivery["ID"]] = $arItem;
		
		}
	
	}	
	
	// automated handlers

	if($arParams["TYPE"]=="ALL" || $arParams["TYPE"]=="AUTO") {
	
		$arFilter = Array();
		if(!$arParams["ADMIN_SECTION"])
			$arFilter["SITE_ID"]=SITE_ID;
		if($arParams["ACTIVE"])
			$arFilter["ACTIVE"] = "Y";
		else
			$arFilter["ACTIVE"] = "ALL";
		if($arResult["SELECTED_VALUE"]) 
			$arFilter["SID"] = is_array($arResult["SELECTED_VALUE"])?implode("|",$arResult["SELECTED_VALUE"]):$arResult["SELECTED_VALUE"];

		$rsDelivery = CSaleDeliveryHandler::GetList(Array($arParams["SORT_BY"]=>$arParams["SORT_ORDER"]),$arFilter);	
		
		while($arDelivery=$rsDelivery->GetNext()) {

			if($arResult["AJAX_RETURN"] && !preg_match("/".preg_quote($arResult["AJAX_SEARCH_QUERY"])."/i".(BX_UTF?"u":""),$arDelivery["NAME"])) 
				continue;

			$arItem = Array(
				"NAME" => $arDelivery["NAME"],
			);
						
			$arResult["DATA"][$arDelivery["SID"]] = $arItem;
		
		}
	
	}
	
}
?>