<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(CModule::IncludeModule("sale")) {

	$rsPtype = CSalePersonType::GetList(Array("SORT"=>"ASC"));
	$arPtypes=Array();
	while ($arPtype = $rsPtype->Fetch())
		$arPtypes[$arPtype["ID"]] = $arPtype["NAME"];
	
	$arSorts = Array("ASC"=>GetMessage("GRAIN_LINKS_LIST_SALE_PAYSYSTEM_SORT_ASC"), "DESC"=>GetMessage("GRAIN_LINKS_LIST_SALE_PAYSYSTEM_SORT_DESC"));
	$arSortFields = Array(
		"ID"=>GetMessage("GRAIN_LINKS_LIST_SALE_PAYSYSTEM_SORT_FID"),
		"NAME"=>GetMessage("GRAIN_LINKS_LIST_SALE_PAYSYSTEM_SORT_FNAME"),
		"SORT"=>GetMessage("GRAIN_LINKS_LIST_SALE_PAYSYSTEM_SORT_FSORT"),
	);

}

$arComponentParameters["PARAMETERS"]["PERSON_TYPE"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_SALE_PAYSYSTEM_PARAM_PERSON_TYPE"),
	"TYPE" => "LIST",
	"VALUES" => $arPtypes,
	"DEFAULT" => "",
	"REFRESH" => "Y",
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["ACTIVE"] = array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_SALE_PAYSYSTEM_PARAM_ACTIVE"),
	"TYPE" => "CHECKBOX",
    "DEFAULT" => "N",
);

$arComponentParameters["PARAMETERS"]["SORT_BY1"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_SALE_PAYSYSTEM_PARAM_SORT_BY1"),
	"TYPE" => "LIST",
	"DEFAULT" => "SORT",
	"VALUES" => $arSortFields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER1"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_SALE_PAYSYSTEM_PARAM_SORT_ORDER1"),
	"TYPE" => "LIST",
	"DEFAULT" => "ASC",
	"VALUES" => $arSorts,
);

$arComponentParameters["PARAMETERS"]["SORT_BY2"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_SALE_PAYSYSTEM_PARAM_SORT_BY2"),
	"TYPE" => "LIST",
	"DEFAULT" => "NAME",
	"VALUES" => $arSortFields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER2"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_SALE_PAYSYSTEM_PARAM_SORT_ORDER2"),
	"TYPE" => "LIST",
	"DEFAULT" => "ASC",
	"VALUES" => $arSorts,
);

?>