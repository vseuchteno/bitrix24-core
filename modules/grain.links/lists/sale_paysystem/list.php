<?
if(CModule::IncludeModule("sale")) {

	/* prepare parameters */

	$arParams["PERSON_TYPE"] = intval($arParams["PERSON_TYPE"]);
	
	$arParams["ACTIVE"] = $arParams["ACTIVE"]=="Y";
	
	$arParams["SORT_BY1"] = trim($arParams["SORT_BY1"]);
	if(strlen($arParams["SORT_BY1"])<=0)
		$arParams["SORT_BY1"] = "SORT";
	$arParams["SORT_ORDER1"] = strtoupper($arParams["SORT_ORDER1"]);
	if($arParams["SORT_ORDER1"]!="DESC")
		 $arParams["SORT_ORDER1"]="ASC";
	if(strlen($arParams["SORT_BY2"])<=0)
		$arParams["SORT_BY2"] = "NAME";
	$arParams["SORT_ORDER2"] = strtoupper($arParams["SORT_ORDER2"]);
	if($arParams["SORT_ORDER2"]!="DESC")
		 $arParams["SORT_ORDER2"]="ASC";
		
	/* build list */
	
	$arSort = array(
		$arParams["SORT_BY1"]=>$arParams["SORT_ORDER1"],
		$arParams["SORT_BY2"]=>$arParams["SORT_ORDER2"],
	);
	if(!array_key_exists("ID", $arSort))
		$arSort["ID"] = "DESC";
	
	$arFilter=Array();
	
	if(!$arParams["ADMIN_SECTION"])
		$arFilter["LID"]=SITE_ID;

	if($arParams["ACTIVE"])
		$arFilter["ACTIVE"] = "Y";

	if($arParams["PERSON_TYPE"])
		$arFilter["PERSON_TYPE_ID"] = $arParams["PERSON_TYPE"];

	if($arResult["AJAX_RETURN"]) $arFilter["%NAME"] = $arResult["AJAX_SEARCH_QUERY"];
	
	if($arResult["SELECTED_VALUE"]) $arFilter["ID"] = $arResult["SELECTED_VALUE"];

	$rsPaySystem = CSalePaySystem::GetList($arSort,$arFilter,false,false,Array("ID","NAME","SORT"));
	
	while($arPaySystem=$rsPaySystem->GetNext()) {

		$arItem = Array(
			"NAME" => $arPaySystem["NAME"],
		);
				
		$arResult["DATA"][$arPaySystem["ID"]] = $arItem;
		
	}
	
}
?>