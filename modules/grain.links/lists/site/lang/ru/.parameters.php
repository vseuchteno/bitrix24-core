<?

$MESS["GRAIN_LINKS_LIST_SITE_PARAM_ACTIVE"] = "Только активные";
$MESS["GRAIN_LINKS_LIST_SITE_PARAM_CUR_LANG_ONLY"] = "Только на текущем языке";

$MESS["GRAIN_LINKS_LIST_SITE_PARAM_SORT_BY"] = "Поле для первой сортировки";
$MESS["GRAIN_LINKS_LIST_SITE_PARAM_SORT_ORDER"] = "Направление для первой сортировки";

$MESS["GRAIN_LINKS_LIST_SITE_SORT_ASC"] = "По возрастанию";
$MESS["GRAIN_LINKS_LIST_SITE_SORT_DESC"] = "По убыванию";
$MESS["GRAIN_LINKS_LIST_SITE_SORT_FID"] = "ID";
$MESS["GRAIN_LINKS_LIST_SITE_SORT_FNAME"] = "Название";
$MESS["GRAIN_LINKS_LIST_SITE_SORT_FSORT"] = "Сортировка";
$MESS["GRAIN_LINKS_LIST_SITE_SORT_FDIR"] = "Имя папки";
$MESS["GRAIN_LINKS_LIST_SITE_SORT_FLENDIR"] = "Длина имени папки";

?>