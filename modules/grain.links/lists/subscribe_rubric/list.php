<?
if(CModule::IncludeModule("subscribe")) {

	/* prepare parameters */

	$arParams["ACTIVE"] = $arParams["ACTIVE"]=="Y";

	$arParams["SORT_BY1"] = trim($arParams["SORT_BY1"]);
	if(strlen($arParams["SORT_BY1"])<=0)
		$arParams["SORT_BY1"] = "SORT";
	$arParams["SORT_ORDER1"] = strtoupper($arParams["SORT_ORDER1"]);
	if($arParams["SORT_ORDER1"]!="DESC")
		 $arParams["SORT_ORDER1"]="ASC";
	if(strlen($arParams["SORT_BY2"])<=0)
		$arParams["SORT_BY2"] = "NAME";
	$arParams["SORT_ORDER2"] = strtoupper($arParams["SORT_ORDER2"]);
	if($arParams["SORT_ORDER2"]!="DESC")
		 $arParams["SORT_ORDER2"]="ASC";
		
	/* build list */
	
	$arSort = array(
		$arParams["SORT_BY1"]=>$arParams["SORT_ORDER1"],
		$arParams["SORT_BY2"]=>$arParams["SORT_ORDER2"],
	);
	if(!array_key_exists("ID", $arSort))
		$arSort["ID"] = "DESC";
	
	$arFilter = Array();

	if(!$arParams["ADMIN_SECTION"])
		$arFilter["LID"]=SITE_ID;

	if($arParams["ACTIVE"])
		$arFilter["ACTIVE"] = "Y";
	if($arParams["VISIBILITY"]=="VISIBLE")
		$arFilter["VISIBLE"] = "Y";
	elseif($arParams["VISIBILITY"]=="HIDDEN")
		$arFilter["VISIBLE"] = "N";
	
	if($arResult["AJAX_RETURN"]) $arFilter["NAME"] = $arResult["AJAX_SEARCH_QUERY"];
	
	if($arResult["SELECTED_VALUE"]) $arFilter["ID"] = $arResult["SELECTED_VALUE"];

	$rsRubric = CRubric::GetList($arSort,$arFilter);
	
	while($arRubric=$rsRubric->GetNext()) {

		$arItem = Array(
			"NAME" => $arRubric["NAME"],
		);
		
	    if($arParams["SHOW_URL"] && $arParams["RUBRIC_URL"]) {
	    
	    	$arRubric["SITE_SERVER_NAME"] = defined("SITE_SERVER_NAME")?SITE_SERVER_NAME:"";
	    	$arRubric["SITE_ID"] = defined("SITE_ID")?SITE_ID:"";
	    	$arRubric["SITE_DIR"] = defined("SITE_DIR")?SITE_DIR:"";
	    	
	    	$arItem["URL"] = $arParams["RUBRIC_URL"];
	    	foreach($arRubric as $FIELD_NAME=>$FIELD_VALUE)
	    		if(substr($FIELD_NAME,0,1)!="~")
	    			$arItem["URL"] = str_replace("#".$FIELD_NAME."#",$FIELD_VALUE,$arItem["URL"]);

	    	$arItem["URL"] = preg_replace("'/+'s", "/", $arItem["URL"]);
	    	$arItem["URL"] = htmlspecialcharsbx($arItem["URL"]);
	    	
	    }
		
		$arResult["DATA"][$arRubric["ID"]] = $arItem;
		
	}
	
}
?>