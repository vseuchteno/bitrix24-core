<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$filter = array('USER_TYPE_ID'=>'enumeration');

$rsUserFields = \Bitrix\Main\UserFieldTable::getList(array(
	'order' => array('ENTITY_ID'=>'ASC','SORT'=>'ASC'),
	'filter'=>$filter,
));

$arEntityIds = array();
$arUserFields = array();

while($arUserField=$rsUserFields->fetch())
{
	if(!array_key_exists($arUserField['ENTITY_ID'], $arEntityIds))
		$arEntityIds[$arUserField['ENTITY_ID']] = $arUserField['ENTITY_ID'];
	if(!!$arCurrentValues["ENTITY_ID"] && $arCurrentValues["ENTITY_ID"]!=$arUserField['ENTITY_ID'])
		continue;
	$arUserFields[$arUserField['ID']] = $arUserField['FIELD_NAME'].' ['.$arUserField['ID'].']';
}
	
$arSorts = Array("ASC"=>GetMessage("GRAIN_LINKS_LIST_UF_ENUM_SORT_ASC"), "DESC"=>GetMessage("GRAIN_LINKS_LIST_UF_ENUM_SORT_DESC"));
$arSortFields = Array(
	"SORT"=>GetMessage("GRAIN_LINKS_LIST_UF_ENUM_SORT_FSORT"),
	"VALUE"=>GetMessage("GRAIN_LINKS_LIST_UF_ENUM_SORT_FVALUE"),
	"XML_ID"=>GetMessage("GRAIN_LINKS_LIST_UF_ENUM_SORT_FXML_ID"),
	"ID"=>GetMessage("GRAIN_LINKS_LIST_UF_ENUM_SORT_FID"),
);


$arComponentParameters["PARAMETERS"]["ENTITY_ID"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_UF_ENUM_PARAM_ENTITY_ID"),
	"TYPE" => "LIST",
	"VALUES" => $arEntityIds,
	"DEFAULT" => "news",
	"REFRESH" => "Y",
);

$arComponentParameters["PARAMETERS"]["USER_FIELD_ID"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_UF_ENUM_PARAM_USER_FIELD_ID"),
	"TYPE" => "LIST",
	"VALUES" => $arUserFields,
	"DEFAULT" => '',
	"ADDITIONAL_VALUES" => "Y",
	"MULTIPLE" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_BY1"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_UF_ENUM_PARAM_SORT_BY1"),
	"TYPE" => "LIST",
	"DEFAULT" => "SORT",
	"VALUES" => $arSortFields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER1"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_UF_ENUM_PARAM_SORT_ORDER1"),
	"TYPE" => "LIST",
	"DEFAULT" => "ASC",
	"VALUES" => $arSorts,
);

$arComponentParameters["PARAMETERS"]["SORT_BY2"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_UF_ENUM_PARAM_SORT_BY2"),
	"TYPE" => "LIST",
	"DEFAULT" => "ID",
	"VALUES" => $arSortFields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER2"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_UF_ENUM_PARAM_SORT_ORDER2"),
	"TYPE" => "LIST",
	"DEFAULT" => "DESC",
	"VALUES" => $arSorts,
);

?>