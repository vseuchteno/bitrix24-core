<?

$MESS["GRAIN_LINKS_LIST_UF_ENUM_PARAM_ENTITY_ID"] = "Entity";
$MESS["GRAIN_LINKS_LIST_UF_ENUM_PARAM_USER_FIELD_ID"] = "Properties";

$MESS["GRAIN_LINKS_LIST_UF_ENUM_PARAM_SORT_BY1"] = "Field for the first sorting pass";
$MESS["GRAIN_LINKS_LIST_UF_ENUM_PARAM_SORT_ORDER1"] = "Direction for the first sorting pass";
$MESS["GRAIN_LINKS_LIST_UF_ENUM_PARAM_SORT_BY2"] = "Field for the second sorting pass";
$MESS["GRAIN_LINKS_LIST_UF_ENUM_PARAM_SORT_ORDER2"] = "Direction for the second sorting pass";

$MESS["GRAIN_LINKS_LIST_UF_ENUM_SORT_ASC"] = "Ascending";
$MESS["GRAIN_LINKS_LIST_UF_ENUM_SORT_DESC"] = "Descending";
$MESS["GRAIN_LINKS_LIST_UF_ENUM_SORT_FSORT"] = "Sort";
$MESS["GRAIN_LINKS_LIST_UF_ENUM_SORT_FVALUE"] = "Value";
$MESS["GRAIN_LINKS_LIST_UF_ENUM_SORT_FXML_ID"] = "XML_ID";
$MESS["GRAIN_LINKS_LIST_UF_ENUM_SORT_FID"] = "ID";


?>