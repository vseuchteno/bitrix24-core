<?

/* prepare parameters */
	
$arParams["SORT_BY1"] = trim($arParams["SORT_BY1"]);
if(strlen($arParams["SORT_BY1"])<=0)
	$arParams["SORT_BY1"] = "SORT";
$arParams["SORT_ORDER1"] = strtoupper($arParams["SORT_ORDER1"]);
if($arParams["SORT_ORDER1"]!="DESC")
	 $arParams["SORT_ORDER1"]="ASC";
if(strlen($arParams["SORT_BY2"])<=0)
	$arParams["SORT_BY2"] = "ID";
$arParams["SORT_ORDER2"] = strtoupper($arParams["SORT_ORDER2"]);
if($arParams["SORT_ORDER2"]!="ASC")
	 $arParams["SORT_ORDER2"]="DESC";
	
/* build list */

$order = array(
	$arParams["SORT_BY1"]=>$arParams["SORT_ORDER1"],
	$arParams["SORT_BY2"]=>$arParams["SORT_ORDER2"],
);
if(!array_key_exists("ID", $order))
	$order["ID"] = "DESC";

$filter = array (
	"USER_FIELD_ID" => $arParams["USER_FIELD_ID"],
);

if($arResult["AJAX_RETURN"]) $filter["%VALUE"] = $arResult["AJAX_SEARCH_QUERY"];

if($arResult["SELECTED_VALUE"]) $filter["ID"] = $arResult["SELECTED_VALUE"];

$rsEnum = \CUserFieldEnum::GetList($order, $filter);
		
while($arEnum=$rsEnum->fetch()) 
{
	$arItem = Array(
		"NAME" => htmlspecialcharsbx($arEnum["VALUE"]),
	);
			
	$arResult["DATA"][$arEnum["ID"]] = $arItem;
}
	
?>