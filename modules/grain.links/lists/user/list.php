<?

/* prepare parameters */

$arGroups = $arParams["GROUP_ID"];
$arParams["GROUP_ID"] = Array();
foreach($arGroups as $group_id)
    if(intval($group_id)>0)
    	$arParams["GROUP_ID"][] = intval($group_id);

$arParams["ACTIVE"] = $arParams["ACTIVE"]=="Y";

$arParams["SORT_BY"] = trim($arParams["SORT_BY"]);
if(strlen($arParams["SORT_BY"])<=0)
    $arParams["SORT_BY"] = "LAST_NAME";
$arParams["SORT_ORDER"] = strtoupper($arParams["SORT_ORDER"]);
if($arParams["SORT_ORDER"]!="DESC")
     $arParams["SORT_ORDER"]="ASC";
    
/* build list */

$arFilter = Array();
if($arParams["GROUP_ID"])
	$arFilter["GROUPS_ID"] = $arParams["GROUP_ID"];

if($arParams["ACTIVE"])
    $arFilter["ACTIVE"] = "Y";

if($arResult["AJAX_RETURN"]) $arFilter["NAME"] = "%".$arResult["AJAX_SEARCH_QUERY"]."%";

if($arResult["SELECTED_VALUE"])	$arFilter["ID"] = $arResult["SELECTED_VALUE"];

$rsUsers = CUser::GetList(($by=$arParams["SORT_BY"]), ($order=$arParams["SORT_ORDER"]),$arFilter);

while($arUser=$rsUsers->GetNext()) {

    $arItem = Array(
    	"NAME" => $arUser["NAME"]." ".$arUser["LAST_NAME"]." [".$arUser["LOGIN"]."]",
    );
    
    if($arParams["SHOW_URL"] && $arParams["USER_URL"]) {
    
    	$arUser["SITE_SERVER_NAME"] = defined("SITE_SERVER_NAME")?SITE_SERVER_NAME:"";
    	$arUser["SITE_ID"] = defined("SITE_ID")?SITE_ID:"";
    	$arUser["SITE_DIR"] = defined("SITE_DIR")?SITE_DIR:"";
    	
    	$arItem["URL"] = $arParams["USER_URL"];
    	foreach($arUser as $FIELD_NAME=>$FIELD_VALUE)
    		if(substr($FIELD_NAME,0,1)!="~")
    			$arItem["URL"] = str_replace("#".$FIELD_NAME."#",$FIELD_VALUE,$arItem["URL"]);

    	$arItem["URL"] = preg_replace("'/+'s", "/", $arItem["URL"]);
    	$arItem["URL"] = htmlspecialcharsbx($arItem["URL"]);
    	
    }
    
    $arResult["DATA"][$arUser["ID"]] = $arItem;
    
}
	
?>