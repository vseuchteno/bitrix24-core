<?

$MESS["GRAIN_LINKS_LIST_VOTE_VOTE_PARAM_CHANNEL_ID"] = "Группы опросов";
$MESS["GRAIN_LINKS_LIST_VOTE_VOTE_PARAM_SORT_BY"] = "Поле для сортировки";
$MESS["GRAIN_LINKS_LIST_VOTE_VOTE_PARAM_SORT_ORDER"] = "Направление для сортировки";
$MESS["GRAIN_LINKS_LIST_VOTE_VOTE_PARAM_ACTIVE"] = "Только активные";
$MESS["GRAIN_LINKS_LIST_VOTE_VOTE_PARAM_CHANNEL_ACTIVE"] = "Только в активной группе";
$MESS["GRAIN_LINKS_LIST_VOTE_VOTE_PARAM_CHANNEL_VISIBLE"] = "Только в видимой группе";

$MESS["GRAIN_LINKS_LIST_VOTE_VOTE_SORT_ASC"] = "По возрастанию";
$MESS["GRAIN_LINKS_LIST_VOTE_VOTE_SORT_DESC"] = "По убыванию";
$MESS["GRAIN_LINKS_LIST_VOTE_VOTE_SORT_FID"] = "ID";
$MESS["GRAIN_LINKS_LIST_VOTE_VOTE_SORT_FTITLE"] = "Заголовок";
$MESS["GRAIN_LINKS_LIST_VOTE_VOTE_SORT_FSORT"] = "Индекс сортировки";
$MESS["GRAIN_LINKS_LIST_VOTE_VOTE_SORT_FDSTART"] = "Дата начала";
$MESS["GRAIN_LINKS_LIST_VOTE_VOTE_SORT_FDEND"] = "Дата окончания";

$MESS["GRAIN_LINKS_LIST_VOTE_VOTE_PARAM_VOTE_URL"] = "Шаблон пути к опросу";

?>