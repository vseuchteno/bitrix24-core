<?

if(CModule::IncludeModule("vote")) {

	/* prepare parameters */
	
	$arVotes = $arParams["CHANNEL_ID"];
	$arParams["CHANNEL_ID"] = Array();
	foreach($arVotes as $channel_id)
	    if(intval($channel_id)>0)
	    	$arParams["CHANNEL_ID"][] = intval($channel_id);

	$arParams["ACTIVE"] = $arParams["ACTIVE"]=="Y";
	$arParams["CHANNEL_ACTIVE"] = $arParams["CHANNEL_ACTIVE"]=="Y";
	$arParams["CHANNEL_VISIBLE"] = $arParams["CHANNEL_VISIBLE"]=="Y";
	
	$arParams["SORT_BY"] = trim($arParams["SORT_BY"]);
	if(strlen($arParams["SORT_BY"])<=0)
	    $arParams["SORT_BY"] = "S_C_SORT";
	$arParams["SORT_ORDER"] = strtoupper($arParams["SORT_ORDER"]);
	if($arParams["SORT_ORDER"]!="DESC")
	     $arParams["SORT_ORDER"]="ASC";
	    
	/* build list */
	
	$arFilter = Array();
	if($arParams["CHANNEL_ID"]) {
		$arFilter["CHANNEL_ID"] = "";
		foreach($arParams["CHANNEL_ID"] as $channel_id) {
			if(strlen($arFilter["CHANNEL_ID"])>0) $arFilter["CHANNEL_ID"] .= "|";
			$arFilter["CHANNEL_ID"] .= $channel_id;
		}
	}

	if($arParams["ACTIVE"])
		$arFilter["ACTIVE"] = "Y";
	if($arParams["CHANNEL_ACTIVE"])
		$arFilter["CHANNEL_ACTIVE"] = "Y";
	if($arParams["CHANNEL_VISIBLE"])
		$arFilter["CHANNEL_HIDDEN"] = "N";
	
	if($arResult["AJAX_RETURN"]) {
		$arFilter["TITLE"] = $arResult["AJAX_SEARCH_QUERY"];
		$arFilter["TITLE_EXACT_MATCH"] = "N";
	}
	
	if($arResult["SELECTED_VALUE"]) {
		$arFilter["ID"] = "";
		if(is_array($arResult["SELECTED_VALUE"])) {
			foreach($arResult["SELECTED_VALUE"] as $value) {
				if(strlen($arFilter["ID"])>0) $arFilter["ID"] .= "|";
				$arFilter["ID"] .= $value;
			}
		} else $arFilter["ID"] = $arResult["SELECTED_VALUE"];
	}

	$rsVote = CVote::GetList(($by=$arParams["SORT_BY"]), ($order=$arParams["SORT_ORDER"]),$arFilter,$isFiltered);
	
	while($arVote=$rsVote->GetNext()) {
	
	    $arItem = Array(
	    	"NAME" => $arVote["TITLE"],
	    );
	    
	    if($arParams["SHOW_URL"] && $arParams["VOTE_URL"]) {
	    
	    	$arVote["SITE_SERVER_NAME"] = defined("SITE_SERVER_NAME")?SITE_SERVER_NAME:"";
	    	$arVote["SITE_ID"] = defined("SITE_ID")?SITE_ID:"";
	    	$arVote["SITE_DIR"] = defined("SITE_DIR")?SITE_DIR:"";
	    	
	    	$arItem["URL"] = $arParams["VOTE_URL"];
	    	foreach($arVote as $FIELD_NAME=>$FIELD_VALUE)
	    		if(substr($FIELD_NAME,0,1)!="~")
	    			$arItem["URL"] = str_replace("#".$FIELD_NAME."#",$FIELD_VALUE,$arItem["URL"]);

	    	$arItem["URL"] = preg_replace("'/+'s", "/", $arItem["URL"]);
	    	$arItem["URL"] = htmlspecialcharsbx($arItem["URL"]);
	    	
	    }
	    
	    $arResult["DATA"][$arVote["ID"]] = $arItem;
	    
	}
	
}
?>