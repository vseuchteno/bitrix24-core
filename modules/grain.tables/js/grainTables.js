BX.namespace("BX.TableView");

(function() {
    'use strict';

    BX.TableView = {

        //вывод таблиц в одну
        changeViewTable: function (parameters) {

            var nodeId = (parameters.nodeId)?parameters.nodeId:'';
            var tableId = (parameters.tableId)?parameters.tableId:'';

            if(!nodeId && !tableId)
                return;

            BX.ready(function() {
                var sections = document.querySelectorAll(nodeId);

                if(sections.length > 0){

                    for (var k in sections) {
                        if (!sections.hasOwnProperty(k))
                            continue;

                        var tables = sections[k].querySelectorAll(tableId);

                        if(tables.length > 1){
                            for (var t in tables) {

                                if (!tables.hasOwnProperty(t))
                                    continue;

                                var nodeTableTbody = tables[0].querySelector('tbody');

                                if(t > 0){
                                    var nodeTable = tables[t].querySelector('tbody > tr');

                                    if(nodeTable)
                                    nodeTableTbody.appendChild(nodeTable);
                                }

                            }
                        }
                    }
                }

            });

        },

    }


})();

