<?
namespace ITHive\ChangePortalDefaulTheme;

use \Bitrix\Main\Config\Option,
    \Bitrix\Main\Application,
    \Bitrix\Intranet,
    \Bitrix\Main\Localization\Loc,
    \Bitrix\Materials\Exception,
    \Bitrix\Main\Text\BinaryString,
    \Bitrix\Main\Security;

class ChangeTheme
{
    protected static $MODULE_ID = "ithive.changeportaldefaultheme";
    protected static $AROPTIONS = array();
    protected $compiler;
    private static $offModule = null;
    private static $adminModule = null;
    private static $resetStyles = null;

    public function __construct()
    {

    }
    /**
     * ���������� ����� � �����
     */
    public function initTheme()
    {
        global $USER;
        $random = \Bitrix\Main\Security\Random::getInt(0,10000000);

        if (file_exists(Application::getDocumentRoot() . '/bitrix/js/' . static::$MODULE_ID . '/script.js'))
            \Bitrix\Main\Page\Asset::getInstance()->addJs('/bitrix/js/' . static::$MODULE_ID . '/script.js?' .$random, true);

        if(self::getManagementModuleModeAdmin() && !$USER->isAdmin()
            || defined('ADMIN_SECTION') && (ADMIN_SECTION === true)
            || self::getManagementModuleMode()
        )
            return false;

        if ($USER->IsAuthorized()) {
            if (file_exists(Application::getDocumentRoot() . '/bitrix/css/' . static::$MODULE_ID . '/auth.css'))
                \Bitrix\Main\Page\Asset::getInstance()->addString("<link href=" . '/bitrix/css/' . static::$MODULE_ID . '/auth.css?' .$random. " rel='stylesheet' type='text/css'>", true);
        } else {
            if (file_exists(Application::getDocumentRoot() . '/bitrix/css/' . static::$MODULE_ID . '/noauth.css')) {
                \Bitrix\Main\Page\Asset::getInstance()->addString("<link href=" . '/bitrix/css/' . static::$MODULE_ID . '/noauth.css?' .$random. " rel='stylesheet' type='text/css'>", true);
            }
        }

        if (file_exists(Application::getDocumentRoot() . '/bitrix/css/' . static::$MODULE_ID . '/dopstyles.css') && strlen(self::getDopStyles()) > 0)
            \Bitrix\Main\Page\Asset::getInstance()->addString("<link href=" . '/bitrix/css/' . static::$MODULE_ID . '/dopstyles.css?' .$random. " rel='stylesheet' type='text/css'>", true);


        $arOptions = self::getOption();

        //set favicon
        if ($arOptions['files']['favicon-@favicon'])
            \Bitrix\Main\Page\Asset::getInstance()->addString("<link rel='shortcut icon' href=" . \CFile::GetPath($arOptions['files']['favicon-@favicon']) . ">", true);

    }
    /**
     * ��������� ������� ��� �������� ������
     */
    public static function getManagementModuleMode(){
        if (is_null(self::$offModule)){
            $offModule =  $arOptions = self::getOption();
            self::$offModule = $offModule["module_mode"] == "Y" ? true : false;
        }else{
            return self::$offModule;
        }
        return self::$offModule;
    }

    /**
     * ��������� �������� �� ������ ������ ��� ������������
     */
    public static function getManagementModuleModeAdmin(){
        if (is_null(self::$adminModule)){
            $adminModule =  $arOptions = self::getOption();
            self::$adminModule = $adminModule["module_admin"] == "Y" ? true : false;
        }else{
            return self::$adminModule;
        }

        return self::$adminModule;
    }

    /**
     * ��������� Request � ��������� ���������� setOption() � ��������� less
     * @param array $request ������ post �������
     * @return string error ������
     */
    public function checkRequest($request)
    {
        $arOptions = self::getOption();
        $arRequest = $request->getPostList()->toArray();
        $arDbFiles = $request->getFileList()->toArray();

        static::$AROPTIONS = array(
            "noauth" => ($arRequest['noauth'])?$arRequest['noauth']: array(),
            "auth" => ($arRequest['auth'])?$arRequest['auth']: array(),
            "module_mode" => ($arRequest["module_mode"] == "Y")?"Y": 'N',
            "module_admin" => ($arRequest["module_admin"] == "Y")?"Y": 'N',
            "files" => ($arOptions["files"])?$arOptions["files"]:array()
        );

        //check delete file
        if($arOptions["files"]){
            foreach ($arOptions["files"] as $key => $file){
                if(array_key_exists($key.'_del',$arRequest) && $key == 'file-@client_logo'){
                    \COption::RemoveOption("bitrix24", "client_logo");
                    \CFile::Delete($file);
                }
                elseif (array_key_exists($key.'_del',$arRequest))
                    \CFile::Delete($file);

            }
        }

        //set default logo
        if ($arDbFiles['file-@client_logo']) {

            $arFile = $arDbFiles['file-@client_logo'];

            $error = "";

            if ($arFile["name"]) {
                $fileID = static::Bitrix24SaveLogo($arFile, array("extensions" => "png", "max_height" => 55, "max_width" => 222));
                if (intval($fileID)) {
                    \COption::SetOptionInt("bitrix24", "client_logo", $fileID);
                } else {
                    $error = str_replace("<br>", "", $fileID);
                    return $error;
                }
            }
        }

        //set autorize style images
        if($arDbFiles) {
            foreach ($arDbFiles as $key => $files) {
                if (is_array($files) && !empty($files["tmp_name"]))
                    static::$AROPTIONS["files"][$key] = self::SaveImage($files, array(), $arOptions["files"], $key);
            }
        }

        //save dop styles
        self::saveDopStyles($arRequest["CONTENT"]);

        //save option post request
        self::setOption();

        //generate less and css
        \ITHive\ChangePortalDefaulTheme\Less::createLessCss();
    }

    /**
     * ��������� �������� ������
     * @param array $arFile ������ ������ ��������
     * @param array $arRestriction ��������� ������������ �����
     * @param string $mode ����� ����������
     * @return int id ������������ �����
     */
    public function SaveImage($arFile, $arRestriction = Array(), $arOptions, $lessKey)
    {
        if($arOptions[$lessKey])
            \CFile::Delete($arOptions[$lessKey]);

        $max_file_size = (array_key_exists("max_file_size", $arRestriction) ? intval($arRestriction["max_file_size"]) : 0);
        $max_width = (array_key_exists("max_width", $arRestriction) ? intval($arRestriction["max_width"]) : 0);
        $max_height = (array_key_exists("max_height", $arRestriction) ? intval($arRestriction["max_height"]) : 0);
        $extensions = (array_key_exists("extensions", $arRestriction) && strlen($arRestriction["extensions"]) > 0 ? trim($arRestriction["extensions"]) : false);

        $error = \CFile::CheckFile($arFile, $max_file_size, false, $extensions);

        if (strlen($error)>0)
        {
            return $error;
        }

        if ($max_width > 0 || $max_height > 0)
        {
            $error = \CFile::CheckImageFile($arFile, $max_file_size, $max_width, $max_height);
            if (strlen($error)>0)
            {
                return $error;
            }
        }

        $fileID = (int)\CFile::SaveFile($arFile, static::$MODULE_ID);

        return $fileID;
    }
    /**
     * ��������� ������� ��������
     * @param array $arFile ������ ������ ��������
     * @param array $arRestriction ��������� ������������ �����
     * @param string $mode ����� ����������
     * @return int id ������������ �����
     */
    public function Bitrix24SaveLogo($arFile, $arRestriction = Array(), $mode = "")
    {
        $mode == "retina" ? "_retina" : "";
        $oldFileID = \COption::GetOptionInt("bitrix24", "client_logo".$mode, "");

        $arFile = $arFile + Array(
                "del" => ($oldFileID ? "Y" : ""),
                "old_file" => (intval($oldFileID) > 0 ? intval($oldFileID): 0 ),
                "MODULE_ID" => "bitrix24"
            );

        $max_file_size = (array_key_exists("max_file_size", $arRestriction) ? intval($arRestriction["max_file_size"]) : 0);
        $max_width = (array_key_exists("max_width", $arRestriction) ? intval($arRestriction["max_width"]) : 0);
        $max_height = (array_key_exists("max_height", $arRestriction) ? intval($arRestriction["max_height"]) : 0);
        $extensions = (array_key_exists("extensions", $arRestriction) && strlen($arRestriction["extensions"]) > 0 ? trim($arRestriction["extensions"]) : false);

        $error = \CFile::CheckFile($arFile, $max_file_size, false, $extensions);


        if (strlen($error)>0)
        {
            return $error;
        }


        $arFile["name"] = "logo_".randString(8).".png";
        $fileID = (int)\CFile::SaveFile($arFile, "bitrix24");

        return $fileID;
    }
    /**
     * ����������� ����� ������
     */
    public static function importOption()
    {
        if (file_exists(Application::getDocumentRoot().'/bitrix/modules/'.static::$MODULE_ID.'/install/db/options.txt')) {

            static::$AROPTIONS = (array)json_decode(file_get_contents(Application::getDocumentRoot() . '/bitrix/modules/'.static::$MODULE_ID.'/install/db/options.txt'), true);

            if(is_array(static::$AROPTIONS)) {
                if (static::$AROPTIONS["files"]) {
                    foreach (static::$AROPTIONS["files"] as $key => $files) {
                        $arFiles = array(
                            "name" => $files,
                            "tmp_name" => Application::getDocumentRoot() . $files,
                            "MODULE_ID" => static::$MODULE_ID,
                        );
                        if ($key == "file-@client_logo") {
                            static::$AROPTIONS["files"][$key] = static::Bitrix24SaveLogo($arFiles, array("extensions" => "png", "max_height" => 55, "max_width" => 222));
                            if (intval(static::$AROPTIONS["files"][$key])) {
                                \COption::SetOptionInt("bitrix24", "client_logo", static::$AROPTIONS["files"][$key]);
                            }
                        }else {
                            static::$AROPTIONS["files"][$key] = self::SaveImage($arFiles, array(), static::$AROPTIONS["files"], $key);
                        }
                    }
                }
                self::setOption();
                \ITHive\ChangePortalDefaulTheme\Less::createLessCss();
            }

            \COption::SetOptionInt("main", "optimize_css_files", "N");
            \COption::SetOptionInt("main", "compres_css_js_files", "N");

        }
    }

    /**
     * �������� ��������� ������
     * @return array ��������� ������
     */
    public static function getOption()
    {
        return (array)json_decode(\COption::GetOptionString(static::$MODULE_ID, "options"), true);
    }

    /**
     * ��������� �������������� ����� �������
     */
    public static function saveDopStyles($CSS)
    {
        if (file_exists(Application::getDocumentRoot().'/bitrix/css/'.static::$MODULE_ID.'/dopstyles.css')) {
            file_put_contents( Application::getDocumentRoot().'/bitrix/css/'.static::$MODULE_ID.'/dopstyles.css', $CSS);
        }else{
            $fp = fopen(Application::getDocumentRoot().'/bitrix/css/'.static::$MODULE_ID.'/dopstyles.css', $CSS);
            fwrite($fp, $CSS);
            fclose($fp);
        }
    }

    /**
     * �������� �������������� ����� �������
     */
    public static function getDopStyles()
    {
        if (file_exists(Application::getDocumentRoot().'/bitrix/css/'.static::$MODULE_ID.'/dopstyles.css')) {
            return file_get_contents(Application::getDocumentRoot().'/bitrix/css/'.static::$MODULE_ID.'/dopstyles.css');
        }
    }

    /**
     * ��������� ��������� ������
     */
    public static function setOption()
    {
        \COption::SetOptionString(static::$MODULE_ID, "options", json_encode(static::$AROPTIONS));
    }

    /**
     * ������� ��������� ������
     */
    public static function resetOption($request)
    {

        $arOptions = self::getOption();
        $arRequest = $request->getPostList()->toArray();

        static::$AROPTIONS = array(
            "noauth" => array(),
            "auth" => array(),
            "module_mode" =>  ($arOptions['module_mode'] == "Y" && $arRequest["module_mode"] == "Y")?"Y": 'N',
            "module_admin" =>  ($arOptions['module_admin'] == "Y" && $arRequest["module_admin"] == "Y")?"Y": 'N',
        );

        //save option post request
        self::setOption();

        //generate less and css
        \ITHive\ChangePortalDefaulTheme\Less::resetCss();

        return true;
    }

    /**
     * ������� �����
     */
    public static function deleteOption()
    {
        \COption::RemoveOption(static::$MODULE_ID, "options");

        $siteLogo = Intranet\Util::getClientLogo();

        if(is_array($siteLogo)){
            foreach ($siteLogo as $id){
                if($id > 0)
                    \CFile::Delete($id);
            }
            \COption::RemoveOption("bitrix24", "client_logo");
            \COption::RemoveOption("bitrix24", "client_logo_retina");
        }
    }
}