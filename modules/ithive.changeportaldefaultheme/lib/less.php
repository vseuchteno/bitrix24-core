<?

namespace ITHive\ChangePortalDefaulTheme;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;
use \ITHive\ChangePortalDefaulTheme\ChangeTheme;

Loc::loadMessages(__FILE__);

class Less extends \ITHive\ChangePortalDefaulTheme\ChangeTheme{

    /**
     * ������ ���� ����� ������������ ������, ��� �������� ����������� � ��� ������� � �����.
     */
    private static $lessFiles = array(
        "auth" => array(),
        "noauth" => array()
    );

    /**
     * ���������� ������������ ������ ���������� less
     * @return array less
     */
    static public function prepareVarsOptions(){

        $arOptions = ChangeTheme::getOption();

        if (empty($arOptions)) return false;

        $arResult = array();

        foreach ($arOptions as $key => $arOption) {
            if(is_array($arOption)){
                foreach ($arOption as $var => $value){
                    if(stristr($var, "input-"))
                        $vars = str_replace("input-", "", $var);
                    elseif (stristr($var, "file-")) {
                        $vars = str_replace("file-", "", $var);
                        $value = '"'.\CFile::GetPath($value).'"';
                    }
                    elseif (stristr($var, "settings-")) {
                        $vars = str_replace("settings-", "", $var);
                    }
                    elseif (stristr($var, "svg-") && $value) {
                        $vars = str_replace("svg-", "", $var);
                        $value = '"'.str_replace("#",'',$value).'"';
                    }
                    $arResult[$key][$vars] = $value;
                }
            }
        }

        return $arResult;
    }

    /**
     * ���������� less � Css �� ������������� ����������
     * @return bool
     */
    static public function createLessCss(){

        $arOptions = self::prepareVarsOptions();

        if (empty($arOptions)) return false;

        require_once __DIR__ . '/../libs/less/Less.php';



        foreach ($arOptions as $key => $arOption) {
            if($arOptions["files"])
                $arOption = array_merge($arOption, $arOptions["files"]);

            if(
                is_array(self::$lessFiles[$key]) &&
                file_exists($_SERVER['DOCUMENT_ROOT'].'/bitrix/css/'.static::$MODULE_ID.'/'.$key.'.less') &&
                file_exists($_SERVER['DOCUMENT_ROOT'].'/bitrix/css/'.static::$MODULE_ID.'/'.$key.'.css')
            )
            {
                $parser = new \Less_Parser();
                $parser->importDir = array($_SERVER['DOCUMENT_ROOT'].'/bitrix/css/'.static::$MODULE_ID.'/');
                $parser->parse(file_get_contents($_SERVER['DOCUMENT_ROOT'].'/bitrix/css/'.static::$MODULE_ID.'/'.$key.'.less'));
                $parser->ModifyVars($arOption);
                $css = $parser->getCss($_SERVER['DOCUMENT_ROOT'].'/bitrix/css/'.static::$MODULE_ID.'/'.$key.'.less');
                if($css)
                    file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/bitrix/css/' . static::$MODULE_ID . '/' . $key . '.css', $css);

            }

        }

        return true;
    }

    /**
     * ���������� Css �� ��������� ����������, ��� ������ ���� ��������
     * @return bool
     */
    static public function resetCss(){

        $arOptions = ChangeTheme::getOption();

        require_once __DIR__ . '/../libs/less/Less.php';

        $parser = new \Less_Parser();
        $parser->importDir = array($_SERVER['DOCUMENT_ROOT'].'/bitrix/css/'.static::$MODULE_ID.'/');

        foreach ($arOptions as $key => $arOption) {
            if(
                is_array(self::$lessFiles[$key]) &&
                file_exists($_SERVER['DOCUMENT_ROOT'].'/bitrix/css/'.static::$MODULE_ID.'/'.$key.'.less')){
                $css = $parser->getCss($_SERVER['DOCUMENT_ROOT'].'/bitrix/css/'.static::$MODULE_ID.'/'.$key.'.less');
                if(
                    $css &&
                    file_exists($_SERVER['DOCUMENT_ROOT'].'/bitrix/css/'.static::$MODULE_ID.'/'.$key.'.css')) {
                    file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/bitrix/css/' . static::$MODULE_ID . '/' . $key . '.css', $css);
                }
            }
        }

        return true;
    }
}