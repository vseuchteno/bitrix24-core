<?
$module_id = GetModuleID(__FILE__);

use \Bitrix\Main\Config\Option,
    \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\IO\FileNotFoundException,
    ITHive\ChangePortalDefaulTheme\ChangeTheme as ChangeTheme;

Loc::loadMessages(__FILE__);

$SUP_RIGHT = $APPLICATION->GetGroupRight($module_id);

if(!\Bitrix\Main\Loader::includeModule($module_id) || !$SUP_RIGHT >= "R"){
    CAdminMessage::ShowMessage("Permission denied");
    return;
}else if (CModule::IncludeModuleEx($module_id) == MODULE_DEMO_EXPIRED) {
    Loc::getMessage('MODULE_TREELIKE_COMMENTS_DEMO_EXPIRED');
    return;
}else {
    $ChangeTheme = new ChangeTheme;
}
\Bitrix\Main\Page\Asset::getInstance()->addString("<link href=".'/bitrix/css/'.$module_id.'/options.css'." rel='stylesheet' type='text/css'>", true);

$request = \Bitrix\Main\HttpApplication::getInstance()->getContext()->getRequest();
$arRequest = $request->getPostList()->toArray();

//generate css
if ($request->isPost() && $arRequest["Apply"] && check_bitrix_sessid())
    $ChangeTheme->checkRequest($request);

//reset options modules
if ($request->isPost() && $arRequest["styles_reset"] && check_bitrix_sessid()){
    $ChangeTheme->resetOption($request);
    $strError .= Loc::getMessage("MAIN_TAB_SETTINGS_RESET_STYLES_OK");
}

$arOptions = $ChangeTheme->getOption();

$APPLICATION->SetTitle(Loc::getMessage("TITLE"));

CJSCore::Init(['color_picker']);

if(!CModule::IncludeModule("fileman")) {
    CAdminMessage::ShowMessage("fileman module not defined");
    return;
}

CAdminMessage::ShowMessage($strError);

?>

<form method="POST" action="<?=$APPLICATION->GetCurUri();?>" ENCTYPE="multipart/form-data" class="changePortalTheme" name="changePortalTheme" id="changePortalTheme">
    <?echo bitrix_sessid_post(); ?>
    <?$aTabs = array(
        array(
            "DIV" => "edit1",
            "TAB" => Loc::getMessage("BITRIX_AUTORIZE_PAGE") ,
            "TITLE" => Loc::getMessage("BITRIX_AUTORIZE_PAGE_DESC"),
        ),array(
            "DIV" => "edit2",
            "TAB" => Loc::getMessage("BITRIX_PORTAL_PAGE") ,
            "TITLE" => Loc::getMessage("BITRIX_PORTAL_PAGE_DESC"),
        ),
        array(
            "DIV" => "styles",
            "TAB" => Loc::getMessage("MAIN_TAB_STYLES") ,
            "TITLE" => Loc::getMessage("MAIN_TAB_STYLES_DESC"),
        ),
        array(
            "DIV" => "settings",
            "TAB" => Loc::getMessage("MAIN_TAB_SETTINGS") ,
            "TITLE" => Loc::getMessage("MAIN_TAB_SETTINGS_DESC"),
        ),
        array(
            "DIV" => "rights",
            "TAB" => Loc::getMessage("MAIN_TAB_RIGHTS"),
            "TITLE" => Loc::getMessage("MAIN_TAB_TITLE_RIGHTS"),
            "OPTIONS" => array()
        ),
    );
    $aSubTabs = array(
        array(
            "DIV" => "headers",
            "TAB" => Loc::getMessage("SUB_TAB_HEADER"),
            "TITLE" => Loc::getMessage("SUB_TAB_HEADER_DESC"),
            "OPTIONS" => array()
        ),
        array(
            "DIV" => "links",
            "TAB" => Loc::getMessage("SUB_TAB_LINKS"),
            "TITLE" => Loc::getMessage("SUB_TAB_LINKS_DESC"),
            "OPTIONS" => array()
        ),
        array(
            "DIV" => "forms",
            "TAB" => Loc::getMessage("SUB_TAB_FORMS"),
            "TITLE" => Loc::getMessage("SUB_TAB_FORMS_DESC"),
            "OPTIONS" => array()
        ),
        array(
            "DIV" => "contents",
            "TAB" => Loc::getMessage("SUB_TAB_CONTENT"),
            "TITLE" => Loc::getMessage("SUB_TAB_CONTENT_DESC"),
            "OPTIONS" => array()
        ),
        array(
            "DIV" => "sprite",
            "TAB" => Loc::getMessage("SUB_TAB_SPRITE"),
            "TITLE" => Loc::getMessage("SUB_TAB_SPRITE_DESC"),
            "OPTIONS" => array()
        ),
        array(
            "DIV" => "other",
            "TAB" => Loc::getMessage("SUB_TAB_OTHER"),
            "TITLE" => Loc::getMessage("SUB_TAB_OTHER_DESC"),
            "OPTIONS" => array()
        )
    );
    $tabControl = new CAdminTabControl("tabControl", $aTabs, false, true);
    $subTabControl = new CAdminViewTabControl("subTabControl", $aSubTabs);
    $tabControl->Begin();
    //auth page style
    $tabControl->BeginNextTab();
    ?>
    <tr>
        <td align="center">
            <div class="adm-info-message-wrap">
                <div class="adm-info-message">
                    <?$INSTRUCTION =  '/bitrix/css/' . $module_id . '/images/auth_page.jpg';?>
                    <?=Loc::getMessage('INSTRUCTION_AUTORIZE_PAGE', array('#INSTRUCTION_AUTORIZE_PAGE#'=>$INSTRUCTION))?>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td width="32.3333333%" align="center" valign="top">
                        <table cellpadding="0" cellspacing="10" >
                            <tr class="heading">
                                <td colspan="2"><?=Loc::getMessage("BITRIX_IMG")?></td>
                            </tr>
                            <tr>
                                <td valign="top" align="left">
                                    <label><?= Loc::getMessage("BITRIX_AUTORIZE_PAGE_LOGO")."<br> <b>@client_logo</b>" ?></label><br>  <?=CFile::InputFile("file-@client_logo", 20, ($arOptions["files"]["file-@client_logo"])?$arOptions["files"]["file-@client_logo"]:$str_IMAGE_ID);?></td>
                                <td valign="top" align="left">
                                    <label><?= Loc::getMessage("BITRIX_AUTORIZE_PAGE_IMG_BG")."<br> <b>@bg-fon</b>" ?></label><br>  <?=CFile::InputFile("file-@bg-fon", 20, ($arOptions["files"]["file-@bg-fon"])?$arOptions["files"]["file-@bg-fon"]:$str_IMAGE_ID);?>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="32.3333333%" align="center" valign="top">
                        <table cellpadding="0" cellspacing="10">
                            <tr class="heading">
                                <td colspan="2"><?=Loc::getMessage("BITRIX_COLOR_PLASHKI")?></td>
                            </tr>
                            <tr>
                                <td valign="top" align="left"><label for="input-@body"><?= Loc::getMessage("BITRIX_AUTORIZE_PAGE_BODY_BG")."<br> <b>@body</b>" ?></label><br><input type="text" id="input-@body" name="noauth[input-@body]" class="colorPicket" value="<?=$arOptions["noauth"]["input-@body"]?$arOptions["noauth"]["input-@body"]:"#2fc6f7"?>" /></td>
                                <td valign="top" align="left"><label for="input-@text"><?= Loc::getMessage("BITRIX_AUTORIZE_PAGE_COLOR_TEXT")."<br> <b>@text</b>" ?></label><br><input type="text" id="input-@text" name="noauth[input-@text]" class="colorPicket" value="<?=$arOptions["noauth"]["input-@text"]?$arOptions["noauth"]["input-@text"]:"#005e94"?>" /></td>
                            </tr>
                        </table>
                    </td>
                    <td width="32.3333333%" align="center" valign="top">
                        <table cellpadding="0" cellspacing="10">
                            <tr class="heading">
                                <td colspan="3"><?=Loc::getMessage("BITRIX_COLOR_BTN")?></td>
                            </tr>
                            <tr>
                                <td valign="top" align="left">
                                    <label for="input-@link"><?= Loc::getMessage("BITRIX_AUTORIZE_PAGE_COLOR_LINK")."<br> <b>@link</b>" ?></label><br><input type="text" id="input-@link" name="noauth[input-@link]" class="colorPicket" value="<?=$arOptions["noauth"]["input-@link"]?$arOptions["noauth"]["input-@link"]:"#2fc6f7"?>" /><br>
                                    <label for="input-@link-active"><?= Loc::getMessage("BITRIX_AUTORIZE_PAGE_COLOR_LINK_ACTIVE")."<br> <b>@link-active</b>" ?></label><br><input type="text" id="input-@link-active" name="noauth[input-@link-active]" class="colorPicket" value="<?=$arOptions["noauth"]["input-@link-active"]?$arOptions["noauth"]["input-@link-active"]:"#1ab8eb"?>" /><br>
                                    <label for="input-@link-hover"><?= Loc::getMessage("BITRIX_AUTORIZE_PAGE_COLOR_LINK_HOVER")."<br> <b>@link-hover</b>" ?></label><br><input type="text" id="input-@link-hover" name="noauth[input-@link-hover]" class="colorPicket" value="<?=$arOptions["noauth"]["input-@link-hover"]?$arOptions["noauth"]["input-@link-hover"]:"#1ab8eb"?>" />
                                </td>
                                <td valign="top" align="left">
                                    <label for="input-@button"><?= Loc::getMessage("BITRIX_AUTORIZE_PAGE_COLOR_BUTTON")."<br> <b>@button</b>" ?></label><br><input type="text" id="input-@button" name="noauth[input-@button]" class="colorPicket" value="<?=$arOptions["noauth"]["input-@button"]?$arOptions["noauth"]["input-@button"]:"#2fc6f7"?>" /><br>
                                    <label for="input-@button-active"><?= Loc::getMessage("BITRIX_AUTORIZE_PAGE_COLOR_BUTTON_ACTIVE")."<br> <b>@button-active</b>" ?></label><br><input type="text" id="input-@button-active" name="noauth[input-@button-active]" class="colorPicket" value="<?=$arOptions["noauth"]["input-@button-active"]?$arOptions["noauth"]["input-@button-active"]:"#1a83bf"?>" /><br>
                                    <label for="input-@button-hover"><?= Loc::getMessage("BITRIX_AUTORIZE_PAGE_COLOR_BUTTON_HOVER")."<br> <b>@button-hover</b>" ?></label><br><input type="text" id="input-@button-hover" name="noauth[input-@button-hover]" class="colorPicket" value="<?=$arOptions["noauth"]["input-@button-hover"]?$arOptions["noauth"]["input-@button-hover"]:"#1ab8eb"?>" /><br>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="left"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <?
    $tabControl->EndTab();
    //portal style
    $tabControl->BeginNextTab();
    ?>
    <tr>
        <td>
            <?$subTabControl->Begin();?>
            <?$subTabControl->BeginNextTab();?>
            <div class="adm-info-message-wrap">
                <div class="adm-info-message">
                    <?$INSTRUCTION =  '/bitrix/css/' . $module_id . '/images/header.jpg';?>
                    <?=Loc::getMessage('SUB_TAB1_NOTICE')?><br>
                    <?=Loc::getMessage('SUB_TAB_DESC_PIC', array('#SUB_TAB_DESC_PIC#'=>$INSTRUCTION))?>
                </div>
            </div>
            <table cellpadding="0" cellspacing="10">
                <tr>
                    <td width="50%" valign="top">
                        <table cellpadding="0" cellspacing="10">
                            <tr>
                                <td class="heading" colspan="3">
                                    <?=Loc::getMessage("SUB_TAB1_DESC_1")?>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="left">
                                    <label for="input-@header"><?= Loc::getMessage("SUB_TAB1_COLOR_HEADER")."<br> <b>@header</b>" ?></label><br><input type="text" id="input-@header" name="auth[input-@header]" class="colorPicket" value="<?=$arOptions["auth"]["input-@header"]?$arOptions["auth"]["input-@header"]:"#535c69"?>" /><br>
                                    <label for="input-@text"><?= Loc::getMessage("SUB_TAB1_COLOR_HEADER_TEXT")."<br> <b>@text</b>" ?></label><br><input type="text" id="input-@text" name="auth[input-@text]" class="colorPicket" value="<?=$arOptions["auth"]["input-@text"]?$arOptions["auth"]["input-@text"]:"#ffffff"?>" />
                                    <label for="svg-@icon-color"><?= Loc::getMessage("SUB_TAB1_COLOR_HEADER_ICON")."<br> <b>@icon-color</b>" ?></label><br><input type="text" id="svg-@icon-color" name="auth[svg-@icon-color]" class="colorPicket" value="<?=$arOptions["auth"]["svg-@icon-color"]?$arOptions["auth"]["svg-@icon-color"]:"#ffffff"?>" />
                                </td>
                                <td valign="top" align="left">
                                    <label for="input-@header-search-inner"><?= Loc::getMessage("SUB_TAB1_COLOR_INPUT_SEARCH")."<br> <b>@header-search-inner</b>" ?></label><br><input type="text" id="input-@header-search-inner" name="auth[input-@header-search-inner]" class="colorPicket" value="<?=$arOptions["auth"]["input-@header-search-inner"]?$arOptions["auth"]["input-@header-search-inner"]:"#717a84"?>" /><br>
                                    <label for="input-@header-search-active"><?= Loc::getMessage("SUB_TAB1_COLOR_INPUT_SEARCH_ACTIVE")."<br> <b>@header-search-active</b>" ?></label><br><input type="text" id="input-@header-search-active" name="auth[input-@header-search-active]" class="colorPicket" value="<?=$arOptions["auth"]["input-@header-search-active"]?$arOptions["auth"]["input-@header-search-active"]:"#ffffff"?>" /><br>
                                    <label for="input-@search-title-top-item"><?= Loc::getMessage("SUB_TAB1_COLOR_INPUT_SEARCH_BTN")."<br> <b>@search-title-top-item</b>" ?></label><br><input type="text" id="input-@search-title-top-item" name="auth[input-@search-title-top-item]" class="colorPicket" value="<?=$arOptions["auth"]["input-@search-title-top-item"]?$arOptions["auth"]["input-@search-title-top-item"]:"#eff3f4"?>" /><br>
                                    <label for="input-@search-title-top-item-selected"><?= Loc::getMessage("SUB_TAB1_COLOR_INPUT_SEARCH_BTN_ACTIVE")."<br> <b>@search-title-top-item-selected</b>" ?></label><br><input type="text" id="input-@search-title-top-item-selected" name="auth[input-@search-title-top-item-selected]" class="colorPicket" value="<?=$arOptions["auth"]["input-@search-title-top-item-selected"]?$arOptions["auth"]["input-@search-title-top-item-selected"]:"#d5f4fd"?>" />
                                </td>
                                <td valign="top" align="left">
                                    <label for="input-@timeman-background"><?= Loc::getMessage("SUB_TAB1_COLOR_TIMEMAN_BG")."<br> <b>@timeman-background</b>" ?></label><br><input type="text" id="input-@timeman-background" name="auth[input-@timeman-background]" class="colorPicket" value="<?=$arOptions["auth"]["input-@timeman-background"]?$arOptions["auth"]["input-@timeman-background"]:"#91b33e"?>" /><br>
                                    <label for="input-@timeman-block-hover"><?= Loc::getMessage("SUB_TAB1_COLOR_TIMEMAN_BG_HOVER")."<br> <b>@timeman-block-hover</b>" ?></label><br><input type="text" id="input-@timeman-block-hover" name="auth[input-@timeman-block-hover]" class="colorPicket" value="<?=$arOptions["auth"]["input-@timeman-block-hover"]?$arOptions["auth"]["input-@timeman-block-hover"]:"#78808b"?>" /><br>
                                    <label for="input-@timeman-block-hover-active"><?= Loc::getMessage("SUB_TAB1_COLOR_TIMEMAN_BG_ACTIVE")."<br> <b>@timeman-block-hover-active</b>" ?></label><br><input type="text" id="input-@timeman-block-hover-active" name="auth[input-@timeman-block-hover-active]" class="colorPicket" value="<?=$arOptions["auth"]["input-@timeman-block-hover-active"]?$arOptions["auth"]["input-@timeman-block-hover-active"]:"#3d4653"?>" /><br>
                                    <label for="input-@timeman-expired"><?= Loc::getMessage("SUB_TAB1_COLOR_TIMEMAN_BG_EXPIRED")."<br> <b>@timeman-expired</b>" ?></label><br><input type="text" id="input-@timeman-expired" name="auth[input-@timeman-expired]" class="colorPicket" value="<?=$arOptions["auth"]["input-@timeman-expired"]?$arOptions["auth"]["input-@timeman-expired"]:"#af393e"?>" /><br>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="16.66666%" valign="top" align="left">
                        <table cellpadding="0" cellspacing="10">
                            <tr>
                                <td class="heading" >
                                    <?=Loc::getMessage("SUB_TAB1_DESC_2")?>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="left">
                                    <label for="input-@menu-item-active"><?= Loc::getMessage("SUB_TAB1_COLOR_LMENU_ACTIVE_BTN")."<br> <b>@menu-item-active</b>" ?></label><br><input type="text" id="input-@menu-item-active" name="auth[input-@menu-item-active]" class="colorPicket" value="<?=$arOptions["auth"]["input-@menu-item-active"]?$arOptions["auth"]["input-@menu-item-active"]:"#717a84"?>" /><br>
                                    <label for="input-@menu-item-hover"><?= Loc::getMessage("SUB_TAB1_COLOR_LMENU_HOVER_BTN")."<br> <b>@menu-item-hover</b>" ?></label><br><input type="text" id="input-@menu-item-hover" name="auth[input-@menu-item-hover]" class="colorPicket" value="<?=$arOptions["auth"]["input-@menu-item-hover"]?$arOptions["auth"]["input-@menu-item-hover"]:"#e6eaed"?>" />
                                    <label for="input-@menu-item-text-fullscreen"><?= Loc::getMessage("SUB_TAB1_COLOR_LMENU_FULL_BTN")."<br> <b>@menu-item-text-fullscreen</b>" ?></label><br><input type="text" id="input-@menu-item-text-fullscreen" name="auth[input-@menu-item-text-fullscreen]" class="colorPicket" value="<?=$arOptions["auth"]["input-@menu-item-text-fullscreen"]?$arOptions["auth"]["input-@menu-item-text-fullscreen"]:"#535c69"?>" />
                                    <label for="input-@menu-item-text"><?= Loc::getMessage("SUB_TAB1_COLOR_LMENU_BTN")."<br> <b>@menu-item-text</b>" ?></label><br><input type="text" id="input-@menu-item-text" name="auth[input-@menu-item-text]" class="colorPicket" value="<?=$arOptions["auth"]["input-@menu-item-text"]?$arOptions["auth"]["input-@menu-item-text"]:"#535c69"?>" />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="16.66666%" valign="top" align="left">
                        <table cellpadding="0" cellspacing="10">
                            <tr>
                                <td class="heading" >
                                    <?=Loc::getMessage("SUB_TAB1_DESC_3")?>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="left">
                                    <label for="input-@feed-add-post-form-link"><?= Loc::getMessage("SUB_COLOR_LINK")."<br> <b>@feed-add-post-form-link</b>" ?></label><br><input type="text" id="input-@feed-add-post-form-link" name="auth[input-@feed-add-post-form-link]" class="colorPicket" value="<?=$arOptions["auth"]["input-@feed-add-post-form-link"]?$arOptions["auth"]["input-@feed-add-post-form-link"]:"#80868e"?>" /><br>
                                    <label for="input-@feed-add-post-form-link-hover"><?= Loc::getMessage("SUB_COLOR_LINK_HOVER")."<br> <b>@feed-add-post-form-link-hover</b>" ?></label><br><input type="text" id="input-@feed-add-post-form-link-hover" name="auth[input-@feed-add-post-form-link-hover]" class="colorPicket" value="<?=$arOptions["auth"]["input-@feed-add-post-form-link-hover"]?$arOptions["auth"]["input-@feed-add-post-form-link-hover"]:"#000000"?>" />
                                    <label for="input-@feed-add-post-form-link-active"><?= Loc::getMessage("SUB_COLOR_LINK_ACTIVE")."<br> <b>@feed-add-post-form-link-active</b>" ?></label><br><input type="text" id="input-@feed-add-post-form-link-active" name="auth[input-@feed-add-post-form-link-active]" class="colorPicket" value="<?=$arOptions["auth"]["input-@feed-add-post-form-link-active"]?$arOptions["auth"]["input-@feed-add-post-form-link-active"]:"#185ccd"?>" />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="16.66666%" valign="top" align="left">
                        <table cellpadding="0" cellspacing="10">
                            <tr>
                                <td class="heading" >
                                    <?=Loc::getMessage("SUB_TAB1_DESC_4")?>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="left">
                                    <label for="input-@bx-im-informer-left-border"><?= Loc::getMessage("SUB_TAB1_COLOR_RMENU_BORDER")."<br> <b>@bx-im-informer-left-border</b>"  ?></label><br><input type="text" id="input-@bx-im-informer-left-border" name="auth[input-@bx-im-informer-left-border]" class="colorPicket" value="<?=$arOptions["auth"]["input-@bx-im-informer-left-border"]?$arOptions["auth"]["input-@bx-im-informer-left-border"]:"#848c95"?>" /><br>
                                    <label for="input-@help-block-icon"><?= Loc::getMessage("SUB_TAB1_COLOR_RMENU_ICON_COLOR")."<br> <b>@help-block-icon</b>"  ?></label><br><input type="text" id="input-@help-block-icon" name="auth[input-@help-block-icon]" class="colorPicket" value="<?=$arOptions["auth"]["input-@help-block-icon"]?$arOptions["auth"]["input-@help-block-icon"]:"#3c9dbf"?>" />
                                    <label for="input-@bx-im-informer-hover"><?= Loc::getMessage("SUB_TAB1_COLOR_RMENU_HOVER")."<br> <b>@bx-im-informer-hover</b>"  ?></label><br><input type="text" id="input-@bx-im-informer-hover" name="auth[input-@bx-im-informer-hover]" class="colorPicket" value="<?=$arOptions["auth"]["input-@bx-im-informer-hover"]?$arOptions["auth"]["input-@bx-im-informer-hover"]:"#e1f6fd"?>" />
                                    <label for="input-@bx-im-informer-bg"><?= Loc::getMessage("SUB_TAB1_COLOR_RMENU_BG")."<br> <b>@bx-im-informer-bg</b>"  ?></label><br><input type="text" id="input-@bx-im-informer-bg" name="auth[input-@bx-im-informer-bg]" class="colorPicket" value="<?=$arOptions["auth"]["input-@bx-im-informer-bg"]?$arOptions["auth"]["input-@bx-im-informer-bg"]:"#eef2f4"?>" />
                                    <label for="input-@bx-im-informer-bg-item-hover"><?= Loc::getMessage("SUB_TAB1_COLOR_RMENU_BG_ITEM_HOVER")."<br> <b>@bx-im-informer-bg-item-hover</b>"  ?></label><br><input type="text" id="input-@bx-im-informer-bg-item-hover" name="auth[input-@bx-im-informer-bg-item-hover]" class="colorPicket" value="<?=$arOptions["auth"]["input-@bx-im-informer-bg-item-hover"]?$arOptions["auth"]["input-@bx-im-informer-bg-item-hover"]:"#e1f6fd"?>" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            <?$subTabControl->BeginNextTab();?>
            <table cellpadding="0" cellspacing="10">
                <tr>
                    <td width="100%" valign="top" align="left">
                        <div class="adm-info-message-wrap">
                            <div class="adm-info-message">
                                <?$INSTRUCTION =  '/bitrix/css/' . $module_id . '/images/links.jpg';?>
                                <?=Loc::getMessage('SUB_TAB_DESC_PIC', array('#SUB_TAB_DESC_PIC#'=>$INSTRUCTION))?>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td width="100%" valign="top" align="left">
                        <table cellpadding="0" cellspacing="10">
                            <tr>
                                <td width="16.66666666%" valign="top">
                                    <table>
                                        <tr>
                                            <td class="heading">
                                                <?=Loc::getMessage("SUB_TAB2_DESC_1")?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="left">
                                                <label for="input-@all-link-block1-bl"><?= Loc::getMessage("SUB_COLOR_LINK_BLUE")."<br> <b>@all-link-block1-bl</b>" ?></label><br><input type="text" id="input-@all-link-block1-bl" name="auth[input-@all-link-block1-bl]" class="colorPicket" value="<?=$arOptions["auth"]["input-@all-link-block1-bl"]?$arOptions["auth"]["input-@all-link-block1-bl"]:"#80868e"?>" /><br>
                                                <label for="input-@all-link-block1-bl-hover"><?= Loc::getMessage("SUB_COLOR_LINK_BLUE_HOVER")."<br> <b>@all-link-block1-bl-hover</b>" ?></label><br><input type="text" id="input-@all-link-block1-bl-hover" name="auth[input-@all-link-block1-bl-hover]" class="colorPicket" value="<?=$arOptions["auth"]["input-@all-link-block1-bl-hover"]?$arOptions["auth"]["input-@all-link-block1-bl-hover"]:"#000000"?>" /><br>
                                                <label for="input-@all-link-block1-bl-active"><?= Loc::getMessage("SUB_COLOR_LINK_BLUE_ACTIVE")."<br> <b>@all-link-block1-bl-active</b>" ?></label><br><input type="text" id="input-@all-link-block1-bl-active" name="auth[input-@all-link-block1-bl-active]" class="colorPicket" value="<?=$arOptions["auth"]["input-@all-link-block1-bl-active"]?$arOptions["auth"]["input-@all-link-block1-bl-active"]:"#185ccd"?>" />
                                                <hr>
                                                <label for="input-@all-link-block1-gr"><?= Loc::getMessage("SUB_COLOR_LINK_GREY")."<br> <b>@all-link-block1-gr</b>" ?></label><br><input type="text" id="input-@all-link-block1-gr" name="auth[input-@all-link-block1-gr]" class="colorPicket" value="<?=$arOptions["auth"]["input-@all-link-block1-gr"]?$arOptions["auth"]["input-@all-link-block1-gr"]:"#a8aeb5"?>" /><br>
                                                <label for="input-@all-link-block1-gr-hover"><?= Loc::getMessage("SUB_COLOR_LINK_GREY_HOVER")."<br> <b>@all-link-block1-gr-hover</b>" ?></label><br><input type="text" id="input-@all-link-block1-gr-hover" name="auth[input-@all-link-block1-gr-hover]" class="colorPicket" value="<?=$arOptions["auth"]["input-@all-link-block1-gr-hover"]?$arOptions["auth"]["input-@all-link-block1-gr-hover"]:"#3a3d42"?>" /><br>
                                                <label for="input-@all-link-block1-gr-active"><?= Loc::getMessage("SUB_COLOR_LINK_GREY_ACTIVE")."<br> <b>@all-link-block1-gr-active</b>" ?></label><br><input type="text" id="input-@all-link-block1-gr-active" name="auth[input-@all-link-block1-gr-active]" class="colorPicket" value="<?=$arOptions["auth"]["input-@all-link-block1-gr-active"]?$arOptions["auth"]["input-@all-link-block1-gr-active"]:"#3a3d42"?>" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="16.66666666%" valign="top">
                                    <table>
                                        <tr>
                                            <td class="heading" >
                                                <?=Loc::getMessage("SUB_TAB2_DESC_2")?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="left">
                                                <label for="input-@all-link-block2"><?= Loc::getMessage("SUB_COLOR_LINK")."<br> <b>@all-link-block2</b>" ?></label><br><input type="text" id="input-@all-link-block2" name="auth[input-@all-link-block2]" class="colorPicket" value="<?=$arOptions["auth"]["input-@all-link-block2"]?$arOptions["auth"]["input-@all-link-block2"]:"#828b95"?>" /><br>
                                                <label for="input-@all-link-block2-hover"><?= Loc::getMessage("SUB_COLOR_LINK_HOVER")."<br> <b>@ll-link-block2-hover</b>" ?></label><br><input type="text" id="input-@all-link-block2-hover" name="auth[input-@all-link-block2-hover]" class="colorPicket" value="<?=$arOptions["auth"]["input-@all-link-block2-hover"]?$arOptions["auth"]["input-@all-link-block2-hover"]:"#3a3d42"?>" /><br>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="16.66666666%" valign="top">
                                    <table>
                                        <tr>
                                            <td class="heading" >
                                                <?=Loc::getMessage("SUB_TAB2_DESC_3")?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="left">
                                                <label for="input-@all-link-block3"><?= Loc::getMessage("SUB_COLOR_BTN")."<br> <b>@all-link-block3</b>" ?></label><br><input type="text" id="input-@all-link-block3" name="auth[input-@all-link-block3]" class="colorPicket" value="<?=$arOptions["auth"]["input-@all-link-block3"]?$arOptions["auth"]["input-@all-link-block3"]:"#bbed21"?>" /><br>
                                                <label for="input-@all-link-block3-hover"><?= Loc::getMessage("SUB_COLOR_BTN_HOVER")."<br> <b>@all-link-block3-hover</b>" ?></label><br><input type="text" id="input-@all-link-block3-hover" name="auth[input-@all-link-block3-hover]" class="colorPicket" value="<?=$arOptions["auth"]["input-@all-link-block3-hover"]?$arOptions["auth"]["input-@all-link-block3-hover"]:"#d2f95f"?>" /><br>
                                                <label for="input-@all-link-block3-active"><?= Loc::getMessage("SUB_COLOR_BTN_ACTIVE")."<br> <b>@all-link-block3-active</b>" ?></label><br><input type="text" id="input-@all-link-block3-active" name="auth[input-@all-link-block3-active]" class="colorPicket" value="<?=$arOptions["auth"]["input-@all-link-block3-active"]?$arOptions["auth"]["input-@all-link-block3-active"]:"#b2e233"?>" /><br>
                                                <label for="input-@all-link-block3-text"><?= Loc::getMessage("SUB_COLOR_BTN_TEXT")."<br> <b>@all-link-block3-text</b>" ?></label><br><input type="text" id="input-@all-link-block3-text" name="auth[input-@all-link-block3-text]" class="colorPicket" value="<?=$arOptions["auth"]["input-@all-link-block3-text"]?$arOptions["auth"]["input-@all-link-block3-text"]:"#535c69"?>" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="16.66666666%" valign="top">
                                    <table>
                                        <tr>
                                            <td class="heading">
                                                <?=Loc::getMessage("SUB_TAB2_DESC_4")?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="left" >
                                                <label for="input-@all-link-block4"><?= Loc::getMessage("SUB_COLOR_BTN")."<br> <b>@all-link-block4</b>" ?></label><br><input type="text" id="input-@all-link-block4" name="auth[input-@all-link-block4]" class="colorPicket" value="<?=$arOptions["auth"]["input-@all-link-block4"]?$arOptions["auth"]["input-@all-link-block4"]:"#eef2f4"?>" /><br>
                                                <label for="input-@all-link-block4-hover"><?= Loc::getMessage("SUB_COLOR_BTN_HOVER")."<br> <b>@all-link-block4-hover</b>" ?></label><br><input type="text" id="input-@all-link-block4-hover" name="auth[input-@all-link-block4-hover]" class="colorPicket" value="<?=$arOptions["auth"]["input-@all-link-block4-hover"]?$arOptions["auth"]["input-@all-link-block4-hover"]:"#cfd4d8"?>" /><br>
                                                <label for="input-@all-link-block4-active"><?= Loc::getMessage("SUB_COLOR_BTN_ACTIVE")."<br> <b>@all-link-block4-active</b>" ?></label><br><input type="text" id="input-@all-link-block4-active" name="auth[input-@all-link-block4-active]" class="colorPicket" value="<?=$arOptions["auth"]["input-@all-link-block4-active"]?$arOptions["auth"]["input-@all-link-block4-active"]:"#dde2e5"?>" /><br>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="16.66666666%" valign="top">
                                    <table>
                                        <tr>
                                            <td class="heading" >
                                                <?=Loc::getMessage("SUB_TAB2_DESC_5")?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="left">
                                                <label for="input-@all-link-block5"><?= Loc::getMessage("SUB_COLOR_BTN")."<br> <b>@all-link-block5</b>" ?></label><br><input type="text" id="input-@all-link-block5" name="auth[input-@all-link-block5]" class="colorPicket" value="<?=$arOptions["auth"]["input-@all-link-block5"]?$arOptions["auth"]["input-@all-link-block5"]:"#3bc8f5"?>" /><br>
                                                <label for="input-@all-link-block5-hover"><?= Loc::getMessage("SUB_COLOR_BTN_HOVER")."<br> <b>@all-link-block5-hover</b>" ?></label><br><input type="text" id="input-@all-link-block5-hover" name="auth[input-@all-link-block5-hover]" class="colorPicket" value="<?=$arOptions["auth"]["input-@all-link-block5-hover"]?$arOptions["auth"]["input-@all-link-block5-hover"]:"#3eddff"?>" /><br>
                                                <label for="input-@all-link-block5-active"><?= Loc::getMessage("SUB_COLOR_BTN_ACTIVE")."<br> <b>@all-link-block5-active</b>" ?></label><br><input type="text" id="input-@all-link-block5-active" name="auth[input-@all-link-block5-active]" class="colorPicket" value="<?=$arOptions["auth"]["input-@all-link-block5-active"]?$arOptions["auth"]["input-@all-link-block5-active"]:"#12b1e3"?>" /><br>
                                                <label for="input-@all-link-block5-text"><?= Loc::getMessage("SUB_COLOR_BTN_TEXT")."<br> <b>@all-link-block5-text</b>" ?></label><br><input type="text" id="input-@all-link-block5-text" name="auth[input-@all-link-block5-text]" class="colorPicket" value="<?=$arOptions["auth"]["input-@all-link-block5-text"]?$arOptions["auth"]["input-@all-link-block5-text"]:"#ffffff"?>" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="16.66666666%" valign="top">
                                    <table>
                                        <tr>
                                            <td class="heading" >
                                                <?=Loc::getMessage("SUB_TAB2_DESC_6")?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="left">
                                                <label for="input-@all-link-block6"><?= Loc::getMessage("SUB_COLOR_BTN")."<br> <b>@all-link-block6</b>" ?></label><br><input type="text" id="input-@all-link-block6" name="auth[input-@all-link-block6]" class="colorPicket" value="<?=$arOptions["auth"]["input-@all-link-block6"]?$arOptions["auth"]["input-@all-link-block6"]:"#ecedef"?>" /><br>
                                                <label for="input-@all-link-block6-hover"><?= Loc::getMessage("SUB_COLOR_BTN_HOVER")."<br> <b>@all-link-block6-hover</b>" ?></label><br><input type="text" id="input-@all-link-block6-hover" name="auth[input-@all-link-block6-hover]" class="colorPicket" value="<?=$arOptions["auth"]["input-@all-link-block6-hover"]?$arOptions["auth"]["input-@all-link-block6-hover"]:"#cfd4d8"?>" /><br>
                                                <label for="input-@all-link-block6-active"><?= Loc::getMessage("SUB_COLOR_BTN_ACTIVE")."<br> <b>@all-link-block6-active</b>" ?></label><br><input type="text" id="input-@all-link-block6-active" name="auth[input-@all-link-block6-active]" class="colorPicket" value="<?=$arOptions["auth"]["input-@all-link-block6-active"]?$arOptions["auth"]["input-@all-link-block6-active"]:"#d2d3d8"?>" /><br>
                                                <label for="input-@all-link-block6-text"><?= Loc::getMessage("SUB_COLOR_BTN_TEXT")."<br> <b>@all-link-block6-text</b>" ?></label><br><input type="text" id="input-@all-link-block6-text" name="auth[input-@all-link-block6-text]" class="colorPicket" value="<?=$arOptions["auth"]["input-@all-link-block6-text"]?$arOptions["auth"]["input-@all-link-block6-text"]:"#7a818a"?>" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <?$subTabControl->BeginNextTab();?>
            <table cellpadding="0" cellspacing="10">
                <tr>
                    <td width="50%" valign="top" align="left">
                        <div class="adm-info-message-wrap">
                            <div class="adm-info-message">
                                <?$INSTRUCTION =  '/bitrix/css/' . $module_id . '/images/forms.jpg';?>
                                <?=Loc::getMessage('SUB_TAB3_NOTICE')?><br>
                                <?=Loc::getMessage('SUB_TAB_DESC_PIC', array('#SUB_TAB_DESC_PIC#'=>$INSTRUCTION))?>
                            </div>
                        </div>
                    </td>
                    <td width="50%" valign="top" align="left">
                        <table cellpadding="0" cellspacing="10">
                            <tr>
                                <td valign="top" align="left">
                                    <label for="input-@feed-add-post-destination-users"><?= Loc::getMessage("SUB_TAB3_COLOR_LABEL_FORM")."<br> <b>@feed-add-post-destination-users</b>" ?></label><br><input type="text" id="input-@feed-add-post-destination-users" name="auth[input-@feed-add-post-destination-users]" class="colorPicket" value="<?=$arOptions["auth"]["input-@feed-add-post-destination-users"]?$arOptions["auth"]["input-@feed-add-post-destination-users"]:"#bcedfc"?>" /><br>
                                    <label for="input-@feed-add-post-destination-all-users"><?= Loc::getMessage("SUB_TAB3_COLOR_LABEL_FORM2")."<br> <b>@feed-add-post-destination-all-users</b>" ?></label><br><input type="text" id="input-@feed-add-post-destination-all-users" name="auth[input-@feed-add-post-destination-all-users]" class="colorPicket" value="<?=$arOptions["auth"]["input-@feed-add-post-destination-all-users"]?$arOptions["auth"]["input-@feed-add-post-destination-all-users"]:"#dbf188"?>" /><br>
                                    <label for="input-@color-radiobutton"><?= Loc::getMessage("SUB_TAB3_COLOR_RADIOBUTTON")."<br> <b>@color-radiobutton</b>" ?></label><br><input type="text" id="input-@color-radiobutton" name="auth[input-@color-radiobutton]" class="colorPicket" value="<?=$arOptions["auth"]["input-@color-radiobutton"]?$arOptions["auth"]["input-@color-radiobutton"]:"#000000"?>" /><br>
                                </td>
                                <td valign="top" align="left">
                                    <label><?= Loc::getMessage("SUB_TAB3_COLOR_ICON_CHECKBOX")."<br> <b>@icon-checkbox</b>" ?></label><br> <?=CFile::InputFile("file-@icon-checkbox", 20, ($arOptions["files"]["file-@icon-checkbox"])?$arOptions["files"]["file-@icon-checkbox"]:$str_IMAGE_ID);?>
                                    <?
                                    echo "<br>". Loc::getMessage("SUB_TAB5_DEFAULT"). ":<br>". CFile::ShowImage("/bitrix/components/bitrix/voting.uf/templates/.default/images/voting-tick.png", 100, 200, "border=1", "", true);
                                    ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <?$subTabControl->BeginNextTab();?>
            <table cellpadding="20" cellspacing="20" width="100%">
                <tr>
                    <td width="60%" valign="top">
                        <div class="adm-info-message-wrap">
                            <div class="adm-info-message">
                                <?$INSTRUCTION =  '/bitrix/css/' . $module_id . '/images/content.jpg';?>
                                <?=Loc::getMessage('SUB_TAB_DESC_PIC', array('#SUB_TAB_DESC_PIC#'=>$INSTRUCTION))?>
                            </div>
                        </div>
                    </td>
                    <td width="20%" valign="top" align="left">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td valign="top" align="left" class="heading">
                                    <?=Loc::getMessage("SUB_TAB4_DESC_1")?>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="left">
                                    <label for="input-@fon-citata1"><?= Loc::getMessage("SUB_TAB4_COLOR_FON_BG_1")."<br> <b>@fon-citata1</b>" ?></label><br><input type="text" id="input-@fon-citata1" name="auth[input-@fon-citata1]" class="colorPicket" value="<?=$arOptions["auth"]["input-@fon-citata1"]?$arOptions["auth"]["input-@fon-citata1"]:"#ffefd0"?>" /><br>
                                    <label for="input-@fon-citata2"><?= Loc::getMessage("SUB_TAB4_COLOR_FON_BG_2")."<br> <b>@fon-citata2</b>" ?></label><br><input type="text" id="input-@fon-citata2" name="auth[input-@fon-citata2]" class="colorPicket" value="<?=$arOptions["auth"]["input-@fon-citata2"]?$arOptions["auth"]["input-@fon-citata2"]:"#f4fed5"?>" /><br>
                                    <label for="input-@fon-citata3"><?= Loc::getMessage("SUB_TAB4_COLOR_FON_BG_3")."<br> <b>@fon-citata3</b>" ?></label><br><input type="text" id="input-@fon-citata3" name="auth[input-@fon-citata3]" class="colorPicket" value="<?=$arOptions["auth"]["input-@fon-citata3"]?$arOptions["auth"]["input-@fon-citata3"]:"#dfeef5"?>" /><br>
                                    <label for="input-@fon-citata-text"><?= Loc::getMessage("SUB_TAB4_COLOR_FON_CITATA_TEXT")."<br> <b>@fon-citata-text</b>" ?></label><br><input type="text" id="input-@fon-citata-text" name="auth[input-@fon-citata-text]" class="colorPicket" value="<?=$arOptions["auth"]["input-@fon-citata-text"]?$arOptions["auth"]["input-@fon-citata-text"]:"#010101"?>" />
                                </td>
                            </tr>

                        </table>
                    </td>
                    <td width="20%" valign="top" align="left">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td valign="top" align="left" class="heading">
                                    <?=Loc::getMessage("SUB_TAB4_DESC_2")?>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="left"><label for="input-@table-active-col"><?= Loc::getMessage("SUB_TAB4_COLOR_TABLE_ACTIVE_COLUM")."<br> <b>@table-active-col</b>" ?></label><br><input type="text" id="input-@table-active-col" name="auth[input-@table-active-col]" class="colorPicket" value="<?=$arOptions["auth"]["input-@table-active-col"]?$arOptions["auth"]["input-@table-active-col"]:"#c5e0ed"?>" /></td>
                            </tr>
                            <tr>
                                <td valign="top" align="left"><label for="input-@table-first-col"><?= Loc::getMessage("SUB_TAB4_COLOR_TABLE_FIRST_COLUM")."<br> <b>@table-first-col</b>" ?></label><br><input type="text" id="input-@table-first-col" name="auth[input-@table-first-col]" class="colorPicket" value="<?=$arOptions["auth"]["input-@table-first-col"]?$arOptions["auth"]["input-@table-first-col"]:"#dfeef5"?>" /></td>
                            </tr>
                            <tr>
                                <td valign="top" align="left"><label for="input-@table-first-col2"><?= Loc::getMessage("SUB_TAB4_COLOR_TABLE_FIRST_COLUM2")."<br> <b>@table-first-col2</b>" ?></label><br><input type="text" id="input-@table-first-col2" name="auth[input-@table-first-col2]" class="colorPicket" value="<?=$arOptions["auth"]["input-@table-first-col2"]?$arOptions["auth"]["input-@table-first-col2"]:"#b5e1f1"?>" /></td>
                            </tr>
                            <tr>
                                <td valign="top" align="left"><label for="input-@table-event-col"><?= Loc::getMessage("SUB_TAB4_COLOR_TABLE_EVEN_COLUM")."<br> <b>@table-event-col</b>" ?></label><br><input type="text" id="input-@table-event-col" name="auth[input-@table-event-col]" class="colorPicket" value="<?=$arOptions["auth"]["input-@table-event-col"]?$arOptions["auth"]["input-@table-event-col"]:"#f6f8f9"?>" /></td>
                            </tr>
                            <tr>
                                <td valign="top" align="left"><label for="input-@table-not-event-col"><?= Loc::getMessage("SUB_TAB4_COLOR_TABLE_NOT_EVEN_COLUM")."<br> <b>@table-not-event-col</b>" ?></label><br><input type="text" id="input-@table-not-event-col" name="auth[input-@table-not-event-col]" class="colorPicket" value="<?=$arOptions["auth"]["input-@table-not-event-col"]?$arOptions["auth"]["input-@table-not-event-col"]:"#ebf2f5"?>" /></td>
                            </tr>
                            <tr>
                                <td valign="top" align="left"><label for="input-@table-border-color"><?= Loc::getMessage("SUB_TAB4_COLOR_TABLE_BORDER_COLOR")."<br> <b>@table-border-color</b>" ?></label><br><input type="text" id="input-@table-border-color" name="auth[input-@table-border-color]" class="colorPicket" value="<?=$arOptions["auth"]["input-@table-border-color"]?$arOptions["auth"]["input-@table-border-color"]:"#808080"?>" /></td>
                            </tr>
                        </table>
                    </td>
                    <td width="20%" valign="top" align="left">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td valign="top" align="left" class="heading">
                                    <?=Loc::getMessage("SUB_TAB4_DESC_3")?>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="left"><label for="input-@feed-user-avatar"><?= Loc::getMessage("SUB_TAB4_COLOR_USER_AVATAR")."<br> <b>@feed-user-avatar</b>" ?></label><br><input type="text" id="svg-@feed-user-avatar" name="auth[svg-@feed-user-avatar]" class="colorPicket" value="<?=$arOptions["auth"]["svg-@feed-user-avatar"]?$arOptions["auth"]["svg-@feed-user-avatar"]:"#535c6a"?>" /></td>
                            </tr>
                            <tr>
                                <td valign="top" align="left">
                                    <label for="svg-@feed-post-emoji-icon-like"><?= Loc::getMessage("SUB_TAB6_IMG_LIKE")."<br> <b>@feed-post-emoji-icon-like</b>" ?></label><br><input type="text" id="svg-@feed-post-emoji-icon-like" name="auth[svg-@feed-post-emoji-icon-like]" class="colorPicket" value="<?=$arOptions["auth"]["svg-@feed-post-emoji-icon-like"]?$arOptions["auth"]["svg-@feed-post-emoji-icon-like"]:"#62a9f3"?>" /><br>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="left">
                                    <label for="input-@bx-user-subtitle"><?= Loc::getMessage("SUB_TAB4_COLOR_BIRTHDAY_PAGE")."<br> <b>@bx-user-subtitle</b>" ?></label><br><input type="text" id="input-@bx-user-subtitle" name="auth[input-@bx-user-subtitle]" class="colorPicket" value="<?=$arOptions["auth"]["input-@bx-user-subtitle"]?$arOptions["auth"]["input-@bx-user-subtitle"]:"#FFFFDA"?>" /><br>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top" align="left" colspan="4">
                        <label for="input-@border-citata"><?= Loc::getMessage("SUB_TAB4_COLOR_CITATA_BG_4")."<br> <b>@border-citata</b>" ?></label><br><input type="text" id="input-@border-citata" name="auth[input-@border-citata]" class="colorPicket" value="<?=$arOptions["auth"]["input-@border-citata"]?$arOptions["auth"]["input-@border-citata"]:"#edede6"?>" /><br>
                        <label for="svg-@fon-citata4-ico"><?= Loc::getMessage("SUB_TAB4_COLOR_CITATA_ICON_4")."<br> <b>@fon-citata4-ico</b>" ?></label><br><input type="text" id="svg-@fon-citata4-ico" name="auth[svg-@fon-citata4-ico]" class="colorPicket" value="<?=$arOptions["auth"]["svg-@fon-citata4-ico"]?$arOptions["auth"]["svg-@fon-citata4-ico"]:"#525C69"?>" /><br>
                    </td>
                </tr>
            </table>
            <?$subTabControl->BeginNextTab();?>
            <table cellpadding="20" cellspacing="20" width="100%">
                <tr>
                    <td valign="top" align="left">
                        <label><?= Loc::getMessage("SUB_TAB5_TIMEMAN")."<br> <b>@tm-sprite</b>" ?></label><br>  <?=CFile::InputFile("file-@tm-sprite", 20, ($arOptions["files"]["file-@tm-sprite"])?$arOptions["files"]["file-@tm-sprite"]:$str_IMAGE_ID);?>
                        <?
                        echo "<br>". Loc::getMessage("SUB_TAB5_DEFAULT"). ":<br>". CFile::ShowImage("/bitrix/templates/bitrix24/images/interface/timeman/tm-sprite.png", 100, 200, "border=1", "", true);
                        ?>
                    </td>
                    <td valign="top" align="left">
                        <label><?= Loc::getMessage("SUB_TAB5_IMP")."<br> <b>@imp-massage-arrows</b>" ?></label><br>  <?=CFile::InputFile("file-@imp-massage-arrows", 20, ($arOptions["files"]["file-@imp-massage-arrows"])?$arOptions["files"]["file-@imp-massage-arrows"]:$str_IMAGE_ID);?>
                        <?
                        echo "<br>". Loc::getMessage("SUB_TAB5_DEFAULT"). ":<br>". CFile::ShowImage("/bitrix/templates/bitrix24/images/imp-massage-arrows.png", 100, 200, "border=1", "", true);
                        ?>
                    </td>
                    <td valign="top" align="left">
                        <label><?= Loc::getMessage("SUB_TAB5_FILES")."<br> <b>@icons-files2</b>" ?></label><br>  <?=CFile::InputFile("file-@icons-files2", 20, ($arOptions["files"]["file-@icons-files2"])?$arOptions["files"]["file-@icons-files2"]:$str_IMAGE_ID);?>
                        <?
                        echo "<br>". Loc::getMessage("SUB_TAB5_DEFAULT"). ":<br>". CFile::ShowImage("/bitrix/images/disk/icons-files2.png", 100, 200, "border=1", "", true);
                        ?>
                    </td>
                    <td valign="top" align="left">
                        <label><?= Loc::getMessage("SUB_TAB5_BP")."<br> <b>@bp-sprite</b>" ?></label><br>  <?=CFile::InputFile("file-@bp-sprite", 20, ($arOptions["files"]["file-@bp-sprite"])?$arOptions["files"]["file-@bp-sprite"]:$str_IMAGE_ID);?>
                        <?
                        echo "<br>". Loc::getMessage("SUB_TAB5_DEFAULT"). ":<br>". CFile::ShowImage("/bitrix/components/bitrix/bizproc.workflow.faces/templates/.default/images/bp-sprite.png", 100, 200, "border=1", "", true);
                        ?>
                    </td>
                    <td valign="top" align="left">
                        <label><?= Loc::getMessage("SUB_TAB5_LF")."<br> <b>@live_feed_sprite_6</b>" ?></label><br>  <?=CFile::InputFile("file-@live_feed_sprite_6", 20, ($arOptions["files"]["file-@live_feed_sprite_6"])?$arOptions["files"]["file-@live_feed_sprite_6"]:$str_IMAGE_ID);?>
                        <?
                        echo "<br>". Loc::getMessage("SUB_TAB5_DEFAULT"). ":<br>". CFile::ShowImage("/bitrix/components/bitrix/socialnetwork.log.ex/templates/.default/images/log/live_feed_sprite_6.png", 100, 200, "border=1", "", true);
                        ?>
                    </td>
                    <td valign="top" align="left">
                        <label><?= Loc::getMessage("SUB_TAB5_LF")."<br> <b>@buttons-sprite</b>" ?></label><br>  <?=CFile::InputFile("file-@buttons-sprite", 20, ($arOptions["files"]["file-@buttons-sprite"])?$arOptions["files"]["file-@buttons-sprite"]:$str_IMAGE_ID);?>
                        <?
                        echo "<br>". Loc::getMessage("SUB_TAB5_DEFAULT"). ":<br>". CFile::ShowImage("/bitrix/templates/bitrix24/images/interface/buttons-sprite.png", 100, 200, "border=1", "", true);
                        ?>
                    </td>
                </tr>
                <tr>
                    <td valign="top" align="left">
                        <label><?= Loc::getMessage("SUB_TAB5_CS")."<br> <b>@calendar-sprite</b>" ?></label><br>  <?=CFile::InputFile("file-@calendar-sprite", 20, ($arOptions["files"]["file-@calendar-sprite"])?$arOptions["files"]["file-@calendar-sprite"]:$str_IMAGE_ID);?>
                        <?
                        echo "<br>". Loc::getMessage("SUB_TAB5_DEFAULT"). ":<br>". CFile::ShowImage("/bitrix/images/calendar/calendar-sprite.png", 100, 200, "border=1", "", true);
                        ?>
                    </td>
                </tr>
            </table>
            <?$subTabControl->BeginNextTab();?>
            <table cellpadding="0" cellspacing="10">
                <tr>
                    <td align="center">
                        <div class="adm-info-message-wrap">
                            <div class="adm-info-message">
                                <?$INSTRUCTION =  '/bitrix/css/' . $module_id . '/images/other.jpg';?>
                                <?=Loc::getMessage('SUB_TAB_DESC_PIC', array('#SUB_TAB_DESC_PIC#'=>$INSTRUCTION))?>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="32.3333333%" align="left" valign="top">
                                    <table cellpadding="0" cellspacing="10" >
                                        <tr class="heading">
                                            <td colspan="2"><?=Loc::getMessage("SUB_TAB6_DESC_2")?></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="left">
                                                <label for="input-@sidebar-imp-mess-top"><?= Loc::getMessage("SUB_TAB6_COLOR_IMPORTANT_MESSAGES")."<br> <b>@sidebar-imp-mess-top</b>" ?></label><br><input type="text" id="input-@sidebar-imp-mess-top" name="auth[input-@sidebar-imp-mess-top]" class="colorPicket" value="<?=$arOptions["auth"]["input-@sidebar-imp-mess-top"]?$arOptions["auth"]["input-@sidebar-imp-mess-top"]:"#b36530"?>" /><br>
                                                <label for="input-@sidebar-imp-mess"><?= Loc::getMessage("SUB_TAB6_COLOR_IMPORTANT_MESSAGES_FON")."<br> <b>@sidebar-imp-mess</b>" ?></label><br><input type="text" id="input-@sidebar-imp-mess" name="auth[input-@sidebar-imp-mess]" class="colorPicket" value="<?=$arOptions["auth"]["input-@sidebar-imp-mess"]?$arOptions["auth"]["input-@sidebar-imp-mess"]:"#fff3a7"?>" /><br>
                                                <label for="input-@sidebar-imp-mess-btn"><?= Loc::getMessage("SUB_TAB6_COLOR_IMPORTANT_MESSAGES_BTN")."<br> <b>@sidebar-imp-mess-btn</b>" ?></label><br><input type="text" id="input-@sidebar-imp-mess-btn" name="auth[input-@sidebar-imp-mess-btn]" class="colorPicket" value="<?=$arOptions["auth"]["input-@sidebar-imp-mess-btn"]?$arOptions["auth"]["input-@sidebar-imp-mess-btn"]:"#d0c157"?>" /><br>
                                                <label for="input-@sidebar-imp-mess-btn-hover"><?= Loc::getMessage("SUB_TAB6_COLOR_IMPORTANT_MESSAGES_BTN_HOVER")."<br> <b>@sidebar-imp-mess-btn-hover</b>" ?></label><br><input type="text" id="input-@sidebar-imp-mess-btn-hover" name="auth[input-@sidebar-imp-mess-btn-hover]" class="colorPicket" value="<?=$arOptions["auth"]["input-@sidebar-imp-mess-btn-hover"]?$arOptions["auth"]["input-@sidebar-imp-mess-btn-hover"]:"#e29706"?>" /><br>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="32.3333333%" align="left" valign="top">
                                    <table cellpadding="0" cellspacing="10" >
                                        <tr class="heading">
                                            <td colspan="2"><?=Loc::getMessage("SUB_TAB6_DESC_3")?></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="left">
                                                <label for="input-@tasks-sidebar-widget-top"><?= Loc::getMessage("SUB_TAB6_COLOR_MY_TASKS")."<br> <b>@tasks-sidebar-widget-top</b>" ?></label><br><input type="text" id="input-@tasks-sidebar-widget-top" name="auth[input-@tasks-sidebar-widget-top]" class="colorPicket" value="<?=$arOptions["auth"]["input-@tasks-sidebar-widget-top"]?$arOptions["auth"]["input-@tasks-sidebar-widget-top"]:"#2fc7f7"?>" /><br>
                                                <label for="input-@task-item-hover"><?= Loc::getMessage("SUB_TAB6_COLOR_MY_TASKS_PL_HOVER")."<br> <b>@task-item-hover</b>" ?></label><br><input type="text" id="input-@task-item-hover" name="auth[input-@task-item-hover]" class="colorPicket" value="<?=$arOptions["auth"]["input-@task-item-hover"]?$arOptions["auth"]["input-@task-item-hover"]:"#f8fafb"?>" /><br>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="32.3333333%" align="left" valign="top">
                                    <table cellpadding="0" cellspacing="10" >
                                        <tr class="heading">
                                            <td colspan="2"><?=Loc::getMessage("SUB_TAB6_DESC_4")?></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="left">
                                                <label for="input-@widget-birthdays"><?= Loc::getMessage("SUB_TAB6_COLOR_MY_TASKS")."<br> <b>@widget-birthdays</b>" ?></label><br><input type="text" id="input-@widget-birthdays" name="auth[input-@widget-birthdays]" class="colorPicket" value="<?=$arOptions["auth"]["input-@widget-birthdays"]?$arOptions["auth"]["input-@widget-birthdays"]:"#f8a70b"?>" /><br>
                                                <label for="input-@widget-birthdays-item-hover"><?= Loc::getMessage("SUB_TAB6_COLOR_MY_TASKS_PL_HOVER")."<br> <b>@widget-birthdays-item-hover</b>" ?></label><br><input type="text" id="input-@widget-birthdays-item-hover" name="auth[input-@widget-birthdays-item-hover]" class="colorPicket" value="<?=$arOptions["auth"]["input-@widget-birthdays-item-hover"]?$arOptions["auth"]["input-@widget-birthdays-item-hover"]:"#f8fafb"?>" /><br>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="32.3333333%" align="left" valign="top">
                                    <table cellpadding="0" cellspacing="10" >
                                        <tr class="heading">
                                            <td colspan="2"><?=Loc::getMessage("SUB_TAB6_DESC_5")?></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="left">
                                                <label for="input-@sidebar-widget-bp"><?= Loc::getMessage("SUB_TAB6_COLOR_MY_TASKS")."<br> <b>@sidebar-widget-bp</b>" ?></label><br><input type="text" id="input-@sidebar-widget-bp" name="auth[input-@sidebar-widget-bp]" class="colorPicket" value="<?=$arOptions["auth"]["input-@sidebar-widget-bp"]?$arOptions["auth"]["input-@sidebar-widget-bp"]:"#0db7ad"?>" /><br>
                                                <label for="input-@sidebar-widget-bp-item-hover"><?= Loc::getMessage("SUB_TAB6_COLOR_MY_TASKS_PL_HOVER")."<br> <b>@sidebar-widget-bp-item-hover</b>" ?></label><br><input type="text" id="input-@sidebar-widget-bp-item-hover" name="auth[input-@sidebar-widget-bp-item-hover]" class="colorPicket" value="<?=$arOptions["auth"]["input-@sidebar-widget-bp-item-hover"]?$arOptions["auth"]["input-@sidebar-widget-bp-item-hover"]:"#dfeef5"?>" /><br>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="32.3333333%" align="left" valign="top">
                                    <table cellpadding="0" cellspacing="10" >
                                        <tr class="heading">
                                            <td colspan="2"><?=Loc::getMessage("SUB_TAB6_DESC_6")?></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="left">
                                                <label for="svg-@ui-icon-folder-header"><?= Loc::getMessage("SUB_TAB6_DISK_COLOR_FOLDER_HEADER")."<br> <b>@ui-icon-folder-header</b>" ?></label><br><input type="text" id="svg-@ui-icon-folder-header" name="auth[svg-@ui-icon-folder-header]" class="colorPicket" value="<?=$arOptions["auth"]["svg-@ui-icon-folder-header"]?$arOptions["auth"]["svg-@ui-icon-folder-header"]:"#2FC7F7"?>" /><br>
                                                <label for="svg-@ui-icon-folder-body"><?= Loc::getMessage("SUB_TAB6_DISK_COLOR_FOLDER_BODY")."<br> <b>@ui-icon-folder-body</b>" ?></label><br><input type="text" id="svg-@ui-icon-folder-body" name="auth[svg-@ui-icon-folder-body]" class="colorPicket" value="<?=$arOptions["auth"]["svg-@ui-icon-folder-body"]?$arOptions["auth"]["svg-@ui-icon-folder-body"]:"#86E0FB"?>" /><br>
                                                <label for="input-@ui-icon-file-folder-active"><?= Loc::getMessage("SUB_TAB6_DISK_COLOR_ACTIVE")."<br> <b>@ui-icon-file-folder-active</b>" ?></label><br><input type="text" id="input-@ui-icon-file-folder-active" name="auth[input-@ui-icon-file-folder-active]" value="<?=$arOptions["auth"]["input-@ui-icon-file-folder-active"]?$arOptions["auth"]["input-@ui-icon-file-folder-active"]:"rgba(47,198,246,.35)"?>" /><br>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td width="32.3333333%" align="left" valign="top">
                                    <table cellpadding="0" cellspacing="10" >
                                        <tr class="heading">
                                            <td colspan="2"><?=Loc::getMessage("SUB_TAB6_DESC_7")?></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="left">
                                                <label for="input-@task-detail-sidebar-status"><?= Loc::getMessage("SUB_TAB6_TASK_PAGE")."<br> <b>@task-detail-sidebar-status</b>" ?></label><br><input type="text" id="input-@task-detail-sidebar-status" name="auth[input-@task-detail-sidebar-status]" class="colorPicket" value="<?=$arOptions["auth"]["input-@task-detail-sidebar-status"]?$arOptions["auth"]["input-@task-detail-sidebar-status"]:"#56d1e0"?>" /><br>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="32.3333333%" align="left" valign="top">
                                    <table cellpadding="0" cellspacing="10" >
                                        <tr class="heading">
                                            <td colspan="2"><?=Loc::getMessage("SUB_TAB6_DESC_8")?></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="left">
                                                <label for="input-@sidebar-widget-calendar"><?= Loc::getMessage("SUB_TAB6_COLOR_MY_TASKS")."<br> <b>@sidebar-widget-calendar</b>" ?></label><br><input type="text" id="input-@sidebar-widget-calendar" name="auth[input-@sidebar-widget-calendar]" class="colorPicket" value="<?=$arOptions["auth"]["input-@sidebar-widget-calendar"]?$arOptions["auth"]["input-@sidebar-widget-calendar"]:"#f8a70b"?>" /><br>
                                                <label for="input-@calendar-item-icon"><?= Loc::getMessage("SUB_TAB6_COLOR_CALENDAR_ICON")."<br> <b>@calendar-item-icon</b>" ?></label><br><input type="text" id="input-@calendar-item-icon" name="auth[input-@calendar-item-icon]" class="colorPicket" value="<?=$arOptions["auth"]["input-@calendar-item-icon"]?$arOptions["auth"]["input-@calendar-item-icon"]:"#2fc7f7"?>" /><br>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="32.3333333%" align="left" valign="top">
                                    <table cellpadding="0" cellspacing="10" >
                                        <tr class="heading">
                                            <td colspan="2"><?=Loc::getMessage("SUB_TAB6_DESC_9")?></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="left">
                                                <label for="input-@sidebar-pulse-block-color"><?= Loc::getMessage("SUB_TAB6_COLOR_PULS")."<br> <b>@sidebar-pulse-block-color</b>" ?></label><br><input type="text" id="input-@sidebar-pulse-block-color" name="auth[input-@sidebar-pulse-block-color]" class="colorPicket" value="<?=$arOptions["auth"]["input-@sidebar-pulse-block-color"]?$arOptions["auth"]["input-@sidebar-pulse-block-color"]:"#2fc7f7"?>" /><br>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="32.3333333%" align="left" valign="top">
                                    <table cellpadding="0" cellspacing="10" >
                                        <tr class="heading">
                                            <td colspan="2"><?=Loc::getMessage("SUB_TAB6_DESC_10")?></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="left">
                                                <label for="input-@sonet-groups-sidebar-title"><?= Loc::getMessage("SUB_TAB6_COLOR_MY_TASKS")."<br> <b>@sonet-groups-sidebar-title</b>" ?></label><br><input type="text" id="input-@sonet-groups-sidebar-title" name="auth[input-@sonet-groups-sidebar-title]" class="colorPicket" value="<?=$arOptions["auth"]["input-@sonet-groups-sidebar-title"]?$arOptions["auth"]["input-@sonet-groups-sidebar-title"]:"#d5f1fc"?>" /><br>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <?$subTabControl->End();?>
        </td>
    </tr>
    <?
    $tabControl->EndTab();

    //dop styles
    $tabControl->BeginNextTab();

    $codeEditorId = CCodeEditor::Show(
        array(
            'textareaId' => 'bxed_CONTENT',
            'height' => 300,
            'width' => 1000
        ));
    ?>
    <tr>
        <td align="left">
            <textarea rows="28" cols="60" style="width:100%" id="bxed_CONTENT" name="CONTENT" wrap="off"><?echo htmlspecialcharsbx(htmlspecialcharsback($ChangeTheme->getDopStyles()))?></textarea>
        </td>
        <td>
            <div class="adm-info-message-wrap">
                <div class="adm-info-message">
                    <?=Loc::getMessage('MAIN_TAB_STYLES_DESC2')?>
                </div>
            </div>
        </td>
    </tr>
    <?$tabControl->EndTab();

    //settings
    $tabControl->BeginNextTab();

    ?>
    <tr class="heading">
        <td colspan="2"><?=Loc::getMessage("MAIN_TAB_SETTINGS_DEFAULT")?></td>
    </tr>
    <tr>
        <td width="40%"><?echo Loc::getMessage("TEMPLATE_MODULE_ADMIN")?></td>
        <td width="60%">
            <input type="checkbox" name="module_admin" value="Y" <?=$arOptions["module_admin"] == "Y" ? 'checked="checked"':''?>>
        </td>
    </tr>
    <tr>
        <td width="40%"><?echo Loc::getMessage("TEMPLATE_MODULE_MODE")?></td>
        <td width="60%">
            <input type="checkbox" name="module_mode" value="Y" <?=$arOptions["module_mode"] == "Y" ? 'checked="checked"':''?>>
        </td>
    </tr>
    <tr>
        <td width="40%"><?echo Loc::getMessage("TEMPLATE_MODULE_MODE_RESET")?></td>
        <td width="60%">
            <input type="submit" name="styles_reset" OnClick="BX.changeportaldefaultheme.ResetDefaultsOptions('<?= $APPLICATION->GetCurPage() ?>?ResetDefaultsOptions=Y&lang=<?= LANG ?>&mid=<?= urlencode($mid) ?>&<?= bitrix_sessid_get() ?>');" value="<?echo Loc::getMessage("TEMPLATE_MODULE_MODE_RESET_BTN")?>" />
        </td>
    </tr>
    <tr class="heading">
        <td colspan="2"><?=Loc::getMessage("MAIN_TAB_SETTINGS_TM")?></td>
    </tr>
    <tr>
        <td width="40%"><?echo Loc::getMessage("MAIN_TAB_SETTINGS_TM_HIDE_DESCTOP_APP")?></td>
        <td width="60%">
            <select name="auth[settings-@b24-app-desktop]">
                <option  value="block" <?=$arOptions["auth"]["settings-@b24-app-desktop"] == "block" ? 'selected':''?>><?=Loc::GetMessage('MAIN_TAB_SETTINGS_SELECT_SHOW');?></option>
                <option  value="none" <?=$arOptions["auth"]["settings-@b24-app-desktop"] == "none" ? 'selected':''?>><?=Loc::GetMessage('MAIN_TAB_SETTINGS_SELECT_HIDE');?></option>
            </select>
        </td>
    </tr>
    <tr>
        <td width="40%"><?echo Loc::getMessage("MAIN_TAB_SETTINGS_TM_HIDE_MOBILE_APP")?></td>
        <td width="60%">
            <select name="auth[settings-@24-app-mobile]">
                <option  value="block" <?=$arOptions["auth"]["settings-@24-app-mobile"] == "block" ? 'selected':''?>><?=Loc::GetMessage('MAIN_TAB_SETTINGS_SELECT_SHOW');?></option>
                <option  value="none" <?=$arOptions["auth"]["settings-@24-app-mobile"] == "none" ? 'selected':''?>><?=Loc::GetMessage('MAIN_TAB_SETTINGS_SELECT_HIDE');?></option>
            </select>
        </td>
    </tr>
    <tr>
        <td width="40%"><?echo Loc::getMessage("MAIN_TAB_SETTINGS_TM_HIDE_PULS_BTN")?></td>
        <td width="60%">
            <select name="auth[settings-@sidebar-pulse-block]">
                <option  value="block" <?=$arOptions["auth"]["settings-@sidebar-pulse-block"] == "block" ? 'selected':''?>><?=Loc::GetMessage('MAIN_TAB_SETTINGS_SELECT_SHOW');?></option>
                <option  value="none" <?=$arOptions["auth"]["settings-@sidebar-pulse-block"] == "none" ? 'selected':''?>><?=Loc::GetMessage('MAIN_TAB_SETTINGS_SELECT_HIDE');?></option>
            </select>
        </td>
    </tr>
    <tr>
        <td width="40%"><?echo Loc::getMessage("MAIN_TAB_SETTINGS_FAVICON_1")?></td>
        <td width="60%">
            <label><?=CFile::InputFile("favicon-@favicon", 20, ($arOptions["files"]["favicon-@favicon"])?$arOptions["files"]["favicon-@favicon"]:$str_IMAGE_ID);?>
        </td>
    </tr>
    <?
    $tabControl->EndTab();
    //rights
    $tabControl->BeginNextTab();

    require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/admin/group_rights.php");

    $tabControl->Buttons(); ?>

    <input <?
    if ($SUP_RIGHT < "W") echo "disabled" ?> type="submit" name="Apply"  value="<? echo GetMessage('MAIN_SAVE') ?>">
    <input type="hidden" name="Update" value="Y">
</form>
<?$tabControl->End();?>
<?
$messages = Loc::loadLanguageFile(__FILE__);
?>
<script>
    BX.message(<?=CUtil::PhpToJSObject($messages)?>);
    BX.ready(function() {
        <?if ($codeEditorId): ?>
        if(BX.changeportaldefaultheme)
            BX.changeportaldefaultheme.init('<?= $codeEditorId?>');
        <?else:?>
        if(BX.changeportaldefaultheme)
            BX.changeportaldefaultheme.init();
        <?endif;?>
    });
</script>