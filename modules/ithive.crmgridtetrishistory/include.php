<?php
use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

function checkTetrisUserFields() {
    $crmEntities = ['CRM_LEAD', 'CRM_DEAL', 'CRM_CONTACT', 'CRM_COMPANY'];

    $crmEntitiesExists = [];
    $rsData = CUserTypeEntity::GetList([], ['FIELD_NAME' => 'UF_IH_TETRIS_HISTORY']);
    while ($arRes = $rsData->fetch()) {
        $crmEntitiesExists[$arRes['ENTITY_ID']] = $arRes['ENTITY_ID'];
    }

    foreach ($crmEntities as $crmEntity) {
        if (!isset($crmEntitiesExists[$crmEntity])) {
            $oUserTypeEntity = new \CUserTypeEntity();
            $aUserFields = array(
                'ENTITY_ID' => $crmEntity,
                'FIELD_NAME' => 'UF_IH_TETRIS_HISTORY',
                'USER_TYPE_ID' => 'string',
                'SORT' => 500,
                'MULTIPLE' => 'N',
                'EDIT_IN_LIST' => 'N',
                'EDIT_FORM_LABEL' => array(
                    'ru' => GetMessage("HISTORY"),
                    'en' => GetMessage("HISTORY"),
                ),
                'LIST_COLUMN_LABEL' => array(
                    'ru' => GetMessage("HISTORY"),
                    'en' => GetMessage("HISTORY"),
                ),
                'LIST_FILTER_LABEL' => array(
                    'ru' => GetMessage("HISTORY"),
                    'en' => GetMessage("HISTORY"),
                )
            );
            $oUserTypeEntity->Add($aUserFields);
        }
    }
}

function setTetrisResultModifierParams($arParams){

    $valField = Option::get('ithive.crmgridtetrishistory', "options");
    $paramField = Option::get('ithive.crmgridtetrishistory', "params");
    $valField = (array)json_decode($valField);
    $paramField = (array)json_decode($paramField);
    $curDate = new DateTime();
    $curDateTimeStap = MakeTimeStamp($curDate->format('d.m.Y'));

    global $APPLICATION;
    $weeks = $valField['weeks'];
    $commentColor = $valField['comment_color'];

    $ENTITY_TYPE = '';

    if ($arParams["NAVIGATION_BAR"]["BINDING"]["key"] === "deal"){
        $ENTITY_TYPE = "DEAL";
        $ENTITY_TYPE_ID = CCrmOwnerType::Deal;
        $DEADLINE_PROP_CODE = $valField['deal_date'];
    }
    if ($arParams["NAVIGATION_BAR"]["BINDING"]["key"] === "lead"){
        $ENTITY_TYPE = "LEAD";
        $ENTITY_TYPE_ID = CCrmOwnerType::Lead;
        $DEADLINE_PROP_CODE = $valField['lead_date'];
    }

    if ($arParams["NAVIGATION_BAR"]["BINDING"]["key"] === "company"){
        $ENTITY_TYPE = "COMPANY";
        $ENTITY_TYPE_ID = CCrmOwnerType::Company;
        $DEADLINE_PROP_CODE = $valField['lead_date'];
    }

    if ($arParams["NAVIGATION_BAR"]["BINDING"]["key"] === "contact"){
        $ENTITY_TYPE = "CONTACT";
        $ENTITY_TYPE_ID = CCrmOwnerType::Contact;
        $DEADLINE_PROP_CODE = $valField['lead_date'];
    }

    if($ENTITY_TYPE === "DEAL" || $ENTITY_TYPE === "LEAD" || $ENTITY_TYPE === "COMPANY" || $ENTITY_TYPE === "CONTACT"){
        if($paramField['HEADERS']){
            $headers = (array)$paramField['HEADERS'];
            if (!\Bitrix\Main\Application::getInstance()->isUtfMode())
                $headers = Bitrix\Main\Text\Encoding::convertEncoding($headers, 'UTF-8', SITE_CHARSET);
            $arParams['~HEADERS'][] = $headers;
        }
        $arWeekDay = (array)$paramField['DAYS'];
        if (!\Bitrix\Main\Application::getInstance()->isUtfMode())
            $arWeekDay = Bitrix\Main\Text\Encoding::convertEncoding($arWeekDay, 'UTF-8', SITE_CHARSET);

        foreach($arParams["~ROWS"] as $k=>$row){

            $arSort = array("ID" => "ASC");

            $arFilter2 = array(
               // "ENTITY_TYPE" => $ENTITY_TYPE,
                "OWNER_ID" => $row['id'],
                "TYPE_ID" => array(0,1,2),
                ">=CREATED" => date("d.m.Y H:i:s", time()-$weeks*7*24*3600),
            );
            /*$arFilter = array(
                "ENTITY_TYPE" => $ENTITY_TYPE,
                "ENTITY_ID" => $row['id'],
                "EVENT_TYPE" => array(0,1,2),
               ">=DATE_CREATE" => date("d.m.Y", time()-$weeks*7*24*3600),
            );*/
            $arOptions = array();

            //$obRes = CCrmEvent::GetListEx($arSort, $arFilter, false, false, array(), $arOptions);

            $ss = "";
            $MON_DATE = "";
            $allEvents = array();
            $arEventByDate = array();

            $dbResult = CCrmActivity::GetList(
                $arSort,
                $arFilter2,
                false,
                false,
                array('CREATED','TYPE_ID','TYPE_NAME','RESPONSIBLE_NAME','RESPONSIBLE_LAST_NAME','COMPLETED','END_TIME','TYPE_NAME')
            );

            while ($arEvent = $dbResult->Fetch())
            {
                $DATE_CREATE = date("d.m.Y", strtotime($arEvent["CREATED"]));
                $DAY_OF_WEEK = date("D", strtotime($arEvent["CREATED"]));
                $TIME_STAMP = strtotime($DATE_CREATE);

                if($arEvent["END_TIME"]) {
                    $DATE_FINISH = date("d.m.Y", strtotime($arEvent["END_TIME"]));
                    $DAY_OF_WEEK_END = date("D", strtotime($arEvent["END_TIME"]));
                    $TIME_STAMP_FINISH = strtotime($DATE_FINISH);

                    $allEvents[$TIME_STAMP_FINISH]["TYPE_ID"] = $arEvent["TYPE_ID"];
                    $allEvents[$TIME_STAMP_FINISH]["COMPLETED"] = $arEvent["COMPLETED"];
                    $allEvents[$TIME_STAMP_FINISH]["DATE_CREATE"] = $DATE_FINISH;
                    $allEvents[$TIME_STAMP_FINISH]["DAY_OF_WEEK"] = $DAY_OF_WEEK_END;

                    $allEvents[$TIME_STAMP_FINISH]["DATE_FINISH"] = $DATE_FINISH;

                    $allEvents[$TIME_STAMP_FINISH]["EVENTS"][date("d.m.Y H:i:s", strtotime($arEvent["END_TIME"]))][] = array(
                        "EVENT_TYPE" => $arEvent["TYPE_ID"],
                        "EVENT_TEXT" => $arEvent["TYPE_NAME"] . ": " . Loc::getMessage("ITHIVE_TETRIS_MODULE_EVENTS_STATUS") . " -> " . (($arEvent["COMPLETED"] == "Y") ? Loc::getMessage('CRM_ACTIVITY_STATUS_COMPLETED') : Loc::getMessage('CRM_ACTIVITY_STATUS_WAITING')),
                        "CREATED_BY" => $arEvent["RESPONSIBLE_NAME"] . " " . $arEvent["RESPONSIBLE_LAST_NAME"],
                    );
                }

                $allEvents[$TIME_STAMP]["TYPE_ID"] = $arEvent["TYPE_ID"];
                $allEvents[$TIME_STAMP]["COMPLETED"] = $arEvent["COMPLETED"];
                $allEvents[$TIME_STAMP]["DATE_CREATE"] = $DATE_CREATE;
                $allEvents[$TIME_STAMP]["DAY_OF_WEEK"] = $DAY_OF_WEEK;
                $allEvents[$TIME_STAMP]["EVENTS"][date("d.m.Y H:i:s", strtotime($arEvent["CREATED"]))][] = array(
                    "EVENT_TYPE" => $arEvent["TYPE_ID"],
                    "EVENT_TEXT" => $arEvent["TYPE_NAME"].": ".Loc::getMessage("ITHIVE_TETRIS_MODULE_EVENTS_STATUS")." -> ".(($arEvent["COMPLETED"] == "Y" && ($TIME_STAMP_FINISH === $TIME_STAMP))?Loc::getMessage('CRM_ACTIVITY_STATUS_COMPLETED'):Loc::getMessage('CRM_ACTIVITY_STATUS_WAITING')),
                    "CREATED_BY" => $arEvent["RESPONSIBLE_NAME"]." ".$arEvent["RESPONSIBLE_LAST_NAME"],
                );


            }

            foreach ($allEvents as $TIME_STAMP => &$event){
                    /*
                "EVENT_TYPE"
                --
                0 - dobavlenie informacii vruchnuju, cherez okno "Dobavit' novoe sobytie" -> "Informacija" (pri jetom "EVENT_ID" = "INFO"), ili "Otpravlen E-mail" (pri jetom "EVENT_ID" = "MESSAGE"), ili "Telefonnyj zvonok" (pri jetom "EVENT_ID" = "PHONE") - no telefonnyj zvonok v BD nashego portala iz 225881 zapisej vstrechaetsja vsego 1 raz (5 let nazad), chto ochen' stranno;
                1 - razlichnye dejstvija nad lidom/sdelkoj: redaktirovanie, sozdanie svjazannoj zadachi, planirovanie zvonka/vstrechi;
                2 - vhodjashhee pis'mo s pochty;
                3 - jeto "prosmotry", ih ne uchityvaem;
                */
                if($event["TYPE_ID"] == 1 && !isset($allEvents[$TIME_STAMP]["COLOR"])){
                    if($valField["colored_vstrecha"] == "Y") {
                        if ($event["COMPLETED"] === "Y" ) {
                            if (isset($event['DATE_FINISH'])) {
                                $event["COLOR"] = "#ccff00";//салатовый
                            } else {
                                $event["COLOR"] = "#fff100";//желтый
                            }
                        } else {
                            $event["COLOR"] = "#fff100";//желтый
                        }
                    }else{
                        $event["COLOR"] = "#fff100";//желтый
                    }
                }elseif ($event["TYPE_ID"]==0 || $event["TYPE_ID"]==2){

                    $event["COLOR"] = "#ccff00";//салатовый
                }

            }

            /*
             * check entity comments
             * mark entity in option color, if it was commented
            */
            $query = new Bitrix\Main\Entity\Query(Bitrix\Crm\Timeline\Entity\TimelineTable::getEntity());
            $query->addSelect('*');

            $subQuery = new Bitrix\Main\Entity\Query(Bitrix\Crm\Timeline\Entity\TimelineBindingTable::getEntity());
            $subQuery->addSelect('OWNER_ID');
            $subQuery->addFilter('=ENTITY_TYPE_ID', $ENTITY_TYPE_ID);
            $subQuery->addFilter('=ENTITY_ID', $row["id"]);

            //$query->addFilter('!=COMMENT', '');
            //$query->addFilter('><CREATED', array("01.04.2018 00:00:00", "01.04.2018 23:59:59"));

            $query->registerRuntimeField('',
                new Bitrix\Main\Entity\ReferenceField('bind',
                    Bitrix\Main\Entity\Base::getInstanceByQuery($subQuery),
                    array('=this.ID' => 'ref.OWNER_ID'),
                    array('join_type' => 'INNER')
                )
            );
            $query->setOrder(array('CREATED' => 'DESC', 'ID' => 'DESC'));
            $dbResult = $query->exec();
            while($timeLine = $dbResult->fetch())
            {
                if (
                    $timeLine['ASSOCIATED_ENTITY_CLASS_NAME'] === 'CRM_EMAIL'
                    || $timeLine['ASSOCIATED_ENTITY_CLASS_NAME'] === 'VOXIMPLANT_CALL'
                    || $timeLine['ASSOCIATED_ENTITY_CLASS_NAME'] === 'TASKS'
                    || $timeLine['COMMENT']
                ) {

                    $eventText = Loc::getmessage("ITHIVE_TETRIS_MODULE_COMMENT") . HTMLtoTXT($timeLine["COMMENT"]);

                    $DATE_CREATE = date("d.m.Y", strtotime($timeLine["CREATED"]->toString()));
                    $DAY_OF_WEEK = date("D", strtotime($timeLine["CREATED"]->toString()));
                    $TIME_STAMP = strtotime($DATE_CREATE);
                    $arUser = CUser::GetByID($timeLine["AUTHOR_ID"])->Fetch();

                    if ($timeLine['ASSOCIATED_ENTITY_CLASS_NAME'] === 'CRM_EMAIL') {
                        $eventText = Loc::getmessage("ITHIVE_TETRIS_MODULE_EMAIL");
                    } elseif ($timeLine['ASSOCIATED_ENTITY_CLASS_NAME'] === 'VOXIMPLANT_CALL') {
                        $eventText = Loc::getmessage("ITHIVE_TETRIS_MODULE_CALL");
                    } elseif ($timeLine['ASSOCIATED_ENTITY_CLASS_NAME'] === 'TASKS') {
                        $eventText = Loc::getmessage("ITHIVE_TETRIS_MODULE_TASK");
                    }

                    $allEvents[$TIME_STAMP]["DATE_CREATE"] = $DATE_CREATE;
                    $allEvents[$TIME_STAMP]["DAY_OF_WEEK"] = $DAY_OF_WEEK;
                    $allEvents[$TIME_STAMP]["EVENTS"][date("H:i:s", strtotime($timeLine["CREATED"]->toString()))][] = array(
                        "EVENT_TYPE" => $timeLine["TYPE_ID"],
                        "EVENT_TEXT" => $eventText,
                        "CREATED_BY" => $arUser["NAME"]." ".$arUser["LAST_NAME"],
                    );

                    if(!isset($allEvents[$TIME_STAMP]["COLOR"]))
                        $allEvents[$TIME_STAMP]["COLOR"] = "#" . $commentColor;
                }
            }

            /*end check entity comments*/

            $DEADLINE = "";
            $ENTITY_DATE_CREATE = "";
            if($ENTITY_TYPE=="DEAL"){
                $db_deal = CCrmDeal::GetListEx(
                    array(),
                    array(
                        "ID" => $row["id"]
                    ),
                    false,
                    false,
                    array("DATE_CREATE", $DEADLINE_PROP_CODE)
                );
                while($ar_deal=$db_deal->Fetch())
                {
                    if($ar_deal[$DEADLINE_PROP_CODE])
                        $DEADLINE = date("d.m.Y", strtotime($ar_deal[$DEADLINE_PROP_CODE]));
                    $ENTITY_DATE_CREATE = strtotime($ar_deal["DATE_CREATE"]);
                }
            }
            if($ENTITY_TYPE=="LEAD"){
                $db_lead = CCrmLead::GetListEx(
                    array(),
                    array(
                        "ID" => $row["id"]
                    ),
                    false,
                    false,
                    array("DATE_CREATE", $DEADLINE_PROP_CODE)
                );
                while($ar_lead=$db_lead->Fetch())
                {
                    if($ar_lead[$DEADLINE_PROP_CODE])
                        $DEADLINE = date("d.m.Y", strtotime($ar_lead[$DEADLINE_PROP_CODE]));
                    $ENTITY_DATE_CREATE = strtotime($ar_lead["DATE_CREATE"]);
                }
            }



            $dd = 0;
            for($d=0; $d<($weeks+1)*7-1; $d++){
                $TIME_STAMP = strtotime(date("d.m.Y"))-($weeks*7-1)*24*3600+$d*24*3600;//we takes one day less, then N weeks, to make a selection on monday. Beginning with tuesday and below we will drop everything till next monday.
                $DATE_CREATE = date("d.m.Y", $TIME_STAMP);
                $DAY_OF_WEEK = date("D", $TIME_STAMP);
                if($DAY_OF_WEEK=="Mon" && !$MON_DATE) $MON_DATE = $TIME_STAMP;
                if($DAY_OF_WEEK!="Mon" && !$MON_DATE) continue;//cut all days till first monday in selection
                $dd++;
                if($dd>$weeks*7) continue;//cut all days after N weeks
                $arEventByDate[$TIME_STAMP] = $allEvents[$TIME_STAMP];
                if($TIME_STAMP==1504472400)
                    $ss .= $TIME_STAMP." - ".$DATE_CREATE." - ".$DAY_OF_WEEK." - color: ".$allEvents[$TIME_STAMP]["COLOR"]."\r\n";

                if(
                    (($DAY_OF_WEEK=="Sat" || $DAY_OF_WEEK=="Sun") && !count($arEventByDate[$TIME_STAMP]["EVENTS"]))
                    || $TIME_STAMP<$ENTITY_DATE_CREATE-24*3600
                    || $TIME_STAMP>strtotime(date("d.m.Y"))
                )
                    $arEventByDate[$TIME_STAMP] = array(
                        "DATE_CREATE" => $DATE_CREATE,
                        "DAY_OF_WEEK" => $DAY_OF_WEEK,
                        "COLOR" => "#eeeeee",//grey
                        "EVENTS" => array(),
                    );
                if(($DAY_OF_WEEK!="Sat" && $DAY_OF_WEEK!="Sun") && !count($arEventByDate[$TIME_STAMP]["EVENTS"]) && $TIME_STAMP>=$ENTITY_DATE_CREATE-24*3600 && $TIME_STAMP<=strtotime(date("d.m.Y")))
                    $arEventByDate[$TIME_STAMP] = array(
                        "DATE_CREATE" => $DATE_CREATE,
                        "DAY_OF_WEEK" => $DAY_OF_WEEK,
                        "COLOR" => "#ff6d3f",//red
                        "EVENTS" => array(),
                    );
            }

            //paste orange #FFA801, in case if there were held calls
            $s = "";
            $week = 1;
            if(count($arEventByDate)){
                ksort($arEventByDate);
                //$s = '<div style="display:none">ENTITY_DATE_CREATE='.date("d.m.Y", $ENTITY_DATE_CREATE).'</div>';
                //$s .= '<div style="display:none">DEADLINE='.$DEADLINE.'</div>';
                //$s .= '<div style="display:none">'.$ss.'<br/>allEvents<pre>'.print_r($allEvents[1504472400], true).'</pre></div>';
                //$s .= '<div style="display:none">arEventByDate<pre>'.print_r($arEventByDate[1504472400], true).'</pre></div>';
                $s .=
                    '<table class="crm-list-stage-bar-table">
				<tbody>
					<tr>
						';
                foreach($arEventByDate as $date=>$date_array){
                    $day_events = "";
                    $on_click = "";
                    if(count($date_array["EVENTS"])){
                        $day_events = "\r\n------------------------\r\n";
                        foreach($date_array["EVENTS"] as $time=>$events){
                            $day_events .= $time."\t".$events[0]["CREATED_BY"]."\t".str_replace("\"","'",$events[0]["EVENT_TEXT"])."\r\n--------\r\n";
                        }
                        //$on_click = ' onclick="alert($(this).attr(\'title\'));"';
                    }
                    $s .= 	'<td class="crm-list-stage-bar-part" style="background:'.$date_array["COLOR"].'; border-bottom: '.((date("d.m.Y")==$date_array["DATE_CREATE"])?"3px solid rgba(0,0,0,.3)":"1px solid rgba(0,0,0,.1)").'!important;'.(($DEADLINE==$date_array["DATE_CREATE"])?' border-right: 3px solid #FF0000!important;':'').' height:15px;" title="'.date("d.m", strtotime($date_array["DATE_CREATE"])).', '.$arWeekDay[$date_array["DAY_OF_WEEK"]].GetMessage("ITHIVE_TETRIS_MODULE_EVENTS").count($date_array["EVENTS"]).$day_events.'"'.$on_click.'></td>
';
                    if($date_array["DAY_OF_WEEK"]=="Sun" && $week<$weeks){
                        $week++;
                        $s .=
                            '						</tr>
					<tr>
';
                    }
                }
                $s .=
                    '
					</tr>
				</tbody>
			</table>';
            }

            $moduleStatus = CModule::IncludeModuleEx(GetModuleID(__FILE__));
            if($moduleStatus == MODULE_DEMO_EXPIRED){
                $s = '<span style="color: red;">'.Loc::getMessage('ITHIVE_TETRIS_MODULE_DEMO_EXPIRED_TEXT').', <a href="http://marketplace.1c-bitrix.ru/solutions/ithive.crmgridtetrishistory/" target="_blank">'.Loc::getMessage('ITHIVE_TETRIS_MODULE_DEMO_EXPIRED_LINK').'</a></span>';
            }

            $arParams["~ROWS"][$k]["columns"][$headers['id']] = $s;
        }
    }

    return $arParams;

}
?>