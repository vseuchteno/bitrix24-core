<?
$module_id = GetModuleID(__FILE__);

use Bitrix\Main\Config\Option,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\IO\FileNotFoundException;

Loc::loadMessages(__FILE__);
Loc::loadMessages(__DIR__.'/install/steps.php');
function ShowParamsHTMLByArray($arParams, $module_id)
{
    foreach($arParams as $key => $Option)
    {
        __AdmSettingsDrawRow($module_id, $Option);
    }
}


//CRM_LEAD
//CRM_DEAL
$entities = array('CRM_LEAD','CRM_DEAL');
$arVals = array();
$rsData = CUserTypeEntity::GetList( array($by=>$order),
    array('ENTITY_ID'=>$entities,'USER_TYPE_ID'=>array('datetime', 'date'), 'LANG'=>LANGUAGE_ID) );

while($arRes = $rsData->Fetch())
{
    $arVals[$arRes["ENTITY_ID"]][$arRes["FIELD_NAME"]] = $arRes["LIST_COLUMN_LABEL"].' ('.$arRes["FIELD_NAME"].')';
}
if(!$arVals['CRM_LEAD']){
    $arVals['CRM_LEAD']['add'] = Loc::getMessage('ADD');
}
if (!$arVals['CRM_DEAL']) {
    $arVals['CRM_DEAL']['add'] = Loc::getMessage('ADD');
}

$arCommentColors = array(
    "fff100" => Loc::getMessage('YELLOW'),
    "ccff00" => Loc::getMessage('GREEN')
);

$arColoreVstrecha = array(
    'Y' => Loc::getMessage('YES'),
    'N' => Loc::getMessage('NO'),
);

$arAllOptions = array(
    Array("lead_date", Loc::getMessage("PROPERTY_LID_DATE"), "", Array("selectbox", $arVals['CRM_LEAD']),
    array('ENTITY'=>'CRM_LEAD', 'FIELD_NAME'=>'LD_ITHIVE_DDL', 'NAME'=>Loc::getMessage('LEAD_ITHIVE_DEADLINE'))),
    Array("deal_date", Loc::getMessage("PROPERTY_DEL_DATE"), "", Array("selectbox", $arVals['CRM_DEAL']),
    array('ENTITY'=>'CRM_DEAL', 'FIELD_NAME'=>'DL_ITHIVE_DDL', 'NAME'=>Loc::getMessage('DEAL_ITHIVE_DEADLINE'))),
    Array("comment_color", Loc::getMessage("COMMENT_COLOR"), "", Array("selectbox", $arCommentColors), "comment_color"),
    Array("weeks", Loc::getMessage("PROPERTY_WEEK"), "3", Array("text", 29), 'weeks'),
    Array("colored_vstrecha", Loc::getMessage("COLORED_VSTRECHA"), "", Array("selectbox", $arColoreVstrecha), "colored_vstrecha"),
);
foreach ($arAllOptions as $option){
    if($_REQUEST[$option[0]]=='add'){
        $codeS = $option[4];
        if(is_array($codeS)){
            $oUserTypeEntity = new \CUserTypeEntity();
            $FIELD_NAME = 'UF_CRM_' . $codeS['FIELD_NAME'];
            $aUserFields = array(
                'ENTITY_ID' => $codeS['ENTITY'],
                'FIELD_NAME' => $FIELD_NAME,
                'USER_TYPE_ID' => 'date',
                'XML_ID' => 'XML_ID_' . $FIELD_NAME,
                'SORT' => 500,
                'MULTIPLE' => 'N',
                'EDIT_FORM_LABEL' => array(
                    'ru' => $codeS['NAME'],
                    'en' => $codeS['NAME'],
                ),
                'LIST_COLUMN_LABEL' => array(
                    'ru' => $codeS['NAME'],
                    'en' => $codeS['NAME'],
                ),
                'LIST_FILTER_LABEL' => array(
                    'ru' => $codeS['NAME'],
                    'en' => $codeS['NAME'],
                )
            );
            $iUserFieldId = $oUserTypeEntity->Add($aUserFields);
        }
    }
}

if($_REQUEST){
    foreach($arAllOptions as $key => $arParams){
        if(isset($_REQUEST[$arParams[0]])){
            $arValues[$arParams[0]] = $_REQUEST[$arParams[0]];
            $arAllOptions[$key][2] = $_REQUEST[$arParams[0]];
        }
    }
    if(count($arValues)==count($arAllOptions)) {
        Option::set($module_id, "options", json_encode($arValues));
    }
}


$valField = Option::get($module_id, "options");

$arFields = (array)json_decode($valField);

foreach($arAllOptions as $key => $arParams){
    if($arFields[$arParams[0]]){
        $arAllOptions[$key][2] = $arFields[$arParams[0]];
    }
}
$INSTRUCTION =  '/bitrix/images/' . $module_id . '/instruction.png';
?>
<div class="adm-info-message-wrap">
    <div class="adm-info-message">
        <?=Loc::getMessage('INSTRUCTION', array('#INSTRUCTION#'=>$INSTRUCTION))?>
    </div>
</div>
<div class="adm-detail-block">
    <div class="adm-detail-content-wrap">
        <div class="adm-detail-content">
            <div class="adm-detail-title"><?=Loc::getMessage('TITLE');?></div>
            <form method="post" name="options" action="<? echo $APPLICATION->GetCurUri() ?>">
                <div class="adm-detail-content-item-block">

                <? if ($arErrors) { ?>
                    <table class="message">
                        <tr>
                            <td valign="center">
                                <div class="icon-error"></div>
                            </td>
                            <td>
                                <? print($arErrors) ?>
                        </tr>
                    </table>
                <? } ?>
                <table>
                    <tbody>
                    <?= bitrix_sessid_post() ?>
                    <?
                    ShowParamsHTMLByArray($arAllOptions, $module_id);
                    ?>
                    </tbody>
                </table>
                </div>
                <div class="adm-detail-content-btns-wrap">
                    <div class="adm-detail-content-btns">
                        <input type="submit" name="Update" value="<?= GetMessage("MAIN_SAVE") ?>" class="adm-btn-save">
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>