<?php
namespace Bitrix\Location\Infrastructure\Service\LoggerService;

class LogLevel
{
	public const NONE      = 0;
	public const EMERGENCY = 100;
	public const ALERT     = 200;
	public const CRITICAL  = 300;
	public const ERROR     = 400;
	public const WARNING   = 500;
	public const NOTICE    = 600;
	public const INFO      = 700;
	public const DEBUG     = 800;
}
