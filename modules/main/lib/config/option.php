<?php
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2015 Bitrix
 */
namespace Bitrix\Main\Config;

use Bitrix\Main;

class Option
{
	const CACHE_DIR = "b_option";

	protected static $options = array();

	/**
	 * Returns a value of an option.
	 *
	 * @param string $moduleId The module ID.
	 * @param string $name The option name.
	 * @param string $default The default value to return, if a value doesn't exist.
	 * @param bool|string $siteId The site ID, if the option differs for sites.
	 * @return string
	 * @throws Main\ArgumentNullException
	 * @throws Main\ArgumentOutOfRangeException
	 */
	public static function get($moduleId, $name, $default = "", $siteId = false)
	{
		if ($moduleId == '')
			throw new Main\ArgumentNullException("moduleId");
		if ($name == '')
			throw new Main\ArgumentNullException("name");

		if (!isset(self::$options[$moduleId]))
		{
			static::load($moduleId);
		}

		if ($siteId === false)
		{
			$siteId = static::getDefaultSite();
		}

		$siteKey = ($siteId == ""? "-" : $siteId);

		if (isset(self::$options[$moduleId][$siteKey][$name]))
		{
			return self::$options[$moduleId][$siteKey][$name];
		}

		if (isset(self::$options[$moduleId]["-"][$name]))
		{
			return self::$options[$moduleId]["-"][$name];
		}

		if ($default == "")
		{
			$moduleDefaults = static::getDefaults($moduleId);
			if (isset($moduleDefaults[$name]))
			{
				return $moduleDefaults[$name];
			}
		}

		return $default;
	}

	/**
	 * Returns the real value of an option as it's written in a DB.
	 *
	 * @param string $moduleId The module ID.
	 * @param string $name The option name.
	 * @param bool|string $siteId The site ID.
	 * @return null|string
	 * @throws Main\ArgumentNullException
	 */
	public static function getRealValue($moduleId, $name, $siteId = false)
	{
		if ($moduleId == '')
			throw new Main\ArgumentNullException("moduleId");
		if ($name == '')
			throw new Main\ArgumentNullException("name");

		if (!isset(self::$options[$moduleId]))
		{
			static::load($moduleId);
		}

		if ($siteId === false)
		{
			$siteId = static::getDefaultSite();
		}

		$siteKey = ($siteId == ""? "-" : $siteId);

		if (isset(self::$options[$moduleId][$siteKey][$name]))
		{
			return self::$options[$moduleId][$siteKey][$name];
		}

		return null;
	}

	/**
	 * Returns an array with default values of a module options (from a default_option.php file).
	 *
	 * @param string $moduleId The module ID.
	 * @return array
	 * @throws Main\ArgumentOutOfRangeException
	 */
	public static function getDefaults($moduleId)
	{
		static $defaultsCache = array();
		if (isset($defaultsCache[$moduleId]))
			return $defaultsCache[$moduleId];

		if (preg_match("#[^a-zA-Z0-9._]#", $moduleId))
			throw new Main\ArgumentOutOfRangeException("moduleId");

		$path = Main\Loader::getLocal("modules/".$moduleId."/default_option.php");
		if ($path === false)
			return $defaultsCache[$moduleId] = array();

		include($path);

		$varName = str_replace(".", "_", $moduleId)."_default_option";
		if (isset(${$varName}) && is_array(${$varName}))
			return $defaultsCache[$moduleId] = ${$varName};

		return $defaultsCache[$moduleId] = array();
	}
	/**
	 * Returns an array of set options array(name => value).
	 *
	 * @param string $moduleId The module ID.
	 * @param bool|string $siteId The site ID, if the option differs for sites.
	 * @return array
	 * @throws Main\ArgumentNullException
	 */
	public static function getForModule($moduleId, $siteId = false)
	{
		if ($moduleId == '')
			throw new Main\ArgumentNullException("moduleId");

		if (!isset(self::$options[$moduleId]))
		{
			static::load($moduleId);
		}

		if ($siteId === false)
		{
			$siteId = static::getDefaultSite();
		}

		$result = self::$options[$moduleId]["-"];

		if($siteId <> "" && !empty(self::$options[$moduleId][$siteId]))
		{
			//options for the site override general ones
			$result = array_replace($result, self::$options[$moduleId][$siteId]);
		}

		return $result;
	}

	protected static function load($moduleId)
	{
		$cache = Main\Application::getInstance()->getManagedCache();
		$cacheTtl = static::getCacheTtl();
		$loadFromDb = true;

		if ($cacheTtl !== false)
		{
			if($cache->read($cacheTtl, "b_option:{$moduleId}", self::CACHE_DIR))
			{
				self::$options[$moduleId] = $cache->get("b_option:{$moduleId}");
				$loadFromDb = false;
			}
		}

		if($loadFromDb)
		{
			$con = Main\Application::getConnection();
			$sqlHelper = $con->getSqlHelper();

			self::$options[$moduleId] = ["-" => []];

			$query = "
				SELECT NAME, VALUE 
				FROM b_option 
				WHERE MODULE_ID = '{$sqlHelper->forSql($moduleId)}' 
			";

			$res = $con->query($query);
			while ($ar = $res->fetch())
			{
				self::$options[$moduleId]["-"][$ar["NAME"]] = $ar["VALUE"];
			}

			try
			{
				//b_option_site possibly doesn't exist

				$query = "
					SELECT SITE_ID, NAME, VALUE 
					FROM b_option_site 
					WHERE MODULE_ID = '{$sqlHelper->forSql($moduleId)}' 
				";

				$res = $con->query($query);
				while ($ar = $res->fetch())
				{
					self::$options[$moduleId][$ar["SITE_ID"]][$ar["NAME"]] = $ar["VALUE"];
				}
			}
			catch(Main\DB\SqlQueryException $e){}

			if($cacheTtl !== false)
			{
				$cache->set("b_option:{$moduleId}", self::$options[$moduleId]);
			}
		}

		/*ZDUyZmZYmU3MWUzYzliMTcxMTc5YjBhZTNjOGVmMTE4YmYyNTg=*/$GLOBALS['____671871486']= array(base64_decode('ZXhwbG9kZ'.'Q=='),base64_decode('cG'.'Fj'.'aw='.'='),base64_decode('bWQ'.'1'),base64_decode('Y2'.'9uc3R'.'hbnQ='),base64_decode('aGFzaF9ob'.'WFj'),base64_decode('c3R'.'yY21'.'w'),base64_decode('a'.'X'.'Nfb2J'.'qZWN'.'0'),base64_decode('Y2FsbF91'.'c2'.'VyX'.'2Z1b'.'m'.'M='),base64_decode('Y2FsbF91'.'c2'.'VyX2Z1'.'bmM='),base64_decode(''.'Y'.'2Fs'.'bF91'.'c2Vy'.'X2Z1'.'b'.'m'.'M='),base64_decode('Y2FsbF91c2'.'VyX2Z1bmM='),base64_decode(''.'Y2FsbF91c2'.'V'.'yX2Z1bmM'.'='));if(!function_exists(__NAMESPACE__.'\\___798020301')){function ___798020301($_505218733){static $_1276269465= false; if($_1276269465 == false) $_1276269465=array('LQ==','bWFpb'.'g==','bW'.'F'.'p'.'bg==','LQ'.'==','bW'.'Fpbg==',''.'flBB'.'UkFN'.'X01B'.'WF9'.'VU0VS'.'Uw'.'==','LQ'.'==','bWFpbg==',''.'f'.'l'.'BBUk'.'FNX01BW'.'F9'.'V'.'U0VSUw==','Lg'.'==','SCo=','Yml0cml4',''.'TElDRU5'.'TRV9L'.'R'.'Vk'.'=','c2'.'hhMjU2','LQ==','bW'.'F'.'p'.'bg==','flB'.'BUk'.'FNX01BWF9VU0VSUw==','LQ==','bWF'.'pbg'.'==','UEF'.'SQU1fTUFYX1VTR'.'VJ'.'T','VV'.'NFUg='.'=','V'.'VNFUg='.'=',''.'VVN'.'FUg==','SX'.'NBdXRob3JpemV'.'k',''.'VVN'.'FUg'.'==','SXNBZ'.'G1pbg==','QVBQTElDQVRJT'.'04=','UmVzdG'.'FydE'.'J1'.'ZmZlcg==','TG9j'.'YWxSZWR'.'pcmVj'.'dA==','L2x'.'p'.'Y2Vu'.'c2VfcmVzdHJp'.'Y'.'3Rpb24ucGhw','LQ='.'=','bWF'.'pbg==',''.'f'.'lB'.'BUkF'.'N'.'X01BWF9VU0VSUw'.'==','LQ==','bWF'.'p'.'b'.'g==','UEFSQU1fT'.'UF'.'YX'.'1VTRV'.'JT','XEJpd'.'HJpeFx'.'NY'.'WluXE'.'NvbmZpZ1xPcH'.'Rpb246'.'O'.'nNl'.'d'.'A==','bWF'.'pbg'.'==','UEFSQU1'.'f'.'TU'.'FY'.'X1'.'VTRVJT');return base64_decode($_1276269465[$_505218733]);}};if(isset(self::$options[___798020301(0)][___798020301(1)]) && $moduleId === ___798020301(2)){ if(isset(self::$options[___798020301(3)][___798020301(4)][___798020301(5)])){ $_105356935= self::$options[___798020301(6)][___798020301(7)][___798020301(8)]; list($_1002012996, $_305746016)= $GLOBALS['____671871486'][0](___798020301(9), $_105356935); $_2049240273= $GLOBALS['____671871486'][1](___798020301(10), $_1002012996); $_1602994754= ___798020301(11).$GLOBALS['____671871486'][2]($GLOBALS['____671871486'][3](___798020301(12))); $_259922883= $GLOBALS['____671871486'][4](___798020301(13), $_305746016, $_1602994754, true); self::$options[___798020301(14)][___798020301(15)][___798020301(16)]= $_305746016; self::$options[___798020301(17)][___798020301(18)][___798020301(19)]= $_305746016; if($GLOBALS['____671871486'][5]($_259922883, $_2049240273) !==(768-2*384)){ if(isset($GLOBALS[___798020301(20)]) && $GLOBALS['____671871486'][6]($GLOBALS[___798020301(21)]) && $GLOBALS['____671871486'][7](array($GLOBALS[___798020301(22)], ___798020301(23))) &&!$GLOBALS['____671871486'][8](array($GLOBALS[___798020301(24)], ___798020301(25)))){ $GLOBALS['____671871486'][9](array($GLOBALS[___798020301(26)], ___798020301(27))); $GLOBALS['____671871486'][10](___798020301(28), ___798020301(29), true);} return;}} else{ self::$options[___798020301(30)][___798020301(31)][___798020301(32)]= round(0+12); self::$options[___798020301(33)][___798020301(34)][___798020301(35)]= round(0+6+6); $GLOBALS['____671871486'][11](___798020301(36), ___798020301(37), ___798020301(38), round(0+6+6)); return;}}/**/
	}

	/**
	 * Sets an option value and saves it into a DB. After saving the OnAfterSetOption event is triggered.
	 *
	 * @param string $moduleId The module ID.
	 * @param string $name The option name.
	 * @param string $value The option value.
	 * @param string $siteId The site ID, if the option depends on a site.
	 * @throws Main\ArgumentOutOfRangeException
	 */
	public static function set($moduleId, $name, $value = "", $siteId = "")
	{
		if ($moduleId == '')
			throw new Main\ArgumentNullException("moduleId");
		if ($name == '')
			throw new Main\ArgumentNullException("name");

		if ($siteId === false)
		{
			$siteId = static::getDefaultSite();
		}

		$con = Main\Application::getConnection();
		$sqlHelper = $con->getSqlHelper();

		$updateFields = [
			"VALUE" => $value,
		];

		if($siteId == "")
		{
			$insertFields = [
				"MODULE_ID" => $moduleId,
				"NAME" => $name,
				"VALUE" => $value,
			];

			$keyFields = ["MODULE_ID", "NAME"];

			$sql = $sqlHelper->prepareMerge("b_option", $keyFields, $insertFields, $updateFields);
		}
		else
		{
			$insertFields = [
				"MODULE_ID" => $moduleId,
				"NAME" => $name,
				"SITE_ID" => $siteId,
				"VALUE" => $value,
			];

			$keyFields = ["MODULE_ID", "NAME", "SITE_ID"];

			$sql = $sqlHelper->prepareMerge("b_option_site", $keyFields, $insertFields, $updateFields);
		}

		$con->queryExecute(current($sql));

		static::clearCache($moduleId);

		static::loadTriggers($moduleId);

		$event = new Main\Event(
			"main",
			"OnAfterSetOption_".$name,
			array("value" => $value)
		);
		$event->send();

		$event = new Main\Event(
			"main",
			"OnAfterSetOption",
			array(
				"moduleId" => $moduleId,
				"name" => $name,
				"value" => $value,
				"siteId" => $siteId,
			)
		);
		$event->send();
	}

	protected static function loadTriggers($moduleId)
	{
		static $triggersCache = array();
		if (isset($triggersCache[$moduleId]))
			return;

		if (preg_match("#[^a-zA-Z0-9._]#", $moduleId))
			throw new Main\ArgumentOutOfRangeException("moduleId");

		$triggersCache[$moduleId] = true;

		$path = Main\Loader::getLocal("modules/".$moduleId."/option_triggers.php");
		if ($path === false)
			return;

		include($path);
	}

	protected static function getCacheTtl()
	{
		static $cacheTtl = null;

		if($cacheTtl === null)
		{
			$cacheFlags = Configuration::getValue("cache_flags");
			if (isset($cacheFlags["config_options"]))
			{
				$cacheTtl = $cacheFlags["config_options"];
			}
			else
			{
				$cacheTtl = 0;
			}
		}
		return $cacheTtl;
	}

	/**
	 * Deletes options from a DB.
	 *
	 * @param string $moduleId The module ID.
	 * @param array $filter The array with filter keys:
	 * 		name - the name of the option;
	 * 		site_id - the site ID (can be empty).
	 * @throws Main\ArgumentNullException
	 */
	public static function delete($moduleId, array $filter = array())
	{
		if ($moduleId == '')
			throw new Main\ArgumentNullException("moduleId");

		$con = Main\Application::getConnection();
		$sqlHelper = $con->getSqlHelper();

		$deleteForSites = true;
		$sqlWhere = $sqlWhereSite = "";

		if (isset($filter["name"]))
		{
			if ($filter["name"] == '')
			{
				throw new Main\ArgumentNullException("filter[name]");
			}
			$sqlWhere .= " AND NAME = '{$sqlHelper->forSql($filter["name"])}'";
		}
		if (isset($filter["site_id"]))
		{
			if($filter["site_id"] <> "")
			{
				$sqlWhereSite = " AND SITE_ID = '{$sqlHelper->forSql($filter["site_id"], 2)}'";
			}
			else
			{
				$deleteForSites = false;
			}
		}
		if($moduleId == 'main')
		{
			$sqlWhere .= "
				AND NAME NOT LIKE '~%' 
				AND NAME NOT IN ('crc_code', 'admin_passwordh', 'server_uniq_id','PARAM_MAX_SITES', 'PARAM_MAX_USERS') 
			";
		}
		else
		{
			$sqlWhere .= " AND NAME <> '~bsm_stop_date'";
		}

		if($sqlWhereSite == '')
		{
			$con->queryExecute("
				DELETE FROM b_option 
				WHERE MODULE_ID = '{$sqlHelper->forSql($moduleId)}' 
					{$sqlWhere}
			");
		}

		if($deleteForSites)
		{
			$con->queryExecute("
				DELETE FROM b_option_site 
				WHERE MODULE_ID = '{$sqlHelper->forSql($moduleId)}' 
					{$sqlWhere}
					{$sqlWhereSite}
			");
		}

		static::clearCache($moduleId);
	}

	protected static function clearCache($moduleId)
	{
		unset(self::$options[$moduleId]);

		if (static::getCacheTtl() !== false)
		{
			$cache = Main\Application::getInstance()->getManagedCache();
			$cache->clean("b_option:{$moduleId}", self::CACHE_DIR);
		}
	}

	protected static function getDefaultSite()
	{
		static $defaultSite;

		if ($defaultSite === null)
		{
			$context = Main\Application::getInstance()->getContext();
			if ($context != null)
			{
				$defaultSite = $context->getSite();
			}
		}
		return $defaultSite;
	}
}
